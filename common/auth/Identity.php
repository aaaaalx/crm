<?php

namespace common\auth;

use core\helpers\CommonHelper;
use core\helpers\DumpHelper;
use core\interfaces\UserInterface;
use core\interfaces\UserStorageRepository;
use core\repositories\read\UserReadRepository;
use yii\web\IdentityInterface;
use core\entities\User;
use core\entities\redis\User as RUser;

/**
 * @property User $user
 */

class Identity implements IdentityInterface
{

    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public static function findIdentity($id)
    {
        $user = self::getRepository()->findActiveById($id);
        return $user ? new self($user): null;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = self::getStorage()->getByToken($token);
        if (!$user) {
            $user = self::getRepository()->findActiveByToken($token);
            if (!$user) {
                return null;
            }
            self::getStorage()->save(self::createStorageUser($user));
        }
        return $user ? new self($user): null;
    }

    public function getId(): int
    {
        return $this->user->id;
    }

    public function getAuthKey(): string
    {
        return $this->user->auth_key;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getAccessToken(): string
    {
        return $this->user->access_token;
    }

    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    private static function getRepository(): UserReadRepository
    {
        return \Yii::$container->get(UserReadRepository::class);
    }

    private static function getStorage(): UserStorageRepository
    {
        return \Yii::$container->get(UserStorageRepository::class);
    }

    private static function createStorageUser(User $user): RUser
    {
        return RUser::create($user->id, $user->access_token, $user->email, $user->password_hash, $user->time_zone, $user->status, $user->ctime);
    }
}