<?php


namespace common\config;

use core\repositories\TaskRepository;
use core\services\api\TaskService;
use Yii;
use core\components\redis\RedisAdapter;
use core\events\EventDispatcher;
use core\events\events\UserRegistrationEvent;
use core\events\handlers\UserRegistrationHandler;
use core\interfaces\KeyValueStorageInterface;
use core\interfaces\TokenStorageInterface;
use core\interfaces\UserStorageRepository;
use core\repositories\ProjectRepository;
use core\repositories\redis\UserRedisRepository;
use core\repositories\UserRepository;
use core\services\api\ProjectService;
use core\services\common\ManageFilesService;
use yii\base\BootstrapInterface;

class BootUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->setSingleton(RedisAdapter::class, function() use ($app) {
            return new RedisAdapter($app->redis);
        });

        $container->setSingleton(KeyValueStorageInterface::class, RedisAdapter::class);

        $container->setSingleton(UserStorageRepository::class, UserRedisRepository::class);

        $container->setSingleton(EventDispatcher::class, function() use ($container){
            return new EventDispatcher($container, [
                UserRegistrationEvent::class => [
                    UserRegistrationHandler::class
                ]
            ]);
        });

        $container->setSingleton(ProjectService::class, function() use ($container){
            return new ProjectService(new ProjectRepository, new ManageFilesService, new UserRepository(new EventDispatcher($container, [
                UserRegistrationEvent::class => [
                    UserRegistrationHandler::class
                ]
            ])), Yii::getAlias('@api'));
        });

        $container->setSingleton(TaskService::class, function() use ($container){
            return new TaskService(new TaskRepository, new ManageFilesService, new UserRepository(new EventDispatcher($container, [
                UserRegistrationEvent::class => [
                    UserRegistrationHandler::class
                ]
            ])), Yii::getAlias('@api'));
        });

    }
}