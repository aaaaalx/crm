<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'class' => \core\components\i18n\I18N::class,
            'languages' => ['ru-RU', 'en-EN', 'ua-UK'],
            'languagesList' => ['ru-RU' => 'Русский', 'en-EN' => 'English', 'ua-UK' => 'Українська']
        ]
    ],
];
