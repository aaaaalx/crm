<?php

namespace backend\auth;

use core\entities\Admin;
use core\repositories\read\AdminReadRepository;
use developeruz\db_rbac\interfaces\UserRbacInterface;
use yii\web\IdentityInterface;

class Identity implements IdentityInterface
{

    private $admin;

    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }

    public static function findIdentity($id)
    {
        $admin = self::getRepository()->findActiveById($id);
        return $admin ? new self($admin): null;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $admin = self::getRepository()->findActiveByToken($token);
        return $admin ? new self($admin): null;
    }

    public function getId(): int
    {
        return $this->admin->id;
    }

    public function getAuthKey(): string
    {
        return $this->admin->auth_key;
    }

    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getUser()
    {
        return $this->admin;
    }

    public function getTimezone()
    {
        return $this->admin->time_zone;
    }

    public function getLang()
    {
        return $this->admin->lang;
    }

    private static function getRepository(): AdminReadRepository
    {
        return \Yii::$container->get(AdminReadRepository::class);
    }
}