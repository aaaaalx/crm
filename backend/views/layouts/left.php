<?php
    use yii\widgets\ActiveForm;
    use core\forms\backend\SetLangForm;
    use core\forms\backend\SearchForm;
    use core\helpers\LanguagesHelper;
?>

<?php

$langForm = new SetLangForm(Yii::$app->user->identity->getUser());
$searchForm = new SearchForm();
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel" style="padding: 10px 10px 20px 10px;">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <?= \yii\helpers\Html::a(
                    'Sign out',
                    ['/logout'],
                    ['data-method' => 'post', 'class' => 'btn btn-info']
                ) ?>
            </div>
        </div>

        <?php

        $form = ActiveForm::begin([
            'action' => ['/search/search'],
            'method' => 'get',
            'options' => [
                'class' => 'sidebar-form search-form white-form'
            ],
            'fieldConfig' => [
                'options' => ['class' => 'input-group'],
            ],
        ]);

        echo $form->field($searchForm, 'q', [
            'template' =>
                '{input}<span class="input-group-btn"><button type="submit"class="btn btn-flat btn-search"><i class="fa fa-search"></i></button>'
        ])->textInput(['class' => 'form-control form-control-own', 'placeholder' => 'Search...']);

        ActiveForm::end();
        ?>

        <?php

        $form = ActiveForm::begin([
            'action' => ['/admin/set-lang'],
            'method' => 'post',
            'options' => [
                'class' => 'sidebar-form search-form white-form'
            ],
            'fieldConfig' => [
                'options' => ['class' => 'input-group'],
            ],
        ]);

        echo $form->field($langForm, 'lang', ['template' => '{input}'])->dropDownList(LanguagesHelper::getList())->label(false);

        ActiveForm::end();
        ?>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu sidebar-menu-own tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => Yii::t('layout.menu','Admins'),
                        'icon' => 'user',
                        'visible' => Yii::$app->user->can('admin'),
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('layout.menu','Roles'), 'icon' => 'users', 'url' => ['/access/role'],],
                            ['label' => Yii::t('layout.menu','Permissions'), 'icon' => 'unlock-alt', 'url' => ['/access/permission'],],
                            ['label' => Yii::t('layout.menu','Add admin'), 'icon' => 'user-plus', 'url' => ['/admin/create'],],
                            ['label' => Yii::t('layout.menu','Admins list'), 'icon' => 'list-ol', 'url' => ['/admin/index'],],
                        ],
                    ],
                    [
                        'visible' => Yii::$app->user->can('user'),
                        'label' => Yii::t('layout.menu','Users'),
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('layout.menu','Add user'), 'icon' => 'user-plus', 'url' => ['/user/create'],],
                            ['label' => Yii::t('layout.menu','Users list'), 'icon' => 'list-ol', 'url' => ['/user/index'],],
                        ],
                    ],

                    [
                        'visible' => Yii::$app->user->can('profile'),
                        'label' => Yii::t('layout.menu','Profiles'),
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('layout.menu','Add profile'), 'icon' => 'user-plus', 'url' => ['/profile/create'],],
                            ['label' => Yii::t('layout.menu','Profiles list'), 'icon' => 'list-ol', 'url' => ['/profile/index'],],
                        ],
                    ],
                    [
                        'visible' => Yii::$app->user->can('position'),
                        'label' => Yii::t('layout.menu','Position'),
                        'icon' => 'sitemap',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('layout.menu','Add position'), 'icon' => 'plus', 'url' => ['/position/create'],],
                            ['label' => Yii::t('layout.menu','Position list'), 'icon' => 'list-ol', 'url' => ['/position/index'],],
                        ],
                    ],
                    [
                        'label' => Yii::t('layout.menu','Front Roles'),
                        'icon' => 'user',
                        'visible' => Yii::$app->user->can('role'),
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('layout.menu','Add role'), 'icon' => 'user-plus', 'url' => ['/role/create'],],
                            ['label' => Yii::t('layout.menu','Roles list'), 'icon' => 'list-ol', 'url' => ['/role/index'],],
                        ],
                    ],
                    [
                        'visible' => Yii::$app->user->can('language'),
                        'label' => Yii::t('layout.menu','Languages'),
                        'icon' => 'language',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('layout.menu','Add language'), 'icon' => 'plus', 'url' => ['/language/create'],],
                            ['label' => Yii::t('layout.menu','Languages list'), 'icon' => 'list-ol', 'url' => ['/language/index'],],
                        ],
                    ],
                    [
                        'visible' => Yii::$app->user->can('translate'),
                        'label' => Yii::t('layout.menu','Translations'),
                        'icon' => 'cogs',
                        'url' => ['/translate/index'],
                    ],
                    [
                        'visible' => Yii::$app->user->can('version'),
                        'label' => Yii::t('layout.menu','Version'),
                        'icon' => 'refresh',
                        'url' => ['/version/index'],
                    ],
                    [
                        'visible' => Yii::$app->user->can('project'),
                        'label' => Yii::t('layout.menu','Project'),
                        'icon' => 'th',
                        'url' => ['/project/index'],
                    ],
                    [
                        'visible' => Yii::$app->user->can('tag'),
                        'label' => Yii::t('layout.menu','Tags'),
                        'icon' => 'tags',
                        'url' => ['/tag/index'],
                    ],
                    [
                        'visible' => Yii::$app->user->can('options'),
                        'label' => Yii::t('layout.menu','Options'),
                        'icon' => 'ellipsis-v',
                        'url' => ['/options/index'],
                    ],
                    [
                        'visible' => Yii::$app->user->can('task'),
                        'label' => Yii::t('layout.menu','Tasks'),
                        'icon' => 'tasks',
                        'url' => ['/task/index'],
                    ],
                    [
                        //'visible' => Yii::$app->user->can('taskType'),
                        'label' => Yii::t('layout.menu','Tasks Type'),
                        'icon' => 'share-alt',
                        'url' => ['/task-type/index'],
                    ],
                    [
                        //'visible' => Yii::$app->user->can('taskType'),
                        'label' => Yii::t('layout.menu','Wiki'),
                        'icon' => 'share-alt',
                        'url' => ['wiki/content/index'],
                    ]
                ],

            ]
        ) ?>
    </section>
</aside>
