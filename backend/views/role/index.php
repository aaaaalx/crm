<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\helpers\RolesHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Roles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Role'), ['create'], ['class' => 'btn btn-success btn-own btn-yellow pull-left']) ?>
    </p>
    <div class="clearfix"></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'tasks_permissions',
                'format' => 'html',
                'value' => function($model) {
                    return RolesHelper::getTasksRulesHtml($model);
                }
            ],
            [
                'attribute' => 'projects_permissions',
                'format' => 'html',
                'value' => function($model) {
                    return RolesHelper::getProjectsRulesHtml($model);
                }
            ],
            [
                'attribute' => 'project_members_permissions',
                'format' => 'html',
                'value' => function($model) {
                    return RolesHelper::getProjectMembersRulesHtml($model);
                }
            ],
            [
                'attribute' => 'task_members_permissions',
                'format' => 'html',
                'value' => function($model) {
                    return RolesHelper::getTaskMembersRulesHtml($model);
                }
            ],
            [
                'attribute' => 'user_permission',
                'format' => 'html',
                'value' => function($model) {
                    return RolesHelper::getUsersRulesHtml($model);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
</div>