<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use core\helpers\RolesHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\backend\RoleForm */

$this->title = 'Update role';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="room-role-create white-box">

    <div class="room-role-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tasks_permissions')->checkboxList(RolesHelper::getRulesNames()) ?>

        <?= $form->field($model, 'projects_permissions')->checkboxList(RolesHelper::getRulesNames()) ?>

        <?= $form->field($model, 'project_members_permissions')->checkboxList(RolesHelper::getRulesNames()) ?>

        <?= $form->field($model, 'task_members_permissions')->checkboxList(RolesHelper::getRulesNames()) ?>

        <?= $form->field($model, 'user_permission')->checkboxList(RolesHelper::getRulesNames())?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-own btn-yellow']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

