<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \core\forms\search\VersionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Versions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Version', ['create'], ['class' => 'btn btn-success btn-own btn-yellow pull-left']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'version',
            [
                'attribute' => 'ctime',
                'format' => 'datetime'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
