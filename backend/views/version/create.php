<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model \core\entities\Version */

$this->title = 'Create Version';
$this->params['breadcrumbs'][] = ['label' => 'Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="version-create white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <div class="currency-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-success btn-own btn-yellow']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
