<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use core\helpers\CommonHelper;
use core\helpers\RbacHelper;
use kartik\select2\Select2;
use core\helpers\LanguagesHelper;


/* @var $this yii\web\View */
/* @var $model \core\forms\backend\AdminCreateForm */

$this->title = Yii::t('app', 'Create Admin');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-create white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <div class="admin-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'lang')->dropDownList(LanguagesHelper::getList()) ?>

        <?= $form->field($model, 'time_zone')->dropDownList(CommonHelper::getTimeZones()) ?>

        <?= $form->field($model, 'roles')->widget(Select2::class, [
            'data' => RbacHelper::getRolesDropdown(),
            'size' => 'lg',
            'options' => [
                'placeholder' => 'Select roles...',
                'multiple' => true
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-own btn-yellow']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
