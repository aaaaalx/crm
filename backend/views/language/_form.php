<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \core\forms\backend\LanguageForm */

?>

<div class="col-md-6">
    <div class="box box-primary">
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'iso')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-own btn-yellow']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>