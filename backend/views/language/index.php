<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\helpers\LanguagesHelper;

/* @var $this yii\web\View */
/* @var $searchModel \core\forms\search\LanguageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Languages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Language', ['create'], ['class' => 'btn btn-success btn-own btn-yellow pull-left']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'iso',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    /**
                     * @var \core\entities\Language $model
                     */
                    return LanguagesHelper::getStatusLabel($model->status);
                }
            ],
            'ctime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {restore}',
                'buttons' =>
                    [
                        'delete' => function ($url, $model) {
                            /**
                             * @var \core\entities\Language $model
                             */
                            return $model->isActive() ? Html::a('<span class="glyphicon glyphicon-trash"></span>', \yii\helpers\Url::to(['/language/delete', 'id' => $model->id]), [
                                'title' => Yii::t('yii', 'Delete language')
                            ]) : null;
                        },
                        'restore' => function ($url, $model) {
                            /**
                             * @var \core\entities\Language $model
                             */
                            return $model->isDeleted() ? Html::a('<span class="fa fa-retweet"></span>', \yii\helpers\Url::to(['/language/restore', 'id' => $model->id]), [
                                'title' => Yii::t('yii', 'Restore language')
                            ]) : null;
                        }
                    ]
            ],
        ],
    ]); ?>
</div>
