<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \core\forms\backend\LanguageForm */

$this->title = Yii::t('app', 'Create Language');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create white-box">
    <div class="row">
        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
        <?php

        echo $this->render('_form', [
            'model' => $model
        ]);

        ?>
    </div>
</div>