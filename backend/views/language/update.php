<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \core\forms\backend\LanguageForm */
/* @var $lang \core\entities\Language */

$this->title = Yii::t('app', 'Update Language: {nameAttribute}', [
    'nameAttribute' => $lang->iso,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $lang->id, 'url' => ['view', 'id' => $lang->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-update white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?php
    echo $this->render('_form', [
        'model' => $model
    ]);
    ?>

</div>