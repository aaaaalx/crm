<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use core\helpers\TaskTypeHelper;

/* @var $this yii\web\View */
/* @var $model core\forms\backend\TaskTypeForm */

$this->title = Yii::t('app', 'Create Task type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-create">

    <div class="task-type-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(TaskTypeHelper::getStatuses()) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>
