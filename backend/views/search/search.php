<?php

use core\others\MainSearch;
use backend\widgets\search\SearchWidget;

/* @var $this yii\web\View */
/* @var $search MainSearch */

echo SearchWidget::widget([
    'doctors' => $search->doctors,
    'orders' => $search->orders,
    'cards' => $search->cards
]);

