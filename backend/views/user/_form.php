<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use core\helpers\CommonHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model \core\forms\common\UserForm */

?>

<div class="col-md-6">
    <div class="box box-primary">
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'timeZone')->widget(Select2::class, [
                'data' => CommonHelper::getTimeZones(),
                'options' => [
                    'placeholder' => 'Select time zone...'
                ],
                'size' => 'lg',
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save and next step'), ['class' => 'btn btn-success btn-own btn-yellow']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
