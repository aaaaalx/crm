<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\helpers\UserStatusHelper;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success btn-own btn-yellow pull-left']) ?>
    </p>
    <div class="clearfix"></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    /**
                     * @var \core\entities\User $model
                     */
                    return UserStatusHelper::getStatusLabel($model->status);
                }
            ],
            'time_zone',
            'ctime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {activate} {moderate} {role}',
                'buttons' =>
                    [
                        'delete' => function ($url, $model) {
                            /**
                             * @var \core\entities\User $model
                             */
                            return $model->isCanBeDeleted() ? Html::a('<span class="glyphicon glyphicon-trash"></span>', \yii\helpers\Url::to(['/user/delete', 'id' => $model->id]), [
                                'title' => Yii::t('yii', 'Delete user')
                            ]) : null;
                        },
                        'activate' => function ($url, $model) {
                            /**
                             * @var \core\entities\User $model
                             */
                            return $model->isCanBeActivated() ? Html::a('<span class="fa fa-thumbs-up"></span>', \yii\helpers\Url::to(['/user/activate', 'id' => $model->id]), [
                                'title' => Yii::t('yii', 'Activate user')
                            ]) : null;
                        },
                        'moderate' => function ($url, $model) {
                            /**
                             * @var \core\entities\User $model
                             */
                            return $model->isCanBeModerated() ? Html::a('<span class="fa fa-thumbs-down"></span>', \yii\helpers\Url::to(['/user/moderate', 'id' => $model->id]), [
                                'title' => Yii::t('yii', 'Send to moderate user')
                            ]) : null;
                        },
                        'role' => function ($url, $model){
                            return Html::a('<span class="fa fa-user-secret"></span>', \yii\helpers\Url::to(['user/assign-role', 'id' => $model->id]), [
                                'title' => Yii::t('yii', 'Assign role')
                            ]);
                        },
                    ]
            ],
        ],
    ]); ?>
</div>
