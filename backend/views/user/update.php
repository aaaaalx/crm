<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \core\forms\common\UserForm */
/* @var $user \core\entities\User */

$this->title = Yii::t('app', 'Update User: {nameAttribute}', [
    'nameAttribute' => $user->email,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->id, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-update white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?php
        echo $this->render('_form', [
            'model' => $model
        ]);
    ?>

</div>
