<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use core\helpers\CommonHelper;


/* @var $this yii\web\View */
/* @var $model \core\forms\common\UserForm */

$this->title = Yii::t('app', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create white-box">
    <div class="row">
        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
        <?php

        echo $this->render('_form', [
            'model' => $model
        ]);

        ?>
    </div>
</div>
