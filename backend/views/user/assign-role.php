<?php

use \yii\widgets\ActiveForm;
use \core\helpers\UserHelper;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \core\forms\backend\UserRoleForm */

$this->title = Yii::t('app', 'Assign Role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="user-role white-box">
    <?php $form = ActiveForm::begin();?>

    <?= $form->field($model, 'role_id')->dropDownList(UserHelper::roleList(), [
            'options' => [UserHelper::getRoleId($model->user_id) => ['Selected'=>'selected']],
            'prompt' => Yii::t('app', 'Select role...')
    ])?>

    <?= Html::submitButton(Yii::t('app', 'Save'));?>

    <?php ActiveForm::end()?>
</div>
