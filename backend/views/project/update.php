<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use core\helpers\ProjectsHelper;
use kartik\datetime\DateTimePicker;
use core\helpers\TagsHelper;
use core\helpers\RolesHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model core\entities\Project */

$this->title = Yii::t('app', 'Update Project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if($project->id){
    $script = "var project_id = $project->id";
    $this->registerJs($script, yii\web\View::POS_READY);
}
?>
    <div class="project">

        <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>

        <div class="col-sm-3">

            <div class="project-form">

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textArea(['maxlength' => true]) ?>

                <?= $form->field($model, 'access_type')->dropDownList(ProjectsHelper::getAccesses()) ?>

                <?= $form->field($model, 'image')->widget(FileInput::class, [
                    'options' => [],
                    'pluginOptions' => [
                        'deleteUrl' => Url::toRoute(['project/remove-image', 'id'=>$project->id]),
                        'initialPreview'=> $project->image?Yii::$app->params['apiUrl'] .'/uploads/projects/'.$project->id.'/'.$project->image:"",
                        'initialPreviewAsData' => true,
                        'previewFileType' => 'image',
                        'maxFileSize'=>2800,
                        'overwriteInitial' => true,
                        'showCaption' => false,
                        'showRemove' => true,
                        'showUpload' => false,
                        'showCancel' => false,
                        'removeIcon' => '<i class="glyphicon glyphicon-remove"></i>',
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
                        'browseLabel' =>  Yii::t('app','Add photo...'),
                        'layoutTemplates'=>  ' {remove} {browse}',
                    ]
                ]); ?>

                <?= $form->field($model, 'stime')->widget(DateTimePicker::class,[
                    'options' => ['placeholder' => Yii::t('app', 'Select start time')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii'
                    ]
                ]) ?>

                <?= $form->field($model, 'etime')->widget(DateTimePicker::class,[
                    'options' => ['placeholder' => Yii::t('app', 'Select end time')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii'
                    ]
                ]) ?>

                <?= $form->field($model, 'status')->dropDownList(ProjectsHelper::getStatuses()) ?>

            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

        </div>

        <div class="col-sm-3">

            <div class="project-crew">

                <h3><?= Yii::t('app', 'Add users') ?></h3>

                <div class="form-group fields-container-wrap">
                    <div class="fields-container">

                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_users', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            'limit' => 100, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $model->crew[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'project_id',
                                'user_id',
                                'role_id',
                            ],
                        ]);

                        ?>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-users"></i><?= Yii::t('app', 'Users') ?>
                                <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i><?= Yii::t('app', 'Add user') ?></button>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-body container-items"><!-- widgetContainer -->
                                <?php foreach ($model->crew as $index => $user): ?>
                                    <div class="item panel panel-default"><!-- widgetBody -->

                                        <div class="panel-heading">
                                            <span class="panel-title"><?= Yii::t('app', 'User') ?>: <?= ($index+1) ?></span>
                                            <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body">
                                            <?php
                                            // necessary for update action.
                                            if ($user->isNewRecord) {
                                                echo Html::activeHiddenInput($user, "[{$index}]id");
                                            }
                                            $url = Url::to(['user/get-users']);
                                            ?>
                                            <?= $form->field($user, "[{$index}]user_id")->widget(Select2::class, [
                                                'data'=> [$user->crew->user->id => $user->crew->user->email],
                                                'name' => 'user_id',
                                                'options' => [
                                                    'placeholder' => Yii::t('app', 'Add user'),
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => false,
                                                    'minimumInputLength' => 0,
                                                    'maximumInputLength' => 20,
                                                    'language' => [
                                                        'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                                    ],
                                                    'ajax' => [
                                                        'url' => $url,
                                                        'dataType' => 'json',
                                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                                    ],
                                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                                    'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                                ]
                                            ]) ?>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <?= $form->field($user, "[{$index}]role_id")->dropDownList($model->getRoles(), ['class'=>'form-control']) ?>
                                                </div>
                                            </div><!-- end:row -->
                                        </div>

                                    </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                        <?php DynamicFormWidget::end(); ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-3">
            <div class="project-options">

                <h3><?= Yii::t('app', 'Add options') ?></h3>

                <div class="form-group fields-container-wrap">
                    <div class="fields-container">

                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_options', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items_options', // required: css class selector
                            'widgetItem' => '.item_options', // required: css class
                            'limit' => 100, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $model->options[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'option_id',
                                'value',
                            ],
                        ]);

                        ?>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-ellipsis-v"></i><?= Yii::t('app', 'Options') ?>
                                <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i><?= Yii::t('app', 'Add option') ?></button>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-body container-items_options"><!-- widgetContainer -->
                                <?php foreach ($model->options as $index => $optionItem): ?>
                                    <div class="item_options panel panel-default"><!-- widgetBody -->

                                        <div class="panel-heading">
                                            <span class="panel-title"><?= Yii::t('app', 'Option') ?>: <?= ($index+1) ?></span>
                                            <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body">
                                            <?php
                                            // necessary for update action.
                                            if ($optionItem->isNewRecord) {
                                                echo Html::activeHiddenInput($optionItem, "[$index]id");
                                            }
                                            $url = Url::to(['options/get-options']);

                                            $data_id = $optionItem->projectOptions->option->id??null;
                                            $data_name = $optionItem->projectOptions->option->name??null;
                                            $data = [$data_id => $data_name];
                                            ?>
                                            <?= $form->field($optionItem, "[$index]option_id")->widget(Select2::class, [
                                                'name' => 'option_id',
                                                'data'=> $data,
                                                'options' => [
                                                    'placeholder' => Yii::t('app', 'Add option'),
                                                    'class' => 'options',
                                                    'data-csrf' => Yii::$app->getRequest()->getCsrfToken(),
                                                    'data-relation' => 'relatedValue'
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => false,
                                                    'minimumInputLength' => 0,
                                                    'maximumInputLength' => 20,
                                                    'language' => [
                                                        'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                                    ],
                                                    'ajax' => [
                                                        'url' => $url,
                                                        'dataType' => 'json',
                                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                                    ],
                                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                                    'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                                ],
                                            ]) ?>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <?= $form->field($optionItem, "[$index]value")->hiddenInput(['class'=>'relatedValue'])->label(false) ?>
                                                </div>
                                            </div><!-- end:row -->
                                        </div>

                                    </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                        <?php DynamicFormWidget::end(); ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-3">
            <div class="project-tags">

                <h3><?= Yii::t('app', 'Add tags') ?></h3>

                <div class="form-group fields-container-wrap">
                    <div class="fields-container">

                        <?php
                        $url = Url::to(['tag/get-tags']);
                        echo $form->field($model->tags, "name")->widget(Select2::class, [
                            'language' => Yii::$app->language,
                            'maintainOrder' => false,
                            'toggleAllSettings' => [
                                'selectLabel' => '',
                                'unselectLabel' => '',
                            ],
                            'options' => [
                                'placeholder' => Yii::t('app', 'Add tag'),
                                'multiple' => true
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                'minimumInputLength' => 1,
                                'maximumInputLength' => 20,
                                'tags' => true,
                                'tokenSeparators' => [',', ' ', '\n'],
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                            ]
                        ]) ?>

                    </div>
                </div>

            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$js = '
jQuery(".dynamicform_wrapper_users").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper_users .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'User').': " + (index + 1))
    });
});
jQuery(".dynamicform_wrapper_users").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper_users .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'User').': " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper_options").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper_options .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'Option').': " + (index + 1))
           jQuery(".kv-plugin-loading").remove();
    });
});

jQuery(".dynamicform_wrapper_options").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper_options .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'Option').': " + (index + 1))
    });
});
';
$this->registerJs($js);
?>