<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\helpers\TaskHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description',
            'access_type',
            'image',
            //'stime',
            //'etime',
            [
                'attribute' => 'status',
                'label' => Yii::t('app','Status'),
                'format' => 'raw',
                'filter' => TaskHelper::getStatuses(),
                'value'=> function ($data){
                    $statusArr = TaskHelper::getStatuses();
                    return Html::dropDownList(
                        'status',
                        isset($statusArr[$data->status])?$data->status:null,
                        $statusArr,
                        [
                            'data-csrf'=>Yii::$app->getRequest()->getCsrfToken(),
                            'data-id'=>$data->id,
                            'data-url'=>Url::to(['project/change-status']),
                            'class'=>'form-control changeStatus'
                        ]);
                },
            ],
            //'ctime',
            //'utime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
