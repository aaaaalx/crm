<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \core\helpers\OptionsHelper;

/* @var $this yii\web\View */
/* @var $model core\entities\Options */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="options-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-5 white-box">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type')->dropDownList(OptionsHelper::typeList(), ['id' => 'list-type']) ?>
        </div>
        <div class="col-md-7 white-box block-variants" style="display: none">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Variants</label>
                                <div id="default-container"></div>
                                <?= Html::textInput('addVariant', null, ['class' => 'form-control form-control-own', 'id' => 'newVariant']); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Html::button('+', ['id' => 'add', 'class' => 'btn btn-primary btn-own blue', 'style' => 'width: 100px; margin-top: 24px;']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inputs">
                <?php if (!empty($model->variants)):?>
                    <?php foreach ($model->variants as $variant):?>
                        <a class="btn btn-primary select-btn" style="margin-right: 10px; margin-bottom: 10px;"><?= $variant ?><i data-variant="<?= $variant ?>" class="remove glyphicon glyphicon-remove"></i></a>
                    <?php endforeach;?>
                <?php endif;?>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJsFile('/js/add-input.js', ['depends' => [\yii\web\JqueryAsset::className()]]);