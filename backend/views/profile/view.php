<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use core\helpers\ProfileHelper;

/* @var $this yii\web\View */
/* @var $model core\entities\Profile */

$this->title = Yii::t('app', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

<?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'label' => Yii::t('app', 'Avatar'),
            'attribute' => 'avatar',
            'value' => Html::img(Yii::$app->params['apiUrl'] . '/uploads/profiles/' . $model->id . '/' . $model->avatar, ['style' => 'width:300px']),
            'format' => 'raw',
        ],
        'first_name',
        'patronymic',
        'surname',
        'phone',
        [
            'label' => Yii::t('app', 'Position'),
            'attribute' => 'position_id',
            'value' => function ($model) {
                return ProfileHelper::getPosition($model->position_id);
            }
        ],
        'additional_email',
        [
            'label' =>  Yii::t('app', 'Language'),
            'attribute' => 'lang_id',
            'value' => function ($model) {
                return ProfileHelper::getLng($model->lang_id);
            }
        ],
        'country',
        'city',
        'time_zone',
        'ctime',
        'utime'

    ],
]);

