<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \core\helpers\ProfileHelper;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\search\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Profiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a(Yii::t('app', 'Create Profile'), ['create'], ['class' => 'btn btn-success btn-own btn-yellow pull-left']) ?>
    </p>
    <div class="clearfix"></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'label' => Yii::t('app', 'User email'),
                'attribute' => 'email',
                'value' => function($model){
                    return ArrayHelper::getValue(ProfileHelper::getEmail($model->user_id), $model->user_id);
                }
            ],
            [
                'label' => Yii::t('app', 'avatar'),
                'attribute' => 'avatar',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::img(Yii::$app->params['apiUrl'] .'/uploads/profiles/'.$model->id.'/'.$model->avatar, ['style' => 'width:150px']);
                }
            ],
            [
                'label' => Yii::t('app', 'First name'),
                'attribute' => 'first_name'
            ],
            [
                'label' => Yii::t('app', 'Surname'),
                'attribute' => 'surname'
            ],
            [
                'label' => Yii::t('app', 'Phone'),
                'attribute' => 'phone'
            ],
            [
                'label' => Yii::t('app', 'Gender'),
                'attribute' => 'gender',
                'value' => function($model){
                    return ProfileHelper::getGender($model->gender);
                },
                'filter' => ProfileHelper::genderList(),
            ],
            [
                'label' => Yii::t('app', 'Position'),
                'attribute' => 'position_id',
                'value' => function($model){
                    return ProfileHelper::getPosition($model->position_id);
                },
                'filter' => ProfileHelper::positionList(),
            ],
            [
                'label' => Yii::t('app', 'Status'),
                'attribute' => 'status',
                'value' => function($model) {
                    return ProfileHelper::getStatus($model->status);
                },
                'filter' => ProfileHelper::statusList()
            ],

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
