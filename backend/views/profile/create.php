<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use core\helpers\ProfileHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use core\helpers\CommonHelper;
use \kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model \core\forms\backend\ProfileCreateForm */

$this->title = Yii::t('app', 'Create Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-create box box-primary">
    <div class="box-body box-profile">
        <div class="row">
            <div class="col-md-10">
                <?php $form = ActiveForm::begin();?>

                <?= $form->field($model, 'avatar')->widget(FileInput::classname(), [
                    'options' => [],
                    'pluginOptions' => [
                        'initialPreviewAsData'=> true,
                        'previewFileType' => 'image',
                        //'showUpload' => false,
                        'maxFileSize'=>2800,
                        'overwriteInitial' => true,
                        'showCaption' => false,
                        'showRemove' => true,
                        'showUpload' => false,
                        'showCancel' => false,
                        'removeIcon' => '<i class="glyphicon glyphicon-remove"></i>',
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
                        'browseLabel' =>  'Добавить фото...',
                        'layoutTemplates'=>  ' {remove} {browse}',
                    ]
                ]);?>

                <?= $form->field($model, 'user_id')->widget(Select2::class, [
                    'options' => ['placeholder' => 'Select user ...'],
                    'data' => $model->user_id ? ProfileHelper::getEmail($model->user_id):null,
                    'pluginOptions' => [
                        'minimumInputLength' => 3,
                        'allowClear' => true,
                        'ajax' => [
                            'url' => Url::to(['/profile/search-users']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q: params.term}; }'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.text; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                    ],

                ])->label('Привязать к пользователю');?>

                <?= $form->field($model, 'first_name')->textInput()->label('Имя')?>

                <?= $form->field($model, 'surname')->textInput()->label('Фамилия')?>

                <?= $form->field($model, 'patronymic')->textInput()->label('Отчество')?>

                <?= $form->field($model, 'gender')->dropDownList(ProfileHelper::genderList())->label('Пол')?>

                <?= $form->field($model, 'phone')->textInput()->label('Телефон')?>

                <?= $form->field($model, 'additional_email')->textInput()->label('Доп.почта')?>

                <?= $form->field($model, 'position_id')->dropDownList(ProfileHelper::positionList())->label('Должность')?>

                <?= $form->field($model, 'country')->textInput()->label('Страна')?>

                <?= $form->field($model, 'city')->textInput()->label('Город')?>

                <?= $form->field($model, 'lang_id')->dropDownList(ProfileHelper::lngList())->label('Язык')?>

                <?= $form->field($model, 'time_zone')->widget(Select2::class, [
                    'data' => CommonHelper::getTimeZones(),
                    'options' => [
                        'placeholder' => 'Select time zone...'
                    ],
                    'size' => 'lg',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

                <?= Html::submitButton('Сохранить', ['class' => 'btn btn btn-danger'])?>

                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>