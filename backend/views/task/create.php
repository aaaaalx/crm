<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use core\helpers\TaskHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;
use dosamigos\ckeditor\CKEditor;
use core\entities\Task;
use core\helpers\TaskTypeHelper;

/* @var $this yii\web\View */
/* @var $model core\forms\backend\TaskForm */

$this->title = Yii::t('app', 'Create Task');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-create">

    <div class="task-form">

        <?php $form = ActiveForm::begin(['id' => 'dynamic-form', 'options'=>['enctype'=>'multipart/form-data']]); ?>

        <div class="row">
            <div class="col-md-12">

                <div class="col-lg-12 col-md-12">
                <?= $form->field($model, 'title')->textInput() ?>
                <?= $form->field($model, 'desc')->widget(CKEditor::class, [
                    'options' => ['rows' => 10],
                    'preset' => 'basic'
                ]) ?>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="row">

                        <div class="col-lg-4 col-md-12">
                            <?= $form->field($model, 'stime')->widget(DateTimePicker::class,[
                                'options' => ['placeholder' => Yii::t('app', 'Select start time')],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd hh:ii'
                                ]
                            ]) ?>
                        </div>

                        <div class="col-lg-4 col-md-12">
                            <?= $form->field($model, 'etime')->widget(DateTimePicker::class,[
                                'options' => ['placeholder' => Yii::t('app', 'Select end time')],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd hh:ii'
                                ]
                            ]) ?>
                        </div>

                        <div class="col-lg-4 col-md-12">
                            <?= $form->field($model, 'remember_time')->widget(DateTimePicker::class,[
                                'options' => ['placeholder' => Yii::t('app', 'Select remember time')],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd hh:ii'
                                ]
                            ]) ?>
                        </div>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="row">

                        <div class="col-lg-2 col-md-12">
                            <?php
                            $url = Url::to(['user/get-users']);
                            ?>
                            <?= $form->field($model, 'user_id')->widget(Select2::class, [
                                'name' => 'user_id',
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Add user'),
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 0,
                                    'maximumInputLength' => 20,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                    'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                ]
                            ]) ?>
                        </div>

                        <div class="col-lg-2 col-md-12">
                            <?php
                            $url = Url::to(['project/get-projects']);
                            ?>
                            <?= $form->field($model, 'project_id')->widget(Select2::class, [
                                'name' => 'project_id',
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Add project'),
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 0,
                                    'maximumInputLength' => 20,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                    'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                ]
                            ]) ?>
                        </div>

                        <div class="col-lg-2 col-md-12">
                            <?= $form->field($model, 'status')->dropDownList(TaskHelper::getStatuses()) ?>
                        </div>

                        <div class="col-lg-2 col-md-12">
                            <?= $form->field($model, 'priority')->dropDownList(Task::PRIORITY) ?>
                        </div>

                        <div class="col-lg-2 col-md-12">
                            <?= $form->field($model, 'type_id')->dropDownList(TaskTypeHelper::getTypes()) ?>
                        </div>

                    </div>
                </div>

                <div class="col-md-12 col-mg-12">
                    <div class="row">

                        <div class="col-lg-4 col-md-12">
                            <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper_users', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items', // required: css class selector
                                'widgetItem' => '.item', // required: css class
                                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $model->crew[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'user_id',
                                    'role',
                                ],
                            ]);

                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-users"></i> <?= Yii::t('app', 'Users') ?>
                                    <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= Yii::t('app', 'Add user') ?></button>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="panel-body container-items"><!-- widgetContainer -->
                                    <?php foreach ($model->crew as $index => $user): ?>
                                        <div class="item panel panel-default"><!-- widgetBody -->

                                            <div class="panel-heading">
                                                <span class="panel-title"><?= Yii::t('app', 'User') ?>: <?= ($index+1) ?></span>
                                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="panel-body">
                                                <?php
                                                // necessary for update action.
                                                if ($user->isNewRecord) {
                                                    echo Html::activeHiddenInput($user, "[{$index}]id");
                                                }
                                                $url = Url::to(['user/get-users']);
                                                ?>
                                                <?= $form->field($user, "[{$index}]user_id")->widget(Select2::class, [
                                                    'name' => 'user_id',
                                                    'options' => [
                                                        'placeholder' => Yii::t('app', 'Add user'),
                                                    ],
                                                    'pluginOptions' => [
                                                        'allowClear' => false,
                                                        'minimumInputLength' => 0,
                                                        'maximumInputLength' => 20,
                                                        'language' => [
                                                            'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                                        ],
                                                        'ajax' => [
                                                            'url' => $url,
                                                            'dataType' => 'json',
                                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                                        ],
                                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                        'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                                        'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                                    ]
                                                ]) ?>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <?= $form->field($user, "[{$index}]role")->dropDownList(TaskHelper::getRoles(), ['class'=>'form-control']) ?>
                                                    </div>
                                                </div><!-- end:row -->
                                            </div>

                                        </div>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                            <?php DynamicFormWidget::end(); ?>
                        </div>

                        <div class="col-lg-4 col-md-12">
                            <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper_relations', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items_relations', // required: css class selector
                                'widgetItem' => '.item_relations', // required: css class
                                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $model->relations[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'related_task_id',
                                    'relation_type',
                                ],
                            ]);

                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-random"></i> <?= Yii::t('app', 'Relations') ?>
                                    <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= Yii::t('app', 'Add relation') ?></button>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="panel-body container-items_relations"><!-- widgetContainer -->
                                    <?php foreach ($model->relations as $index => $relation): ?>
                                        <div class="item_relations panel panel-default"><!-- widgetBody -->

                                            <div class="panel-heading">
                                                <span class="panel-title"><?= Yii::t('app', 'Relation') ?>: <?= ($index+1) ?></span>
                                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="panel-body">
                                                <?php
                                                // necessary for update action.
                                                if ($relation->isNewRecord) {
                                                    echo Html::activeHiddenInput($relation, "[{$index}]id");
                                                }
                                                $url = Url::to(['task/get-relations']);
                                                ?>
                                                <?= $form->field($relation, "[{$index}]related_task_id")->widget(Select2::class, [
                                                    'name' => 'related_task_id',
                                                    'options' => [
                                                        'placeholder' => Yii::t('app', 'Add user'),
                                                    ],
                                                    'pluginOptions' => [
                                                        'allowClear' => false,
                                                        'minimumInputLength' => 0,
                                                        'maximumInputLength' => 20,
                                                        'language' => [
                                                            'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                                        ],
                                                        'ajax' => [
                                                            'url' => $url,
                                                            'dataType' => 'json',
                                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                                        ],
                                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                        'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                                        'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                                    ]
                                                ]) ?>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <?= $form->field($relation, "[{$index}]relation_type")->dropDownList(TaskHelper::getRelationsTypes(), ['class'=>'form-control']) ?>
                                                    </div>
                                                </div><!-- end:row -->
                                            </div>

                                        </div>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                            <?php DynamicFormWidget::end(); ?>
                        </div>

                        <div class="col-lg-4 col-md-12">
                            <?php
                            $url = Url::to(['tag/get-tags']);
                            echo $form->field($model->tags, "name")->widget(Select2::class, [
                                'attribute' => Yii::$app->language,
                                'name' => 'name',
                                'maintainOrder' => false,
                                'toggleAllSettings' => [
                                    'selectLabel' => '',
                                    'unselectLabel' => '',
                                ],
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Add tag'),
                                    'multiple' => true
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                    'minimumInputLength' => 0,
                                    'maximumInputLength' => 20,
                                    'tags' => true,
                                    'tokenSeparators' => [',', ' ', '\n'],
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                                    'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                ]
                            ])->label(Yii::t('app', 'Tags')) ?>
                        </div>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="row">

                        <div class="col-lg-6 col-md-12">
                            <?= $form->field($model->files, 'file[]')->widget(FileInput::class, [
                                'options' => [
                                    'multiple' => true,
                                    //'accept' => 'image/*'
                                ],
                                'pluginOptions' => [
                                    'initialPreviewAsData'=> true,
                                    'initialPreviewShowDelete' => true,
                                    //'previewFileType' => 'any',
                                    'maxFileSize' => 2800,
                                    'overwriteInitial' => true,
                                    'showCaption' => false,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'removeIcon' => '<i class="glyphicon glyphicon-remove"></i>',
                                    'browseClass' => 'btn btn-primary btn-block',
                                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
                                    'browseLabel' =>  Yii::t('app','Add file...'),
                                    'layoutTemplates'=> '{remove}{browse}',
                                    'showCancel' => false
                                ]
                            ]); ?>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper_list', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items_list', // required: css class selector
                                'widgetItem' => '.item_list', // required: css class
                                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $model->list[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'text',
                                    'status',
                                ],
                            ]);

                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-server"></i> <?= Yii::t('app', 'Checklist') ?>
                                    <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= Yii::t('app', 'Add checklist') ?></button>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="panel-body container-items_list"><!-- widgetContainer -->
                                    <?php foreach ($model->list as $index => $item): ?>
                                        <div class="item_list panel panel-default"><!-- widgetBody -->

                                            <div class="panel-heading">
                                                <span class="panel-title"><?= Yii::t('app', 'Checklist') ?>: <?= ($index+1) ?></span>
                                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="panel-body">
                                                <?php
                                                // necessary for update action.
                                                if ($item->isNewRecord) {
                                                    echo Html::activeHiddenInput($item, "[{$index}]id");
                                                }
                                                $url = Url::to(['project/get-users']);
                                                ?>
                                                <?= $form->field($item, "[{$index}]text")->textarea() ?>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <?= $form->field($item, "[{$index}]status")->dropDownList($item::getStatus(), ['class'=>'form-control']) ?>
                                                    </div>
                                                </div><!-- end:row -->
                                            </div>

                                        </div>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                            <?php DynamicFormWidget::end(); ?>
                        </div>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12 form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>

            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$js = '
jQuery(".dynamicform_wrapper_users").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper_users .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'User').': " + (index + 1));
        jQuery(".kv-plugin-loading").remove();
    });
});
jQuery(".dynamicform_wrapper_users").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper_users .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'User').': " + (index + 1));
    });
});
jQuery(".dynamicform_wrapper_relations").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper_relations .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'Relation').': " + (index + 1));
        jQuery(".kv-plugin-loading").remove();
    });
});
jQuery(".dynamicform_wrapper_relations").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper_relations .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'Relation').': " + (index + 1));
    });
});
jQuery(".dynamicform_wrapper_list").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper_list .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'Checklist').': " + (index + 1));
        jQuery(".kv-plugin-loading").remove();
    });
});
jQuery(".dynamicform_wrapper_list").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper_list .panel-title").each(function(index) {
        jQuery(this).html("'.Yii::t('app', 'Checklist').': " + (index + 1));
    });
});
';
$this->registerJs($js);
?>


</div>
