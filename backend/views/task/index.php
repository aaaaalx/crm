<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\helpers\TaskHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\search\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Task');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Task'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'status',
                'label' => Yii::t('app','Status'),
                'format' => 'raw',
                'filter' => TaskHelper::getStatuses(),
                'value'=> function ($data){
                    $statusArr = TaskHelper::getStatuses();
                    return Html::dropDownList(
                        'status',
                        isset($statusArr[$data->status])?$data->status:null,
                        $statusArr,
                        [
                            'data-csrf'=>Yii::$app->getRequest()->getCsrfToken(),
                            'data-id'=>$data->id,
                            'data-url'=>Url::to(['task/change-status']),
                            'class'=>'form-control changeStatus'
                        ]);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'header' => Yii::t('app', 'action')
            ],
        ],
    ]); ?>
</div>
