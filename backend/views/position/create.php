<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \core\forms\backend\PositionForm */

$this->title = Yii::t('app', 'Create Position');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-create white-box">
    <div class="row">
        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
        <?php

        echo $this->render('_form', [
            'model' => $model
        ]);

        ?>
    </div>
</div>
