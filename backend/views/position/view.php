<?php
use yii\widgets\DetailView;
?>

<div class="box box-info">
    <div class="box box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'title',
                'description',
                'status',
                'ctime'
            ]
        ]);?>
    </div>
</div>
