<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use core\helpers\PositionHelper;

/* @var $this yii\web\View */
/* @var $model \core\forms\backend\PositionForm */

?>

<div class="col-md-10">

    <div class="box box-primary">
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput() ?>

            <?= $form->field($model, 'description')->textInput() ?>

            <?= $form->field($model, 'status')->dropDownList(PositionHelper::statusList())?>
        </div>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
