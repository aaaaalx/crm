<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\helpers\PositionHelper;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Positions');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="admin-index white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a(Yii::t('app', 'Create Position'), ['create'], ['class' => 'btn btn-success pull-left']) ?>
    </p>
    <div class="clearfix"></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'label' => 'title',
                'attribute' => 'title'
            ],
            [
                'label' => 'status',
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model){
                    return PositionHelper::getStatus($model->status);
                }
            ],
            [
                'label' => 'description',
                'attribute' => 'description'
            ],
            'ctime',

            [
                'class' => 'yii\grid\ActionColumn',
            ],

        ],
    ]); ?>
</div>
