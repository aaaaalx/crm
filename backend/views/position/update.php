<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \core\forms\backend\PositionForm */
/* @var $user \core\entities\User */

$this->title = Yii::t('app', 'Update Position: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="position-update white-box">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?php
        echo $this->render('_form', [
            'model' => $model
        ]);
    ?>

</div>
