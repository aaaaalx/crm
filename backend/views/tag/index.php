<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\helpers\TagsHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\search\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tags');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Tag'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'status',
                'label' => Yii::t('app','Status'),
                'format' => 'raw',
                'filter' => TagsHelper::getStatuses(),
                'value'=> function ($data){
                    $statusArr = TagsHelper::getStatuses();
                    return Html::dropDownList(
                        'status',
                        isset($statusArr[$data->status])?$data->status:null,
                        $statusArr,
                        [
                            'data-csrf'=>Yii::$app->getRequest()->getCsrfToken(),
                            'data-id'=>$data->id,
                            'data-url'=>Url::to(['tag/change-status']),
                            'class'=>'form-control changeStatus'
                        ]);
                },
            ],
            'ctime',
            'utime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
</div>
