<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use core\helpers\TagsHelper;

/* @var $this yii\web\View */
/* @var $model core\forms\backend\TagForm */

$this->title = Yii::t('app', 'Update Tag: ' . $model->name, [
    'nameAttribute' => '' . $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="tag-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="tag-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(TagsHelper::getStatuses()) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
