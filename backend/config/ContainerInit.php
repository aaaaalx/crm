<?php

namespace backend\config;

use yii\base\BootstrapInterface;
use yii\rbac\DbManager;
use yii\rbac\ManagerInterface;

class ContainerInit implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->setSingleton(ManagerInterface::class, DbManager::class);
    }
}