<?php
return [
    'adminEmail' => 'admin@example.com',
    'bsVersion' => '4.x',
    'apiUrl' => getenv('API_URL')
];
