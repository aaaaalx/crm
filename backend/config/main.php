<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'backend\config\ContainerInit', 'common\config\BootUp'],
    'name' => 'CRM',
    'modules' => [
        'permit' => [
            'class' => 'developeruz\db_rbac\Yii2DbRbac',
            'params' => [
                'accessRoles' => ['administrator']
            ]
        ],
        'i18n' => Zelenin\yii\modules\I18n\Module::class,
        'wiki'=>[
            'class'=>'asinfotrack\yii2\wiki\Module',
            'processContentCallback'=>function($content) {
                //example if you want to use markdown in your wiki
                $Parsedown = new Parsedown();
                return $Parsedown->text($content);
            }
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'backend\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl'=>['/auth/login']
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'signup' => 'auth/signup',
                'login' => 'auth/login',
                'dashboard' => 'site/index',
                '<module:\w+>/<controller:\w+>/<action:(\w|-)+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:(\w|-)+>/<id:\d+>' => '<module>/<controller>/<action>',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ]
    ],
    'as AccessBehavior' => [
        'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
        'redirect_url' => '/dashboard',
        'login_url' => '/login',
        'rules' =>
            [
                'auth' =>
                    [
                        [
                            'actions' => ['login', 'logout'],
                            'allow' => true,
                        ],
                    ],
                'site' =>
                    [
                        [
                            'actions' => ['index'],
                            'allow' => '@',
                        ]
                    ]
            ]
    ],
    'params' => $params,
];
