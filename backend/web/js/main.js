function setTimeZone(UserTimeZone) {
    var current_time_zone = -(new Date().getTimezoneOffset() / 60);
    if(UserTimeZone != current_time_zone) {
        $.ajax({
            url: '/admin/set-timezone',
            type: 'post',
            data: {'timezone' : current_time_zone}
        });
    }
}

$(document).ready(function(){
    //change status
    $(document).on('change', '.changeStatus', function(){
        var id = $(this).data('id');
        var status = $(this).val();
        var csrf = $(this).data('csrf');
        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'post',
            data: {
                id: id,
                status: status,
                _csrf : csrf
            },
            success: function (data){

            }
        });
    });

    $(document).on('change', '.options', function(e){
        insertOption(e);
    });

    $('.options').change();

});

function insertOption(result){
    var caller = $(result.target);
    var parent = caller.parents('.panel-body').eq(0);
    var relatedEl = parent.find('.'+caller.data('relation'));
    var name = relatedEl.attr('name');
    var id = relatedEl.attr('id');
    var option_id = caller.val();
    var projectID = (typeof project_id !== 'undefined')?project_id:null;
    var options = {class:'relatedValue'};
    var csrf = result.target.attributes['data-csrf'].nodeValue;
    var data = {csrf:csrf,field_id:id,field_name:name,option_id:option_id,options:options,project_id:projectID}
    $.ajax({
        url: '/project/get-option-output',
        type: 'post',
        data: data,
        success: function (data){
            if(data){
                var html = $(data);
                var el = $('#'+id);
                html.val(el.val());
                el.replaceWith(html);
            }
        }
    });
}