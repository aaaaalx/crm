
$(document).ready(function(){
    let variants = [];

    let chips = $('.remove');
    if(chips.length) {
        for(let chip of chips) {
            variants.push($(chip).attr('data-variant'));
        }
    }

    $('#add').click(function() {
        let variantEl = $('#newVariant');
        let variant = variantEl.val();

        if (variant.length) {
            variants.push(variant);
            variantEl.val("");
            renderChips(variant);
            createSelectOptions(variants);
        }
    });

    $(document).on('click', '.remove', function () {
        $(this).parent().remove();
        variants.splice(variants.indexOf($(this).attr('data-variant')), 1);
        if (variants.length) {
            createSelectOptions(variants);
        }
    });

    function renderChips(variant)
    {
        "use strict";
        $('<a class="btn btn-primary select-btn" style="margin-right: 10px; margin-bottom: 10px;">'+variant+' '+'<i data-variant="'+variant+'" class="glyphicon glyphicon-remove remove"></i></a>').fadeIn('slow').appendTo('.inputs');
    }


    function createSelectOptions(variants){
        let html = '';
        $('#default-container').empty();
        for (let variant of variants) {
            $('#default-container').append('<input name="OptionsForm[variants][]" value="'+variant+'" type="hidden">');
            html += '<option value="'+variant+'">'+variant+'</option>';
        }
        return html;
    }
    let optionVal = $('#list-type').find(":selected").val();
    if (optionVal === '3' || optionVal === '5'){
        $('.block-variants').show();
    }
    $('#list-type').change(function() {
        if ($(this).val() === '3' || $(this).val() === '5') {
            $('.block-variants').show();
        }else {
            $('.block-variants').hide();
        }
    });

});
