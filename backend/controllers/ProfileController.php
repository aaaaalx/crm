<?php

namespace backend\controllers;


use core\entities\Profile;
use core\exceptions\NotFoundException;
use core\forms\backend\ProfileCreateForm;
use core\forms\backend\ProfileUpdateForm;
use core\forms\search\ProfileSearch;
use core\helpers\DumpHelper;
use core\services\common\ProfileService;
use yii\base\Module;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use core\entities\User;
use \core\helpers\UploadedFile;


class ProfileController extends Controller
{

    private $service;
    private $path;

    public function __construct($id, Module $module, array $config = [], ProfileService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->path = \Yii::getAlias('@api');
    }

    public function actionIndex()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionCreate()
    {
        $userId = Yii::$app->request->get('id');
        $form = new ProfileCreateForm();

        if (!empty($userId)){
            $form->user_id = $userId;
        }
        if ($form->load(Yii::$app->request->post())){
            $form->avatar = UploadedFile::uploaded($form, 'avatar');
            if ($form->validate()){
                try{
                    $this->service->create($form, $this->path);
                    return $this->redirect('index');
                }catch (\DomainException $e){
                    Yii::$app->session->setFlash('error', $e->getMessage());
                }
            }
        }
        return $this->render('create', ['model' => $form]);
    }

    public function actionUpdate($id)
    {
        $profile = $this->findModel($id);
        $form = new ProfileUpdateForm($profile);
        if ($form->load(Yii::$app->request->post())) {
            $form->avatar = UploadedFile::uploaded($form, 'avatar');
            if($form->validate()){
                try{
                    $this->service->edit($profile->id, $form, $this->path);
                    $this->redirect('index');
                }catch (\DomainException $e){
                    Yii::$app->session->setFlash($e->getMessage());
                }
            }

        }
        return $this->render('update', ['model' => $form, 'profile'=>$profile]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDelete($id)
    {
        try{
            $this->service->delete($id);
            Yii::$app->session->setFlash('success', 'was removed');
        }catch (\DomainException $e) {
            Yii::$app->session->setFlash($e->getMessage());
        }
        return $this->redirect('index');
    }

    public function actionRemoveImage($id)
    {
        $this->service->removeImage($id, $this->path);
        return json_decode(true);
    }

    public function actionSearchUsers($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = User::find();
            $query->select(['id' =>'id','text' => 'email'])
                ->where(['like', 'email', $q]);
            $data = $query->asArray()->all();
            $out['results'] = $data;
        }
        return $out;
    }

    private function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundException('The requested page does not exist.');
    }

}