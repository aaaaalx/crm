<?php

namespace backend\controllers;

use core\entities\Language;
use core\forms\backend\LanguageForm;
use core\forms\search\LanguageSearch;
use core\services\back\LanguageService;
use yii\base\Module;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

class LanguageController extends DefaultController
{
    private $service;

    public function __construct($id, Module $module, array $config = [], LanguageService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionCreate()
    {
        $form = new LanguageForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $lang = $this->service->create($form);
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form
        ]);
    }

    public function actionUpdate($id)
    {
        $lang = $this->findModel($id);

        $form = new LanguageForm($lang);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $lang = $this->service->edit($form, $lang->id);
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id)
    {
        try {
            $lang = $this->service->remove($id);
            Yii::$app->session->setFlash('success', Yii::t('controllers.flash', 'Language was successfully deleted'));
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    public function actionRestore($id)
    {
        try {
            $lang = $this->service->restore($id);
            Yii::$app->session->setFlash('success', Yii::t('controllers.flash', 'Language was successfully restored'));
            return $this->redirect('index');
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect('index');
        }
    }

    protected function findModel($id)
    {
        if (($lang = Language::findOne($id)) !== null) {
            return $lang;
        }
        throw new NotFoundHttpException(Yii::t('controllers.errors', 'Language not found'));
    }
}