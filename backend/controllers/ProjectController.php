<?php

namespace backend\controllers;

use core\entities\Options;
use core\entities\ProjectOptions;
use core\forms\backend\compositeForm\ProjectCreateForm;
use core\forms\backend\compositeForm\ProjectUpdateForm;
use core\helpers\OptionsHelper;
use core\helpers\UploadedFile;
use Yii;
use core\entities\Project;
use core\services\back\ProjectService;
use core\forms\search\ProjectSearch;
use yii\db\Query;
use yii\web\Response;
use yii\web\Controller;
use yii\base\Module;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{

    private $service;
    private $path;

    public function __construct($id, Module $module, array $config = [], ProjectService $project)
    {
        parent::__construct($id, $module, $config);
        $this->service = $project;
        $this->path = \Yii::getAlias('@api');
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $form = new ProjectCreateForm(Yii::$app->request->post());

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $form->image = UploadedFile::uploaded($form, 'image');
                $entity = $this->service->create($form, $this->path);
                \Yii::$app->session->setFlash('success', Yii::t('app', 'Project was created!'));
                return $this->redirect('index');
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $entity = $this->findModel($id);
        $form = new ProjectUpdateForm(Yii::$app->request->post(), $entity);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $form->image = UploadedFile::uploaded($form, 'image');
                $entity = $this->service->edit($form, $this->path, $entity->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Project was updated!'));
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'project' => $entity
        ]);
    }

    public function actionChangeStatus()
    {
        try{
            $this->service->changeStatus(Yii::$app->request->post('id'), Yii::$app->request->post('status'));
            Yii::$app->session->setFlash('success', Yii::t('app', 'Project status was changed!'));
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
        catch (\DomainException $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
    }

    public function actionRemoveImage($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        try{
            $this->service->removeImage($id, $this->path);
            return true;
        }
        catch (\DomainException $e){
            return false;
        }
    }

    public function actionGetOptionOutput()
    {
        $post = Yii::$app->request->post();
        $project_id = !empty($post['project_id']) ? $post['project_id'] : null;
        $option_id = !empty($post['option_id']) ? $post['option_id'] : null;
        $options = !empty($post['options']) ? $post['options'] : null;
        $field_name = @$post['field_name'];
        $field_id = @$post['field_id'];
        if (Yii::$app->request->isAjax && $option_id) {
            $option = Options::find()->where(['id' => $option_id])->one();
            $projectOptions = ProjectOptions::find()->where('project_id = :project_id AND option_id = :option_id', [':project_id' => $project_id, ':option_id' => $option_id])->one() ?: null;
            if ($option) {
                return OptionsHelper::getOutputByType($option, $field_name, $field_id, $projectOptions->value ?? null, $options);
            }
        }
    }

    public function actionGetProjects($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, title AS text')
                ->from('projects')
                ->where(['status' => Project::ACTIVE]);
            if (is_numeric($q)) {
                $query->andWhere(['id' => filter_var($q, FILTER_VALIDATE_INT)]);
                $query->orWhere(['like', 'title', $q]);
            } else {
                $query->andWhere(['like', 'title', $q]);
            }
            $query->orderBy(['id' => SORT_DESC])->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = $data;
        }
        return $out;
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->service->delete($id);
            Yii::$app->session->setFlash('success', 'Project was successfully removed!');
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash($e->getMessage());
        }
        return $this->redirect('index');
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
