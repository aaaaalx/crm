<?php

namespace backend\controllers;

use core\forms\backend\AdminForm;
use core\forms\backend\SetLangForm;
use Yii;
use core\entities\Admin;
use core\forms\backend\AdminCreateForm;
use core\forms\backend\AdminSearch;
use yii\helpers\BaseVarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use core\forms\backend\AdminUpdateForm;
use yii\base\Module;
use core\services\back\AdminService;

/**
 * AdminController implements the CRUD actions for Admin model.
 */
class AdminController extends DefaultController
{

    private $service;

    public function __construct($id, Module $module, array $config = [], AdminService $service)
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Admin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Admin model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Admin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new AdminCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $admin = $this->service->create($form);
            return $this->redirect(['view', 'id' => $admin->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $admin = $this->findModel($id);

        $form = new AdminUpdateForm($admin);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->service->edit($admin->id, $form);
            return $this->redirect(['view', 'id' => $admin->id]);
        }

        return $this->render('update', [
            'model' => $form,
            'admin' => $admin
        ]);
    }

    public function actionSetLang()
    {
        $form = new SetLangForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->service->setLang($form, Yii::$app->user->id);
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    /**
     * Deletes an existing Admin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('controllers.errors', 'The requested page does not exist.'));
    }

    public function actionSetTimezone()
    {
        if (Yii::$app->request->isAjax && $tz = Yii::$app->request->post('timezone')) {
            try {
                $this->service->timezone(Yii::$app->user->id, $tz);
                return true;
            } catch (\DomainException $e) {
                return false;
            }
        }
    }
}
