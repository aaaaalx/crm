<?php

namespace backend\controllers;

use core\entities\Position;
use core\exceptions\NotFoundException;
use core\forms\backend\PositionForm;
use core\forms\search\PositionSearch;
use yii\base\Module;
use yii\web\Controller;
use core\services\back\PositionService;
use Yii;

class PositionController extends Controller
{
    public $service;

    public function __construct(string $id, Module $module, array $config = [], PositionService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        $searchModel = new PositionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionCreate()
    {
        $form = new PositionForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try{
                $this->service->create($form);
                $this->redirect('index');
            }catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', ['model' => $form]);

    }

    public function actionUpdate($id)
    {
        $position = $this->findModel($id);
        $form = new PositionForm($position);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try{
                $this->service->edit($position->id, $form);
                return $this->redirect('index');
            }catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', ['model' => $form]);

    }

    public function actionView($id)
    {
        $position = $this->findModel($id);
        return $this->render('view', ['model'=>$position]);
    }

    public function actionDelete($id)
    {
        try{
            $this->service->delete($id);
            Yii::$app->session->setFlash('success', 'position has been deleted');
        }catch (\DomainException $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect('index');
    }

    private function findModel($id)
    {
        if (($model = Position::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundException('The requested page does not exist.');
    }

}