<?php

namespace backend\controllers;

use Yii;
use core\forms\backend\TaskTypeForm;
use core\services\back\TaskTypeService;
use core\entities\TaskType;
use core\forms\search\TaskTypeSearch;
use yii\base\Module;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class TaskTypeController extends Controller
{
    private $service;

    public function __construct($id, Module $module, array $config = [], TaskTypeService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'change-status' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaskType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new TaskType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new TaskTypeForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $entity = $this->service->create($form);
                \Yii::$app->session->setFlash('success', Yii::t('app', 'Task type was created!'));
                return $this->redirect('index');
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing TaskType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $entity = $this->findModel($id);
        $form = new TaskTypeForm($entity);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Task type was updated!'));
            $entity = $this->service->edit($form, $entity->id);
            return $this->redirect(['index']);
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    /**
     * Deletes an existing TaskType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->service->delete($id);
            Yii::$app->session->setFlash('success', 'Task type was deleted!');
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash($e->getMessage());
        }
        return $this->redirect('index');
    }


    public function actionChangeStatus(){
        try{
            $this->service->changeStatus(Yii::$app->request->post('id'), Yii::$app->request->post('status'));
            Yii::$app->session->setFlash('success', Yii::t('app', 'Task type status was changed!'));
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
        catch (\DomainException $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
    }

    /**
     * Finds the TaskType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaskType::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
