<?php


namespace backend\controllers;


class AccessController extends \developeruz\db_rbac\controllers\AccessController
{
    public function beforeAction($action)
    {
        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->language = \Yii::$app->user->identity->getLang();
        }
        return parent::beforeAction($action);
    }
}