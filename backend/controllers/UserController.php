<?php

namespace backend\controllers;

use core\entities\User;
use core\forms\backend\UserRoleForm;
use core\forms\common\UserForm;
use core\forms\common\UserUpdateForm;
use core\forms\search\UserSearch;
use core\services\common\UserService;
use yii\base\Module;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

class UserController extends DefaultController
{

    private $service;

    public function __construct($id, Module $module, array $config = [], UserService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate()
    {
        $form = new UserForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $user = $this->service->create($form);
                return $this->redirect(['profile/create', 'id' => $user->id]);
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $profile = $user->profile;
        $form = new UserUpdateForm($user);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($form, $user->id);
                return $this->redirect(['profile/update', 'id' => $profile->id]);
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionView($id)
    {
        try {
            $user = $this->service->view($id);
            return $this->render('view', [
                'user' => $user
            ]);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }

    public function actionDelete($id)
    {
        try {
            $user = $this->service->remove($id);
            Yii::$app->session->setFlash('success', Yii::t('controllers.flash', 'User was successfully deleted'));
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    public function actionActivate($id)
    {
        try {
            $user = $this->service->activate($id);
            Yii::$app->session->setFlash('success', Yii::t('controllers.flash', 'User was successfully updated'));
            return $this->redirect('index');
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect('index');
        }
    }

    public function actionModerate($id)
    {
        try {
            $user = $this->service->moderate($id);
            Yii::$app->session->setFlash('success', Yii::t('controllers.flash', 'User was successfully send to moderation'));
            return $this->redirect('index');
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect('index');
        }
    }

    public function actionList($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, email AS text')
                ->from('user')
                ->where(['like', 'email', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::findOne($id)->email];
        }
        return $out;
    }

    public function actionAssignRole($id)
    {
        $form = new UserRoleForm();
        $form->user_id = $id;
        if ($form->load(Yii::$app->request->post()) && $form->validate()){
            try{
                $this->service->assignRole($form);
                return $this->redirect('index');
            }catch (\DomainException $e){
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('assign-role', ['model' => $form]);
    }

    public function actionGetUsers($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, email AS text')
                ->from('users')
                ->where(['status' => User::STATUS_ACTIVE]);
            if (is_numeric($q)) {
                $query->andWhere(['id' => filter_var($q, FILTER_VALIDATE_INT)]);
                $query->orWhere(['like', 'email', $q]);
            } else {
                $query->andWhere(['like', 'email', $q]);
            }
            $query->orderBy(['id' => SORT_DESC])->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = $data;
        }
        return $out;
    }

    private function findModel($id)
    {
        if (($user = User::findOne($id)) !== null) {
            return $user;
        }
        throw new NotFoundHttpException(Yii::t('controllers.errors', 'User not found'));
    }
}