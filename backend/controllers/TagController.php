<?php

namespace backend\controllers;

use Yii;
use core\forms\backend\TagForm;
use core\services\back\TagService;
use core\entities\Tag;
use core\forms\search\TagSearch;
use yii\base\Module;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\db\Query;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    private $service;

    public function __construct($id, Module $module, array $config = [], TagService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'change-status' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new TagForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $entity = $this->service->create($form);
                \Yii::$app->session->setFlash('success', Yii::t('app', 'Tag was created!'));
                return $this->redirect('index');
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $entity = $this->findModel($id);
        $form = new TagForm($entity);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Tag was updated!'));
            $entity = $this->service->edit($form, $entity->id);
            return $this->redirect(['index']);
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->delete()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Tag was deleted!'));
        }
        else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Tag was not deleted!'));
        }
        return $this->redirect(['index']);
    }

    public function actionChangeStatus(){
        try{
            $this->service->changeStatus(Yii::$app->request->post('id'), Yii::$app->request->post('status'));
            Yii::$app->session->setFlash('success', Yii::t('app', 'Tag status was changed!'));
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
        catch (\DomainException $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
    }

    public function actionGetTags($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, name AS text')->from('tags')->where(['status' => Tag::ACTIVE]);
            if (is_numeric($q)) {
                $query->andWhere(['id' => filter_var($q, FILTER_VALIDATE_INT)]);
                $query->orWhere(['like', 'name', $q]);
            } else {
                $query->andWhere(['like', 'name', $q]);
            }
            $query->orderBy(['id' => SORT_DESC])->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = $data;
        }
        return $out;
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
