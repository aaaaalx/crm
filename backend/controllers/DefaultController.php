<?php


namespace backend\controllers;


use yii\web\Controller;

class DefaultController extends Controller
{
    public function beforeAction($action)
    {
        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->language = \Yii::$app->user->identity->getLang();
        }
        return parent::beforeAction($action);
    }
}