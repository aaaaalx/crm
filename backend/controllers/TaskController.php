<?php

namespace backend\controllers;

use core\forms\backend\compositeForm\TaskCreateForm;
use core\forms\backend\compositeForm\TaskUpdateForm;
use Yii;
use core\forms\backend\TaskForm;
use core\services\back\TaskService;
use core\entities\Task;
use core\forms\search\TaskSearch;
use yii\base\Module;
use yii\helpers\BaseVarDumper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use core\helpers\UploadedFile;
use yii\db\Query;
use yii\web\Response;

class TaskController extends Controller
{
    private $service;
    private $path;

    public function __construct($id, Module $module, array $config = [], TaskService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->path = \Yii::getAlias('@api');
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'change-status' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new TaskCreateForm(Yii::$app->request->post());

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $form->uploadedFile = UploadedFile::uploads($form->files, 'file');
                $entity = $this->service->create($form, $this->path);
                \Yii::$app->session->setFlash('success', Yii::t('app', 'Task was created!'));
                return $this->redirect('index');
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $entity = $this->findModel($id);
        $form = new TaskUpdateForm(Yii::$app->request->post(), $entity);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $form->uploadedFile = UploadedFile::uploads($form->files, 'file');
                $entity = $this->service->edit($form, $this->path, $entity->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Task was updated!'));
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'task' => $entity
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->service->delete($id);
            Yii::$app->session->setFlash('success', 'Task was deleted!');
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash($e->getMessage());
        }
        return $this->redirect('index');
    }

    public function actionChangeStatus()
    {
        try{
            $this->service->changeStatus(Yii::$app->request->post('id'), Yii::$app->request->post('status'));
            Yii::$app->session->setFlash('success', Yii::t('app', 'Task status was changed!'));
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
        catch (\DomainException $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect(Yii::$app->getRequest()->getReferrer());
        }
    }

    public function actionGetRelations($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, title AS text')
                ->from('tasks')
                ->where(['status' => Task::ACTIVE]);
            if (is_numeric($q)) {
                $query->andWhere(['id' => filter_var($q, FILTER_VALIDATE_INT)]);
                $query->orWhere(['like', 'title', $q]);
            } else {
                $query->andWhere(['like', 'title', $q]);
            }
            $query->orderBy(['id' => SORT_DESC])->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = $data;
        }
        return $out;
    }

    public function actionRemoveImage($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        try{
            $this->service->removeImage($id, $this->path);
            return true;
        }
        catch (\DomainException $e){
            return false;
        }
    }

    /**
     * Finds the TaskType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
