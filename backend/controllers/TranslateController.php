<?php


namespace backend\controllers;


use Zelenin\yii\modules\I18n\controllers\DefaultController;

class TranslateController extends DefaultController
{
    public function beforeAction($action)
    {
        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->language = \Yii::$app->user->identity->getLang();
        }
        return parent::beforeAction($action);
    }
}