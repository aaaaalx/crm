<?php

namespace backend\controllers;

use core\forms\backend\SearchForm;
use yii\web\Controller;
use Yii;

class SearchController extends DefaultController
{
    public function actionSearch()
    {
        $form = new SearchForm();

        $searchResult = $form->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'search' => $searchResult
        ]);
    }
}