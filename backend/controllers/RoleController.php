<?php

namespace backend\controllers;

use core\entities\Role;
use core\forms\backend\RoleForm;
use core\helpers\DumpHelper;
use core\services\back\RoleService;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class RoleController extends DefaultController
{
    private $service;

    public function __construct($id, Module $module, array $config = [], RoleService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => Role::find()])
        ]);
    }

    public function actionCreate()
    {
        $form = new RoleForm();

        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            try {
                $role = $this->service->create($form);
                return $this->redirect('index');
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form
        ]);
    }

    public function actionUpdate($id)
    {
        $role = $this->findRoleModel($id);
        $form = new RoleForm($role);
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            try {
                $role = $this->service->edit($form, $role->id);
                return $this->redirect('index');
            } catch (\DomainException $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form
        ]);
    }

    private function findRoleModel($id)
    {
        if (($role = Role::findOne($id)) !== null) {
            return $role;
        }
        throw new NotFoundHttpException('Page not found');
    }
}