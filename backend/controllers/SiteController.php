<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends DefaultController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

/*        $format = '%1$03d';
        $zero = decbin(0);
        $one = decbin(1);
        $two = decbin(2);
        $three = decbin(3);
        $four = decbin(4);
        $five = decbin(5);
        $six = decbin(6);
        $seven = decbin(7);

        echo '<br>'.sprintf($format,  $zero).' = 0';
        echo '<br>'.sprintf($format,  $one).' = 1';
        echo '<br>'.sprintf($format,  $two).' = 2';
        echo '<br>'.sprintf($format,  $three).' = 3';
        echo '<br>'.sprintf($format,  $four).' = 4';
        echo '<br>'.sprintf($format,  $five).' = 5';
        echo '<br>'.sprintf($format,  $six).' = 6';
        echo '<br>'.sprintf($format,  $seven).' = 7';

        echo '<br>';

        echo '<br>'.sprintf($format,  decbin(0<<0)).' = 0';
        echo '<br>'.sprintf($format,  decbin(1<<0)).' = 1';
        echo '<br>'.sprintf($format,  decbin(1<<1)).' = 2';
        echo '<br>'.sprintf($format,  decbin(1<<2)).' = 3';
//        echo '<br>'.sprintf($format,  $four).' = 4';
//        echo '<br>'.sprintf($format,  $five).' = 5';
//        echo '<br>'.sprintf($format,  $six).' = 6';
//        echo '<br>'.sprintf($format,  $seven).' = 7';

        echo '<br>';

        echo '<br>'. (0<<0) .' = 0';
        echo '<br>'. (1<<0) .' = 1';
        echo '<br>'. (1<<1) .' = 2';
        echo '<br>'. (1<<2) .' = 3';
        die;*/

        return $this->render('index');
    }
}
