<?php

namespace core\events\handlers;

use core\entities\redis\User;
use core\events\events\UserRegistrationEvent;
use core\helpers\DumpHelper;

class UserRegistrationHandler
{

    public function handle(UserRegistrationEvent $event)
    {
        $user = $event->user;
        try {
            $user = User::create($user->id, $user->access_token, $user->email, $user->password_hash, $user->time_zone, $user->status, $user->ctime);
            $user->save();
        } catch (\Exception $e) {

        }
    }
}