<?php

namespace core\events\events;

use core\entities\User;

class UserRegistrationEvent
{

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}