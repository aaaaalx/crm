<?php

namespace core\forms\api;

use yii\base\Model;

class LoginForm extends Model
{
    public $email;
    public $password;
    public $lang;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password', 'email'], 'required'],
            ['email', 'email'],
        ];
    }

}