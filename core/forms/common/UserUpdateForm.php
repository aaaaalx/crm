<?php


namespace core\forms\common;


use core\entities\User;
use yii\base\Model;

class UserUpdateForm extends Model
{

    private $user;

    public $email;
    public $timeZone = 0;
    public $password;

    public function __construct(User $user, array $config = [])
    {
        parent::__construct($config);
        $this->user = $user;
        $this->email = $user->email;
        $this->timeZone = $user->time_zone;
    }

    public function rules()
    {
        return [
            ['email', 'email'],
            ['email', 'trim'],
            [['email'], 'required'],
            ['email', 'unique', 'targetClass' => User::class, 'filter' => $this->user ? ['<>', 'id', $this->user->id] : null],
            ['password', 'string', 'min' => 6],
            ['timeZone', 'integer', 'min' => -12, 'max' => 14]
        ];
    }
}