<?php
namespace core\forms\backend;

use \yii\base\Model;

class PositionForm extends Model
{
    public $id;
    public $title;
    public $description;
    public $status;
    public $ctime;
    public $utime;

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['description', 'title'], 'string']
        ];
    }

}