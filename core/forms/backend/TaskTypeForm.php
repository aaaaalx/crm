<?

namespace core\forms\backend;

use Yii;
use core\entities\TaskType;
use yii\base\Model;

class TaskTypeForm extends Model
{

    public $name;
    public $status;
    private $taskType;

    public function __construct(TaskType $taskType = null, array $config = [])
    {
        parent::__construct($config);
        if ($taskType) {
            $this->taskType = $taskType;
            $this->name = $taskType->name;
            $this->status = $taskType->status;
        }
    }

    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['name'], 'trim'],
            [['name'], 'unique', 'targetClass' => TaskType::class, 'filter' => $this->name ? ['<>', 'name', $this->taskType->name] : null],
            [['name'], 'string', 'min' => 2, 'max' => 15],
            ['status', 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

}