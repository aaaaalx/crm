<?php

namespace core\forms\backend;


use core\traits\ConsoleValidationErrorLog;
use yii\base\Model;
use core\entities\Admin;

class SignupForm extends Model
{

    use ConsoleValidationErrorLog;

    public $email;
    public $password;
    public $time_zone = 0;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password', 'time_zone'], 'required'],
            [['password'], 'string', 'min' => 6],
            ['email', 'trim'],
            [['email'], 'email'],
            ['time_zone', 'integer'],
            ['email', 'unique', 'targetClass' => Admin::class, 'targetAttribute' => 'email'],
        ];
    }

    public function beforeValidate()
    {
        $this->time_zone = -($this->time_zone) / 60;
        return parent::beforeValidate();
    }
}