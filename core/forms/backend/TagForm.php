<?

namespace core\forms\backend;

use Yii;
use core\entities\Tag;
use yii\base\Model;

class TagForm extends Model
{

    public $name;
    public $status;
    private $tag;

    public function __construct(Tag $tag = null, array $config = [])
    {
        parent::__construct($config);
        if ($tag) {
            $this->tag = $tag;
            $this->name = $tag->name;
            $this->status = $tag->status;
        }
    }

    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['name'], 'trim'],
            [['name'], 'unique', 'targetClass' => Tag::class, 'filter' => $this->name ? ['<>', 'name', $this->tag->name] : null],
            [['name'], 'string', 'min' => 2, 'max' => 15],
            ['status', 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

}