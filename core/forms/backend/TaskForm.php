<?

namespace core\forms\backend;

use Yii;
use core\entities\Task;
use yii\base\Model;

class TaskForm extends Model
{

    public $title;
    public $desc;
    public $user_id;
    public $project_id;
    public $status;
    public $priority;
    public $type_id;
    public $stime;
    public $etime;
    public $remember_time;
    public $checkbox_list;
    private $tasks;

    public function __construct(Task $task = null, array $config = [])
    {
        parent::__construct($config);
        if ($task) {
            $this->tasks = $task;
            $this->title = $task->title;
            $this->desc = $task->desc;
            $this->user_id = $task->user_id;
            $this->project_id = $task->project_id;
            $this->status = $task->status;
            $this->priority = $task->priority;
            $this->type_id = $task->type_id;
            $this->ctime = $task->ctime;
            $this->utime = $task->utime;
            $this->stime = $task->stime;
            $this->etime = $task->etime;
            $this->remember_time = $task->remember_time;
            $this->checkbox_list = $task->checkbox_list;
        }
    }

    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['title'], 'trim'],
            [['user_id', 'project_id'], 'required', 'when' => function(){
                !$this->user_id&&!$this->project_id?false:true;
            }],
            [['title'], 'unique', 'targetClass' => Task::class, 'filter' => $this->title ? ['<>', 'title', $this->tasks->title] : null],
            [['title'], 'string', 'min' => 2, 'max' => 50],
            [['desc', 'checkbox_list'], 'string', 'min' => 0, 'max' => 5000],
            [['status', 'priority'], 'integer', 'min' => 0, 'max' => 10],
            [['priority'], 'default', 'value' => Task::PRIORITY[0]],
            [['stime', 'etime', 'remember_time'], 'datetime', 'format' => 'yyyy-M-d H:m']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'desc' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'status' => Yii::t('app', 'Status'),
            'priority' => Yii::t('app', 'Priority'),
            'type_id' => Yii::t('app', 'Type ID'),
            'stime' => Yii::t('app', 'Start time'),
            'etime' => Yii::t('app', 'End time'),
            'remember_time' => Yii::t('app', 'Remember time'),
            'checkbox_list' => Yii::t('app', 'Checkbox list'),
        ];
    }

}