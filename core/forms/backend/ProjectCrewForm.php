<?

namespace core\forms\backend;

use Yii;
use yii\base\Model;


class ProjectCrewForm extends Model
{
    public $project_id;
    public $user_id;
    public $role_id;

    public function rules()
    {
        return [
            [['user_id', 'role_id'], 'required'],
            [['project_id', 'user_id', 'role_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app', 'Project'),
            'user_id' => Yii::t('app', 'User'),
            'role_id' => Yii::t('app', 'Role'),
        ];
    }

}