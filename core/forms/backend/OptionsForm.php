<?php

namespace core\forms\backend;

use core\helpers\DumpHelper;
use Yii;
use yii\base\Model;
use core\entities\Options;

/**
 * Class OptionsForm
 * @package core\forms\backend
 */
class OptionsForm extends Model
{

    private $option;

    public $id;
    public $name;
    public $type;
    public $variants;

    public function __construct(Options $option = null, array $config = [])
    {
        parent::__construct($config);
        if ($option){
            $this->option = $option;
            $this->name = $option->name;
            $this->type = $option->type;
            $this->variants = $option->variants;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type'], 'default', 'value' => null],
            [['type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['variants', 'each', 'rule' => ['string']],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

}