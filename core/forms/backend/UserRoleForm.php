<?php

namespace core\forms\backend;


use core\entities\Role;
use core\entities\User;
use core\entities\UserRole;
use yii\base\Model;

/**
 * Class UserRoleForm
 * @package core\forms\backend
 * @property $id integer
 * @property $user_id integer
 * @property $role_id integer
 */
class UserRoleForm extends Model
{
    public $id;
    public $user_id;
    public $role_id;

    public function rules()
    {
        return [
            [['role_id'], 'required'],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['role_id', 'exist', 'targetClass' => Role::class, 'targetAttribute' => 'id']
        ];
    }

}