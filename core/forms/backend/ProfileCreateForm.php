<?php

namespace core\forms\backend;


use core\entities\Profile;
use core\entities\User;
use core\helpers\FileHelper;
use yii\base\Model;

/**
 * Class ProfileCreateForm
 * @package core\forms\backend
 *
 */

class ProfileCreateForm extends Model
{
    public $user_id;
    public $position_id;
    public $gender;
    public $first_name;
    public $surname;
    public $phone;
    public $additional_email;
    public $patronymic;
    public $avatar;
    public $country;
    public $city;
    public $lang_id;
    public $time_zone;

    public function rules()
    {
        return [
            [['user_id', 'lang_id'], 'required'],
            ['user_id', 'unique','targetClass' => Profile::class, 'targetAttribute'  => 'user_id', 'message' =>\Yii::t('app', 'There is a profile with such a user')],
            [['position_id', 'gender', 'phone', 'lang_id'], 'integer'],
            [['user_id'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            [['first_name', 'surname', 'patronymic', 'country', 'city'], 'string'],
            [['avatar'], 'file', 'extensions' => ['png', 'jpg']],
            [['additional_email'], 'email'],
            [['time_zone'], 'safe']
        ];
    }

}