<?

namespace core\forms\backend;

use Yii;
use core\entities\Project;
use yii\base\Model;

/**
 *
 * @property int $tasksPermissions
 * @property int $projectsPermissions
 * @property int $projectMembersPermissions
 *
 */

class ProjectForm extends Model
{
    public $title;
    public $description;
    public $image;
    public $access_type;
    public $owners;
    public $assistants;
    public $users;
    public $stime;
    public $etime;
    public $status;

    private $project;

    public function __construct(Project $project = null, array $config = [])
    {
        parent::__construct($config);
        if ($project) {
            $this->project = $project;
            $this->title = $project->title;
            $this->description = $project->description;
            $this->access_type = $project->access_type;
            $this->owners = $project->owners;
            $this->assistants = $project->assistants;
            $this->users = $project->users;
            $this->stime = $project->stime;
            $this->etime = $project->etime;
            $this->status = $project->status;
        }
    }

    public function rules()
    {
        return [
            [['title', 'access_type', 'status', 'owners'], 'required'],
            [['access_type', 'status'], 'default', 'value' => null],
            [['owners', 'assistants', 'users'], 'each', 'rule' => ['integer']],
            [['access_type', 'status'], 'integer'],
            [['stime', 'etime'], 'datetime', 'format' => 'yyyy-M-d H:m'],
            [['title', 'image'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'access_type' => Yii::t('app', 'Access Type'),
            'image' => Yii::t('app', 'Image'),
            'stime' => Yii::t('app', 'Start time'),
            'etime' => Yii::t('app', 'End time'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

}