<?php

namespace core\forms\backend;


use yii\base\Model;

/**
 * @property string $version
 *
 */

class VersionCreateForm extends Model
{

    public $version;

    public function rules()
    {
        return [
            ['version', 'required']
        ];
    }
}