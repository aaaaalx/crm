<?

namespace core\forms\backend;

use Yii;
use yii\base\Model;


class ProjectOptionsForm extends Model
{
    public $project_id;
    public $option_id;
    public $value;

    public function rules()
    {
        return [
            [['project_id', 'option_id', 'value'], 'required'],
            [['project_id', 'option_id'], 'integer'],
            [['value'], 'string', 'min' => 1, 'max' => 100]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app', 'Project'),
            'option_id' => Yii::t('app', 'Options'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

}