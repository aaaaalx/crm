<?php

namespace core\forms\backend;



use core\entities\Language;
use yii\base\Model;

class LanguageForm extends Model
{
    public $title;
    public $iso;

    private $lang;

    public function __construct(Language $lang = null, array $config = [])
    {
        parent::__construct($config);
        if ($lang) {
            $this->lang = $lang;
            $this->title = $lang->title;
            $this->iso = $lang->iso;
        }
    }

    public function rules()
    {
        return [
            [['iso', 'title'], 'required'],
            ['iso', 'unique', 'targetClass' => Language::class, 'filter' => $this->lang ? ['<>', 'id', $this->lang->id] : null],
            [['title', 'iso'], 'string']
        ];
    }
}