<?
namespace core\forms\backend\compositeForm;

use core\entities\ProjectCrew;
use Yii;
use yii\base\Model;

class CompositeProjectCrewForm extends Model
{
    public $id;
    public $project_id;
    public $user_id;
    public $role_id;
    public $isNewRecord = true;
    private $projectCrew;

    public function __construct(ProjectCrew $projectCrew = null, array $config = [])
    {
        parent::__construct($config);
        if ($projectCrew) {
            $this->projectCrew = $projectCrew;
            $this->project_id = $projectCrew->project_id;
            $this->user_id = $projectCrew->user_id;
            $this->role_id = $projectCrew->role_id;
        }
    }

    public function getCrew(){
        return $this->projectCrew;
    }

    public function rules()
    {
        return [
            [['user_id', 'role_id'], 'required'],
            [['project_id', 'user_id', 'role_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app', 'Project'),
            'user_id' => Yii::t('app', 'User'),
            'role_id' => Yii::t('app', 'Role'),
        ];
    }

}