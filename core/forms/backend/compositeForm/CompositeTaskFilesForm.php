<?php

namespace core\forms\backend\compositeForm;

use core\entities\TaskFiles;
use Yii;
use yii\base\Model;


class CompositeTaskFilesForm extends Model
{
    public $id;
    public $file;
    public $isNewRecord = true;
    private $taskFiles;

    public function __construct(TaskFiles $taskFiles = null, array $config = [])
    {
        parent::__construct($config);
        if ($taskFiles) {
            $this->taskFiles = $taskFiles;
            $this->file = $taskFiles->file;
        }
    }

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'file' => Yii::t('app', 'File'),
        ];
    }

}