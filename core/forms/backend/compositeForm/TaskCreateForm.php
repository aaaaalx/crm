<?php

namespace core\forms\backend\compositeForm;

use core\entities\Task;
use core\helpers\CommonHelper;
use Yii;

/**
 * @property CompositeTaskCrewForm[] $crew
 * @property CompositeTaskRelationsForm[] $relations
 * @property CompositeTaskFilesForm $files
 * @property CompositeTagForm $tags
 * @property CompositeTaskChecklistForm[] $list
 */
class TaskCreateForm extends CompositeForm
{
    public $title;
    public $desc;
    public $user_id;
    public $project_id;
    public $status;
    public $priority;
    public $type_id;
    public $stime;
    public $etime;
    public $remember_time;
    public $checklist;
    public $uploadedFile;

    public function __construct($post = null, $config = [])
    {
        parent::__construct($config);

        if($post){

            $crew = [];
            $count = isset($post[CommonHelper::mb_basename(CompositeTaskCrewForm::class)])?count($post[CommonHelper::mb_basename(CompositeTaskCrewForm::class)]):1;
            for ($i = 0; $i < $count; $i++) {
                $crew[] = new CompositeTaskCrewForm();
            }
            $this->crew = $crew;

            $relations = [];
            $count = isset($post[CommonHelper::mb_basename(CompositeTaskRelationsForm::class)])?count($post[CommonHelper::mb_basename(CompositeTaskRelationsForm::class)]):1;
            for ($i = 0; $i < $count; $i++) {
                $relations[] = new CompositeTaskRelationsForm();
            }
            $this->relations = $relations;

            $list = [];
            $count = isset($post[CommonHelper::mb_basename(CompositeTaskChecklistForm::class)])?count($post[CommonHelper::mb_basename(CompositeTaskChecklistForm::class)]):1;
            for ($i = 0; $i < $count; $i++) {
                $list[] = new CompositeTaskChecklistForm();
            }
            $this->list = $list;

            $this->files = new CompositeTaskFilesForm();
            $this->tags = new CompositeTagForm();

        }
        else{
            $this->crew = [new CompositeTaskCrewForm()];
            $this->relations = [new CompositeTaskRelationsForm()];
            $this->files = new CompositeTaskFilesForm();
            $this->tags = new CompositeTagForm();
            $this->list = [new CompositeTaskChecklistForm()];
        }

    }

    protected function internalForms(){
        return ['crew', 'relations', 'files', 'tags', 'list'];
    }

    public function rules()
    {
        return [
            [['title', 'priority', 'status', 'type_id'], 'required'],
            [['user_id', 'project_id'], 'required', 'message' => Yii::t('app', 'User or Project must be set!'), 'when' => function($model){
                $user = trim($model->user_id);
                $project = trim($model->project_id);
                return empty($user)&&empty($project);
            }, 'whenClient' => "function (attribute, value) {
                return !$('#taskcreateform-user_id').val().trim() && !$('#taskcreateform-project_id').val().trim();
            }"],
            [['status', 'type_id', 'user_id', 'project_id'], 'integer'],
            [['stime', 'etime', 'remember_time'], 'datetime', 'format' => 'yyyy-M-d H:m'],
            ['priority', 'integer', 'max' => count(Task::PRIORITY)-1],
            [['title'], 'string', 'max' => 255],
            [['desc'], 'string', 'max' => 5000]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Title'),
            'desc' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'project_id' => Yii::t('app', 'Project'),
            'status' => Yii::t('app', 'Status'),
            'priority' => Yii::t('app', 'Priority'),
            'type_id' => Yii::t('app', 'Task type'),
            'stime' => Yii::t('app', 'Start time'),
            'etime' => Yii::t('app', 'End time'),
            'remember_time' => Yii::t('app', 'Remember at'),
            'checklist' => Yii::t('app', 'Checklist'),

        ];
    }

}