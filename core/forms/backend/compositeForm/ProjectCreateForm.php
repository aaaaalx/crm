<?php

namespace core\forms\backend\compositeForm;

use core\entities\Project;
use core\entities\Role;
use core\helpers\CommonHelper;
use Yii;
use yii\helpers\BaseVarDumper;

/**
 * @property CompositeProjectCrewForm[] $crew
 * @property CompositeProjectOptionsForm[] $options
 * @property CompositeTagForm $tags
 */
class ProjectCreateForm extends CompositeForm
{
    public $title;
    public $description;
    public $image;
    public $access_type;
    public $stime;
    public $etime;
    public $status;

    public function __construct($post = null, $config = [])
    {
        parent::__construct($config);
        if($post){

            $crew = [];
            $count = isset($post[CommonHelper::mb_basename(CompositeProjectCrewForm::class)])?count($post[CommonHelper::mb_basename(CompositeProjectCrewForm::class)]):1;
            for ($i = 0; $i < $count; $i++) {
                $crew[] = new CompositeProjectCrewForm();
            }
            $this->crew = $crew;

            $options = [];
            $count = isset($post[CommonHelper::mb_basename(CompositeProjectOptionsForm::class)])?count($post[CommonHelper::mb_basename(CompositeProjectOptionsForm::class)]):1;
            for ($i = 0; $i < $count; $i++) {
                $options[] = new CompositeProjectOptionsForm();
            }
            $this->options = $options;
            $this->tags = new CompositeTagForm();

        }
        else{
            $this->crew = [new CompositeProjectCrewForm()];
            $this->options = [new CompositeProjectOptionsForm()];
            $this->tags = new CompositeTagForm();
        }
    }

    protected function internalForms(){
        return ['crew', 'options', 'tags'];
    }

    public function rules()
    {
        return [
            [['title', 'access_type', 'status'], 'required'],
            [['access_type', 'status'], 'default', 'value' => null],
            [['access_type', 'status'], 'integer'],
            [['stime', 'etime'], 'datetime', 'format' => 'yyyy-M-d H:m'],
            [['title', 'image'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['title'], 'unique', 'targetClass'=>Project::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'access_type' => Yii::t('app', 'Access Type'),
            'image' => Yii::t('app', 'Image'),
            'stime' => Yii::t('app', 'Start time'),
            'etime' => Yii::t('app', 'End time'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getRoles(){
        $roles = Role::find()->all();
        return array_column($roles, 'name', 'id');
    }

}