<?

namespace core\forms\backend\compositeForm;

use core\entities\Project;
use core\entities\Role;
use core\helpers\CommonHelper;
use Yii;
use yii\helpers\BaseVarDumper;

/**
 * @property CompositeProjectCrewForm $crew
 * @property CompositeProjectOptionsForm[] $options
 * @property CompositeTagForm[] $tags
 */
class ProjectUpdateForm extends CompositeForm
{
    public $title;
    public $description;
    public $image;
    public $access_type;
    public $stime;
    public $etime;
    public $status;
    private $project;

    public function __construct($post = null, Project $project, $config = [])
    {
        parent::__construct($config);
        if ($project) {
            $this->project = $project;
            $this->title = $project->title;
            $this->description = $project->description;
            $this->image = $project->image;
            $this->stime = $project->stime?Yii::$app->formatter->asDate($project->stime, 'y-MM-dd hh:m'):'';
            $this->etime =  $project->etime?Yii::$app->formatter->asDate($project->etime, 'y-MM-dd hh:m'):'';
            $this->status = $project->status;
        }
        if($post){
            $crew = [];
            $count = isset($post[CommonHelper::mb_basename(CompositeProjectCrewForm::class)])?count($post[CommonHelper::mb_basename(CompositeProjectCrewForm::class)]):1;
            for ($i = 0; $i < $count; $i++) {
                $crew[] = new CompositeProjectCrewForm($project->crew[$i]??null);
            }
            $this->crew = $crew;

            $options = [];
            $count = isset($post[CommonHelper::mb_basename(CompositeProjectOptionsForm::class)])?count($post[CommonHelper::mb_basename(CompositeProjectOptionsForm::class)]):1;
            for ($i = 0; $i < $count; $i++) {
                $options[] = new CompositeProjectOptionsForm($project->options[$i]??null);
            }
            $this->options = $options;
            $this->tags = new CompositeTagForm();

        }
        else{
            $crew = [];
            foreach ($project->crew as $key => $user){
                $crew[] = new CompositeProjectCrewForm($user);
            }
            $this->crew = $crew;

            $options = [];
            if(count($project->options)){
                foreach ($project->options as $option){
                    $options[] = new CompositeProjectOptionsForm($option);
                }
            }
            else{
                $options[] = new CompositeProjectOptionsForm();
            }
            $this->options = $options;

            $tags = new CompositeTagForm();
            $tags->name = array_column($project->tags, 'name', 'id');
            $this->tags = $tags;

        }
    }

    protected function internalForms(){
        return ['crew', 'options', 'tags'];
    }

    public function rules()
    {
        return [
            [['title', 'access_type', 'status'], 'required'],
            [['access_type', 'status'], 'default', 'value' => null],
            [['access_type', 'status'], 'integer'],
            [['stime', 'etime'], 'datetime', 'format' => 'yyyy-M-d H:m'],
            [['title', 'image'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'access_type' => Yii::t('app', 'Access Type'),
            'image' => Yii::t('app', 'Image'),
            'stime' => Yii::t('app', 'Start time'),
            'etime' => Yii::t('app', 'End time'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getRoles(){
        $roles = Role::find()->all();
        return array_column($roles, 'name', 'id');
    }

}