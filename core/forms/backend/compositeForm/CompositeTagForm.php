<?

namespace core\forms\backend\compositeForm;

use Yii;
use core\entities\Tag;
use yii\base\Model;

/**
 *
 * @property int $tasksPermissions
 * @property int $projectsPermissions
 * @property int $projectMembersPermissions
 *
 */

class CompositeTagForm extends Model
{
    public $id;
    public $name;
    public $status;
    private $tag;

    public function __construct(Tag $tag = null, array $config = [])
    {
        parent::__construct($config);
        if ($tag) {
            $this->tag = $tag;
            $this->id = $tag->id;
            $this->name = $tag->name;
            $this->status = $tag->status;
        }
    }

    public function getTag(){
        return $this->tag;
    }

    public function rules()
    {
        return [
            [['status'], 'integer'],
            ['status', 'default', 'value' => Tag::ACTIVE],
            [['name'], 'each', 'rule' => ['string']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

}