<?

namespace core\forms\backend\compositeForm;

use core\entities\TaskRelations;
use Yii;
use yii\base\Model;

class CompositeTaskRelationsForm extends Model
{
    public $id;
    public $task_id;
    public $related_task_id;
    public $relation_type;
    public $isNewRecord = true;
    private $taskRelations;

    public function __construct(TaskRelations $taskRelations = null, array $config = [])
    {
        parent::__construct($config);
        if ($taskRelations) {
            $this->taskRelations = $taskRelations;
            $this->task_id = $taskRelations->task_id;
            $this->related_task_id = $taskRelations->related_task_id;
            $this->relation_type = $taskRelations->relation_type;
        }
    }

    public function getTaskRelation(){
        return $this->taskRelations;
    }

    public function rules()
    {
        return [
            [['task_id', 'related_task_id', 'relation_type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('app', 'Task'),
            'related_task_id' => Yii::t('app', 'Related task'),
            'relation_type' => Yii::t('app', 'Relation type'),
        ];
    }

}