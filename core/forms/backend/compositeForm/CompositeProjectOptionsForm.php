<?

namespace core\forms\backend\compositeForm;

use core\entities\ProjectOptions;
use Yii;
use yii\base\Model;


class CompositeProjectOptionsForm extends Model
{
    public $id;
    public $project_id;
    public $option_id;
    public $value;
    public $isNewRecord = true;
    private $projectOptions;

    public function __construct(ProjectOptions $projectOptions = null, array $config = [])
    {
        parent::__construct($config);
        if ($projectOptions) {
            $this->projectOptions = $projectOptions;
            $this->project_id = $projectOptions->project_id;
            $this->option_id = $projectOptions->option_id;
            $this->value = $projectOptions->value;
        }
    }

    public function getProjectOptions(){
        return $this->projectOptions;
    }

    public function rules()
    {
        return [
            [['project_id', 'option_id'], 'integer'],
            [['value'], 'string', 'min' => 1, 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app', 'Project'),
            'option_id' => Yii::t('app', 'Options'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

}