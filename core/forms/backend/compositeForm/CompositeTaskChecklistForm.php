<?php
namespace core\forms\backend\compositeForm;

use Yii;
use yii\base\Model;

class CompositeTaskChecklistForm extends Model
{
    const ACTIVE = 1;
    const INACTIVE = 2;

    public $id;
    public $text;
    public $status;
    public $isNewRecord = true;

    public function __construct($text = null, $status = null, array $config = [])
    {
        parent::__construct($config);
        $this->text = $text;
        $this->status = $status;
    }

    public function rules()
    {
        return [
            [['status'], 'integer'],
            ['text', 'string', 'max' => 2000]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status')
        ];
    }

    public static function getStatus(){
        return [
            self::ACTIVE => Yii::t('app', 'Active'),
            self::INACTIVE => Yii::t('app', 'Inactive'),
        ];
    }

}