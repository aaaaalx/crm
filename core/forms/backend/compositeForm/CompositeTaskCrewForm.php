<?php
namespace core\forms\backend\compositeForm;

use core\entities\TaskCrew;
use Yii;
use yii\base\Model;

class CompositeTaskCrewForm extends Model
{
    public $id;
    public $task_id;
    public $user_id;
    public $role;
    public $isNewRecord = true;
    private $taskCrew;

    public function __construct(TaskCrew $taskCrew = null, array $config = [])
    {
        parent::__construct($config);
        if ($taskCrew) {
            $this->taskCrew = $taskCrew;
            $this->task_id = $taskCrew->task_id;
            $this->user_id = $taskCrew->user_id;
            $this->role = $taskCrew->role;
        }
    }

    public function getCrew(){
        return $this->taskCrew;
    }

    public function rules()
    {
        return [
            [['task_id', 'user_id', 'role'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('app', 'Task'),
            'user_id' => Yii::t('app', 'User'),
            'role' => Yii::t('app', 'Role'),
        ];
    }

}