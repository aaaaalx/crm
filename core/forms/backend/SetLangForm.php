<?php

namespace core\forms\backend;

use core\entities\Admin;
use yii\base\Model;

class SetLangForm extends Model
{
    public $lang;

    public function __construct(Admin $admin = null, array $config = [])
    {
        parent::__construct($config);
        $this->lang = \Yii::$app->language;
        if ($admin) {
            $this->lang = $admin->lang;
        }
    }

    public function rules()
    {
        return [
            ['lang', 'string']
        ];
    }
}