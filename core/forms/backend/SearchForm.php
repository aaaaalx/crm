<?php


namespace core\forms\backend;


use core\entities\Card;
use core\entities\Doctor;
use core\entities\Order;
use core\others\MainSearch;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseVarDumper;

class SearchForm extends Model
{
    public $q;

    public function rules()
    {
        return [
            [['q'], 'string']
        ];
    }

    public function search($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            return null;
        }
    }
}