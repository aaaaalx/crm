<?

namespace core\forms\backend;

use core\entities\Role;
use core\helpers\RolesHelper;
use yii\base\Model;

/**
 *
 * @property int $tasksPermissions
 * @property int $projectsPermissions
 * @property int $projectMembersPermissions
 * @property int $taskMembersPermissions
 * @property int $userPermission
 *
 */

class RoleForm extends Model
{

    public $name;
    public $tasks_permissions = [];
    public $projects_permissions = [];
    public $project_members_permissions = [];
    public $task_members_permissions = [];
    public $user_permission = [];

    private $role;

    public function __construct(Role $role = null, array $config = [])
    {
        parent::__construct($config);
        if ($role) {
            $this->role = $role;
            $this->name = $role->name;
            $this->tasks_permissions = RolesHelper::getTasksRulesArray($this->role);
            $this->project_members_permissions = RolesHelper::getProjectMembersRulesArray($this->role);
            $this->task_members_permissions = RolesHelper::getTaskMembersRulesArray($this->role);
            $this->projects_permissions = RolesHelper::getProjectsRulesArray($this->role);
            $this->user_permission = RolesHelper::getUsersRulesArray($this->role);
        }
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['name'], 'unique', 'targetClass' => Role::class, 'filter' => $this->role ? ['<>', 'name', $this->role->name] : null],
            [['name'], 'string'],
            [['tasks_permissions', 'project_members_permissions', 'task_members_permissions', 'projects_permissions', 'user_permission'], 'each', 'rule' => ['integer']]
        ];
    }

    public function getTasksPermissions(): int
    {
        return array_sum($this->tasks_permissions ?: []);
    }

    public function getProjectsPermissions(): int
    {
        return array_sum($this->projects_permissions ?: []);
    }

    public function getProjectMembersPermissions(): int
    {
        return array_sum($this->project_members_permissions ?: []);
    }

    public function getTaskMembersPermissions(): int
    {
        return array_sum($this->task_members_permissions ?: []);
    }

    public function getUserPermission(): int
    {
        return array_sum($this->user_permission ?: []);
    }
}