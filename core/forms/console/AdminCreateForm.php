<?php


namespace core\forms\console;


use core\entities\Admin;
use core\traits\ConsoleValidationErrorLog;
use yii\base\Model;

class AdminCreateForm extends Model
{
    use ConsoleValidationErrorLog;

    public $email;
    public $password;
    public $role;

    public function __construct(string $role = null, array $config = [])
    {
        parent::__construct($config);
        if ($role) {
            $this->role = $role;
        }
    }

    public function rules()
    {
        return [
            ['email', 'email'],
            ['email', 'trim'],
            ['email', 'unique', 'targetClass' => Admin::class],
            [['email', 'password', 'role'], 'required'],
            [['password', 'role'], 'string'],
            ['role', 'existRole']
        ];
    }

    public function existRole()
    {
        if (\Yii::$app->authManager->getRole($this->role) == null) {
            $this->addError('role', 'Role "' . $this->role . '" does not exist');
            return false;
        }
        return true;
    }
}