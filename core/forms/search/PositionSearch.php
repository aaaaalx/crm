<?php

namespace core\forms\search;


use core\entities\Position;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PositionSearch extends Position
{
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title', 'description'], 'string']
        ];
    }

    public function search($params)
    {
        $query = Position::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title]);
        $query->andFilterWhere(['ilike', 'description', $this->description]);

        return $dataProvider;
    }

}