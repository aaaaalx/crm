<?php

namespace core\forms\search;


use core\entities\Profile;
use yii\data\ActiveDataProvider;

class ProfileSearch extends Profile
{
    public $email;

    public function rules()
    {
        return [
            [['id', 'position_id', 'gender', 'status'], 'integer'],
            [['first_name', 'surname', 'phone', 'email'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = Profile::find();

        $dataProvider = new ActiveDataProvider([

            'query' => $query,
        ]);

        $this->load($params);

        $query->joinWith(['user']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'position_id' => $this->position_id,
            'gender' => $this->gender,
            'status' => $this->status
        ]);

        $query->andFilterWhere(['ilike', 'users.email', $this->email]);
        $query->andFilterWhere(['ilike', 'surname', $this->surname]);
        $query->andFilterWhere(['ilike', 'first_name', $this->first_name]);
        $query->andFilterWhere(['ilike', 'phone', $this->phone]);

        return $dataProvider;
    }

}