<?php

namespace core\forms\search;


use yii\data\ActiveDataProvider;
use core\entities\Version;

/**
 * @property integer $id
 * @property string $version
 * @property string $ctime
 *
 */

class VersionSearch extends Version
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['version', 'ctime'], 'string']
        ];
    }

    public function search($params)
    {
        $query = Version::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'version', $this->version]);

        return $dataProvider;
    }
}