<?php

namespace core\forms\search;


use core\entities\Language;
use yii\data\ActiveDataProvider;

/**
 * @property integer $id
 * @property string $title
 * @property string $iso
 * @property string $ctime
 *
 */

class LanguageSearch extends Language
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'iso', 'ctime'], 'string']
        ];
    }

    public function search($params)
    {
        $query = Language::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'iso', $this->iso]);

        return $dataProvider;
    }
}