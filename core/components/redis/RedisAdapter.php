<?php

namespace core\components\redis;

use core\interfaces\KeyValueStorageInterface;
use yii\redis\Connection;

class RedisAdapter implements KeyValueStorageInterface
{

    private $redis;

    public function __construct(Connection $redis)
    {
        $this->redis = $redis;
    }

    public function set(string $key, string $value): void
    {
        $this->redis->set($key, $value);
    }

    public function get(string $key): string
    {
        return $this->redis->get($key);
    }
}