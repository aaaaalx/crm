<?php

namespace core\entities;

use core\helpers\CommonHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "tasks".
 * @property int $id
 * @property string $title
 * @property string $desc
 * @property int $user_id
 * @property int $project_id
 * @property int $status
 * @property int $priority
 * @property int $type_id
 * @property string $ctime
 * @property string $utime
 * @property string $stime
 * @property string $etime
 * @property string $remember_time
 * @property string $checklist
 */
class Task extends ActiveRecord
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;

    const PRIORITY = [1,2,3,4,5,6,7,8,9,10];

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => ['utime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ctime', 'utime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ctime' => Yii::t('app', 'Create time'),
            'utime' => Yii::t('app', 'Update time'),
        ];
    }

    public static function create($title, $desc, $user_id, $project_id, $priority, $type_id, $stime, $etime, $remember_time, $checklist, $status): self
    {
        $tasks = new self();
        $tasks->title = $title;
        $tasks->desc = $desc;
        $tasks->user_id = $user_id;
        $tasks->project_id = $project_id;
        $tasks->status = $status;
        $tasks->priority = $priority;
        $tasks->type_id = $type_id;
        $tasks->ctime = $ctime;
        $tasks->utime = $utime;
        $tasks->stime = $stime;
        $tasks->etime = $etime;
        $tasks->remember_time = $remember_time;
        $tasks->checklist = $checklist;
        return $tasks;
    }

    public function edit($title, $desc, $user_id, $project_id, $priority, $type_id, $stime, $etime, $remember_time, $checklist, $status): void
    {
        $this->title = $title;
        $this->desc = $desc;
        $this->user_id = $user_id;
        $this->project_id = $project_id;
        $this->status = $status;
        $this->priority = $priority;
        $this->type_id = $type_id;
        $this->ctime = $ctime;
        $this->utime = $utime;
        $this->stime = $stime;
        $this->etime = $etime;
        $this->remember_time = $remember_time;
        $this->checklist = $checklist;
    }

    public function afterFind(){
        parent::afterFind();
        $this->checklist = json_decode($this->checklist, true);
    }

    public function beforeSave($insert)
    {
        $this->checklist = json_encode($this->checklist);
        return parent::beforeSave($insert);
    }

    public function isStatus($status){
        return $this->status === $status;
    }

    public function setStatusDeleted()
    {
        $this->status = self::DELETED;
    }

    public function getCrew(){
        return $this->hasMany(TaskCrew::class, ['task_id' => 'id']);
    }

    public function getRelations(){
        return $this->hasMany(TaskRelations::class, ['task_id' => 'id']);
    }

    public function getTags(){
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->via('tagsBind');
    }

    public function getFiles(){
        return $this->hasMany(TaskFiles::class, ['task_id' => 'id']);
    }

    public function getTagsBind(){
        return $this->hasMany(TagsBind::class, ['entity_id' => 'id'])->andOnCondition(['entity' => CommonHelper::mb_basename(self::class)]);
    }

    public function getUser(){
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getProject(){
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function getRole($user_id){
        $users = $this->crew;
        if($users){
            $users = array_column($users, null, 'user_id');
            if(!empty($users[$user_id]) && $role = $users[$user_id]->role){
                return $role;
            }
        }
        throw new \DomainException(Yii::t('app', 'Error while getting role!'));
    }

}
