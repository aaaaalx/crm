<?php

namespace core\entities;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property string $ctime
 * @property string $utime
 */
class Tag extends ActiveRecord
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => ['utime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ctime', 'utime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'ctime' => Yii::t('app', 'Create time'),
            'utime' => Yii::t('app', 'Update time'),
        ];
    }

    public function getBind(){
        return $this->hasMany(TagsBind::class, ['tag_id' => 'id']);
    }

    public static function create($name, $status = Tag::ACTIVE): self
    {
        $tag = new self();
        $tag->name = $name;
        $tag->status = $status;
        return $tag;
    }

    public function edit($name, $status): void
    {
        $this->name = $name;
        $this->status = $status;
    }

    public function isStatus($status){
        return $this->status === $status;
    }

}
