<?php

namespace core\entities;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_crew".
 *
 * @property int $id
 * @property int $task_id
 * @property int $user_id
 * @property int $role
 */
class TaskCrew extends ActiveRecord
{
    //users roles
    const RESPONSIBLE = 1;
    const SUBCONTRACTORS = 2;
    const OBSERVERS = 3;
    const DIRECTOR = 4;
    const REVIEWER = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_crew';
    }

    public static function create($task_id, $user_id, $role): self
    {
        $taskCrew = new self();
        $taskCrew->task_id = $task_id;
        $taskCrew->user_id = $user_id;
        $taskCrew->role = $role;
        return $taskCrew;
    }

    public function edit($task_id, $user_id, $role): void
    {
        $this->task_id = $task_id;
        $this->user_id = $user_id;
        $this->role = $role;
    }

    public function getTask(){
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    public function getUser(){
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
