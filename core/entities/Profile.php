<?php

namespace core\entities;


use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class Profile
 * @package core\entities
 * @property string $id
 * @property string $user_id
 * @property string $phone
 * @property string $position_id
 * @property string $first_name
 * @property string $patronymic
 * @property string $surname
 * @property string $additional_email
 * @property string $avatar
 * @property int $gender
 * @property string $country
 * @property string $city
 * @property string $lang_id
 * @property string $time_zone
 * @property int $ctime
 * @property int $utime
 * @property int $status
 */
class Profile extends ActiveRecord
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;
    const STATUS_BLOCKED = 3;

    public static function tableName()
    {
        return 'profiles';
    }

    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['user', 'lang', 'position'],
            ],
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => ['utime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function fields()
    {
        return [
            'id',
            'user_id',
            'phone',
            'position',
            'first_name',
            'patronymic',
            'surname',
            'additional_email',
            'gender',
            'country',
            'city',
            'lang',
            'time_zone',
            'status'
        ];
    }

    public static function create($user_id, $phone, $position_id, $first_name, $patronymic, $surname, $additional_email, $gender, $country, $city, $lang_id, $time_zone)
    {
        $profile = new self();
        $profile->user_id = $user_id;
        $profile->phone = $phone?:null;
        $profile->position_id = $position_id;
        $profile->first_name = $first_name;
        $profile->patronymic = $patronymic;
        $profile->surname = $surname;
        $profile->additional_email = $additional_email;
        $profile->gender = $gender;
        $profile->country = $country;
        $profile->city = $city;
        $profile->lang_id = $lang_id;
        $profile->time_zone = $time_zone;
        $profile->status = self::STATUS_ACTIVE;
        return $profile;

    }

    public function edit($user_id, $phone, $position_id, $first_name, $patronymic, $surname, $additional_email, $gender, $country, $city, $lang_id, $time_zone)
    {
        $this->user_id = $user_id;
        $this->phone = $phone?:null;
        $this->position_id = $position_id;
        $this->first_name = $first_name;
        $this->patronymic = $patronymic;
        $this->surname = $surname;
        $this->additional_email = $additional_email;
        $this->gender = $gender;
        $this->country = $country;
        $this->city = $city;
        $this->lang_id = $lang_id;
        $this->time_zone = $time_zone;
        return $this;
    }

    public function setStatusDeleted()
    {
        $this->status = self::STATUS_DELETED;
    }

    public function isStatusDeleted()
    {
        return $this->status = self::STATUS_DELETED;
    }

    public function checkStatusDelete()
    {
        if ($this->isStatusDeleted()){
            throw new \DomainException('This is profile was deleted');
        }
    }

    public function getUser()
    {
        return $this->hasMany(User::class, ['id' => 'user_id']);
    }

    public function getLang()
    {
        return $this->hasOne(Language::class, ['id' => 'lang_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(Position::class, ['id' => 'position_id']);
    }

}