<?php

namespace core\entities;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "projects_options".
 *
 * @property int $id
 * @property int $project_id
 * @property int $option_id
 * @property string $value
 */
class ProjectOptions extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects_options';
    }

    public static function create($project_id, $option_id, $value): self
    {
        $projectOptions = new self();
        $projectOptions->project_id = $project_id;
        $projectOptions->option_id = $option_id;
        $projectOptions->value = $value;
        return $projectOptions;
    }

    public function edit($project_id, $option_id, $value): void
    {
        $this->project_id = $project_id;
        $this->option_id = $option_id;
        $this->value = $value;
    }

    public function getProject(){
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function getOption(){
        return $this->hasOne(Options::class, ['id' => 'option_id']);
    }

}
