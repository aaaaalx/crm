<?php

namespace core\entities;

use Yii;

/**
 * This is the model class for table "tags_bind".
 *
 * @property int $id
 * @property int $tag_id
 * @property int $entity_id
 * @property string $entity
 */
class TagsBind extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags_bind';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tag_id', 'entity_id', 'entity'], 'required'],
            [['tag_id', 'entity_id'], 'default', 'value' => null],
            [['tag_id', 'entity_id'], 'integer'],
            [['entity'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'entity' => Yii::t('app', 'Entity'),
        ];
    }

    public static function create($tag_id, $entity_id, $entity_name): self
    {
        $tagBind = new self();
        $tagBind->tag_id = $tag_id;
        $tagBind->entity_id = $entity_id;
        $tagBind->entity = $entity_name;
        return $tagBind;
    }
}
