<?php

namespace core\entities;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_comments".
 *
 * @property int $id
 * @property string $text
 * @property int $status
 * @property int $task_id
 * @property int $user_id
 * @property string $ctime
 * @property string $utime
 */
class TaskComments extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_comments';
    }

    public static function create($text, $status, $task_id, $user_id, $ctime, $utime): self
    {
        $taskComments = new self();
        $taskComments->text = $text;
        $taskComments->status = $status;
        $taskComments->task_id = $task_id;
        $taskComments->user_id = $user_id;
        $taskComments->ctime = $ctime;
        $taskComments->utime = $utime;
        return $taskComments;
    }

    public function edit($text, $status, $task_id, $user_id, $ctime, $utime): void
    {
        $this->text = $text;
        $this->status = $status;
        $this->task_id = $task_id;
        $this->user_id = $user_id;
        $this->ctime = $ctime;
        $this->utime = $utime;
    }

    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
