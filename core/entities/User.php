<?php

namespace core\entities;

use core\interfaces\UserInterface;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;
use yii\db\Expression;

/**
* User model
*
* @property integer $id
* @property string $email
* @property string $password_hash
* @property string $password_reset_token
* @property string $auth_key
* @property string $access_token
* @property integer $time_zone
* @property integer $status
* @property integer $ctime
* @property integer $utime
* @property string $password write-only password
*/

class User extends ActiveRecord implements UserInterface
{

    const STATUS_MODERATE = 10;
    const STATUS_ACTIVE = 20;
    const STATUS_DELETED = 30;

    public static function tableName()
    {
        return '{{%users}}';
    }

    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'profile', 'userRole', 'role'
                ],
            ],
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => ['utime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function fields()
    {
        return [
            'id',
            'email',
            'access_token',
            'time_zone',
            'status',
            'profile'
        ];
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    private function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    private function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates "access_token
     */
    private function generateAccessToken()
    {
        $this->access_token = Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString());
    }

    public static function create(string $email, string $password, int $timezone): self
    {
        $user = new self;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generateAccessToken();
        $user->time_zone = $timezone;
        return $user;
    }

    public function edit(string $email, string $password, int $time_zone): self
    {
        $this->email = $email;
        if ($password) {
            $this->setPassword($password);
        }
        $this->time_zone = $time_zone;
        return $this;
    }

    public function setTimezone($timezone): void
    {
        $this->time_zone = $timezone;
    }

    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isModerate(): bool
    {
        return $this->status == self::STATUS_MODERATE;
    }

    public function isDelete(): bool
    {
        return $this->status == self::STATUS_DELETED;
    }

    public function isCanBeActivated(): bool
    {
        return $this->isDelete() || $this->isModerate();
    }

    public function isCanBeModerated(): bool
    {
        return $this->isActive() || $this->isDelete();
    }

    public function isCanBeDeleted()
    {
        return $this->isActive() || $this->isModerate();
    }

    public function guardIsCanBeActivated(): bool
    {
        if ($this->isCanBeActivated()) {
            return true;
        }
        throw new \DomainException(Yii::t('models.errors','Can not be activated'));
    }

    public function guardIsCanBeDeleted(): bool
    {
        if ($this->isCanBeDeleted()) {
            return true;
        }
        throw new \DomainException(Yii::t('models.errors','Can not be deleted'));
    }


    public function guardIsCanBeModerate(): bool
    {
        if ($this->isCanBeModerated()) {
            return true;
        }
        throw new \DomainException(Yii::t('models.errors','Can not be send to moderation'));
    }

    public function activate()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function moderate()
    {
        $this->status = self::STATUS_MODERATE;
    }

    public function remove()
    {
        $this->status = self::STATUS_DELETED;
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'id']);
    }

    public function getUserRole()
    {
        return $this->hasOne(UserRole::class, ['user_id' => 'id']);
    }

    public function getRole()
    {
        return $this->hasOne(Role::class, ['id' => 'role_id'])->viaTable('users_role', ['user_id' => 'id']);
    }

}