<?php

namespace core\entities;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property integer $id
 * @property string $version
 * @property string $ctime
 *
 */

class Version extends ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => false,
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{version}}';
    }

    public function fields()
    {
        return [
            'version' => 'version'
        ];
    }

    public static function create($version): self
    {
        $model = new self();
        $model->version = $version;
        return $model;
    }
}