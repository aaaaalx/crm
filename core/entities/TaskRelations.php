<?php

namespace core\entities;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_relations".
 *
 * @property int $id
 * @property int $task_id
 * @property int $related_task_id
 * @property int $relation_type
 */
class TaskRelations extends ActiveRecord
{

    //relation types
    const START_FINISH = 1;
    const FINISH_START = 2;
    const START_START = 3;
    const FINISH_FINISH = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_relations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['task_id', 'related_task_id', 'relation_type'], 'required'],
            [['task_id', 'related_task_id', 'relation_type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'related_task_id' => Yii::t('app', 'Related task ID'),
            'relation_type' => Yii::t('app', 'Relation type'),
        ];
    }

    public static function create($task_id, $related_task_id, $relation_type): self
    {
        $taskRelations = new self();
        $taskRelations->task_id = $task_id;
        $taskRelations->related_task_id = $related_task_id;
        $taskRelations->relation_type = $relation_type;
        return $taskRelations;
    }

    public function edit($task_id, $related_task_id, $relation_type): void
    {
        $this->task_id = $task_id;
        $this->related_task_id = $related_task_id;
        $this->relation_type = $relation_type;
    }

    public function getTask(){
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

}
