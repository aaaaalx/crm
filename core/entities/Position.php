<?php

namespace core\entities;


use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * Class Position
 * @package core\entities
 * @property string $id [integer]
 * @property string $title [varchar(255)]
 * @property string $description
 * @property string $status [integer]
 * @property int $ctime [timestamp(0)]
 * @property int $utime [timestamp(0)]
 */

class Position extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NO_ACTIVE = 0;
    const STATUS_DELETED = 2;

    public static function tableName()
    {
        return 'positions';
    }

    public static function create($title, $description, $status)
    {
        $position = new self();
        $position->title = $title;
        $position->description = $description;
        $position->status = $status;
        return $position;

    }

    public function edit($title, $description, $status)
    {
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;
        return $this;
    }

    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }

    public function setStatusDeleted()
    {
        $this->status = self::STATUS_DELETED;

    }

    public function checkStatusDeleted()
    {
        if ($this->isDeleted()){
            throw new \DomainException('post already deleted');
        }
    }

}