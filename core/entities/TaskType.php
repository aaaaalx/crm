<?php

namespace core\entities;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_type".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class TaskType extends ActiveRecord
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_type';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public static function create($name, $status = TaskType::ACTIVE): self
    {
        $taskType = new self();
        $taskType->name = $name;
        $taskType->status = $status;
        return $taskType;
    }

    public function edit($name, $status): void
    {
        $this->name = $name;
        $this->status = $status;
    }

    public function isStatus($status){
        return $this->status === $status;
    }

    public function setStatusDeleted()
    {
        $this->status = self::DELETED;
    }

}
