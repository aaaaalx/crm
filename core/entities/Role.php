<?php

namespace core\entities;

use yii\db\ActiveRecord;
use Yii;

/**
 * Admin model
 *
 * @property integer $id
 * @property string $name
 * @property string $tasks_permissions
 * @property string $project_permissions
 * @property string $project_member_permissions
 * @property string $task_member_permissions
 * @property string $user_permission
 *
 */

class Role extends ActiveRecord
{

    const CREATE = 1 << 0;
    const READ = 1 << 1;
    const UPDATE = 1 << 2;
    const DELETE = 1 << 3;

    const DEFAULT = 0;
    const ALL = self::CREATE | self::READ | self::UPDATE | self::DELETE;

    public $max_level_permission;

    public static function tableName()
    {
        return 'roles';
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function __construct(array $config = [])
    {
        $this->max_level_permission = self::ALL;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models.attributes', 'ID'),
            'name' => Yii::t('models.attributes', 'Role name'),
            'tasks_permissions' => Yii::t('models.attributes', 'Tasks permissions'),
            'project_permissions' => Yii::t('models.attributes', 'Projects permissions'),
            'project_member_permissions' => Yii::t('models.attributes', 'Members control permissions'),
            'task_member_permissions' => Yii::t('models.attributes', 'Members control permissions'),
            'user_permission' => Yii::t('models.attributes', 'Users control permission')
        ];
    }

    public function fields()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'tasks_permissions' => 'tasks_permissions',
            'project_permissions' => 'project_permissions',
            'project_member_permissions' => 'project_member_permissions',
            'task_member_permissions' => 'task_member_permissions',
            'user_permission' => 'user_permission',
            'max_level_permission' =>'max_level_permission'
        ];
    }

    public static function create($name, $tasks_permissions = self::DEFAULT, $project_permissions = self::DEFAULT, $project_member_permissions = self::DEFAULT, $user_permission = self::DEFAULT, $task_member_permissions = self::DEFAULT): self
    {
        $role = new self();
        $role->name = $name;
        $role->tasks_permissions = $tasks_permissions;
        $role->project_permissions = $project_permissions;
        $role->project_member_permissions = $project_member_permissions;
        $role->task_member_permissions = $task_member_permissions;
        $role->user_permission = $user_permission;
        return $role;
    }

    public function edit($name, $tasks_permissions = self::DEFAULT, $project_permissions = self::DEFAULT, $project_member_permissions = self::DEFAULT, $user_permission = self::DEFAULT, $task_member_permissions = self::DEFAULT): void
    {
        $this->name = $name;
        $this->tasks_permissions = $tasks_permissions;
        $this->project_permissions = $project_permissions;
        $this->project_member_permissions = $project_member_permissions;
        $this->task_member_permissions = $task_member_permissions;
        $this->user_permission = $user_permission;
    }

    public function checkTaskPermission($permission): bool
    {
        return $this->tasks_permissions & $permission;
    }

    public function checkProjectPermission($permission): bool
    {
        return $this->project_permissions & $permission;
    }

    public function checkProjectMemberPermission($permission): bool
    {
        return $this->project_member_permissions & $permission;
    }

    public function checkTaskMemberPermission($permission): bool
    {
        return $this->task_member_permissions & $permission;
    }

    public function checkUserPermission($permission): bool
    {
        return $this->user_permission & $permission;
    }

    public function checkPermission($entity, $permission)
    {
        $method = 'check'.ucfirst($entity).'Permission';
        if (!method_exists($this, $method)){
            throw new \DomainException('Not exist method');
        }
        if (!$this->{$method}($permission)){
            throw new \DomainException('Access denied');
        }
        return true;

    }

    public function getPermissionAll()
    {
        $this->permissionAll = self::ALL;
    }
}