<?php

namespace core\entities;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $access_type
 * @property string $image
 * @property string $stime
 * @property string $etime
 * @property int $status
 * @property string $ctime
 * @property string $utime
 */
class Project extends ActiveRecord
{

    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;
    const ACCESS_OPEN = 1;
    const ACCESS_CLOSED = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => ['utime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ctime', 'utime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'access_type' => Yii::t('app', 'Access Type'),
            'image' => Yii::t('app', 'Image'),
            'stime' => Yii::t('app', 'Start time'),
            'etime' => Yii::t('app', 'End time'),
            'status' => Yii::t('app', 'Status'),
            'ctime' => Yii::t('app', 'Create time'),
            'utime' => Yii::t('app', 'Update time'),
        ];
    }

    public function getCrew(){
        return $this->hasMany(ProjectCrew::class, ['project_id' => 'id']);
    }

    public function getOptions(){
        return $this->hasMany(ProjectOptions::class, ['project_id' => 'id']);
    }

    public function getTags(){
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->via('tagsBind');
    }

    public function getTagsBind(){
        return $this->hasMany(TagsBind::class, ['entity_id' => 'id'])->andOnCondition(['entity' => basename(self::class)]);
    }

    public static function create($title, $description, $access_type, $stime, $etime, $status): self
    {
        $project = new self();
        $project->title = $title;
        $project->description = $description;
        $project->access_type = $access_type;
        $project->stime = $stime;
        $project->etime = $etime;
        $project->status = $status;
        return $project;
    }

    public function edit($title, $description, $access_type, $stime, $etime, $status): void
    {
        $this->title = $title;
        $this->description = $description;
        $this->access_type = $access_type;
        $this->stime = $stime;
        $this->etime = $etime;
        $this->status = $status;
    }

    public function isStatus($status){
        return $this->status === $status;
    }

    public function setStatusDeleted()
    {
        $this->status = self::DELETED;
    }

    public function getRole($user_id){
        $users = $this->crew;
        if($users){
            $users = array_column($users, null, 'user_id');
            if(!empty($users[$user_id]) && $role = $users[$user_id]->role){
                return $role;
            }
        }
        throw new \DomainException(Yii::t('app', 'Error while getting role!'));
    }

}
