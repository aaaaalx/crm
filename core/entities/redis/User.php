<?php


namespace core\entities\redis;


use core\helpers\DumpHelper;
use core\interfaces\UserInterface;
use core\queries\redis\UserQuery;
use yii\redis\ActiveRecord;
use Yii;

/**
 * User model
 *
 * @property integer $id
 * @property string $email
 * @property string $password_hash
 * @property string $access_token
 * @property integer $time_zone
 * @property integer $status
 * @property integer $ctime
 */

class User extends ActiveRecord implements UserInterface
{
    public function attributes()
    {
        return ['id', 'access_token', 'email', 'password_hash', 'time_zone', 'status', 'ctime'];
    }

    public function fields()
    {
        return [
            'id',
            'access_token',
            'email',
            'status',
            'time_zone',
            'ctime'
        ];
    }

    public static function create($id, $access_token, $email, $password_hash, $time_zone, $status, $ctime)
    {
        $user = new self();
        $user->id = $id;
        $user->access_token = $access_token;
        $user->email = $email;
        $user->password_hash = $password_hash;
        $user->time_zone = $time_zone;
        $user->status = $status;
        $user->ctime = $ctime;
        return $user;
    }

    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }


}