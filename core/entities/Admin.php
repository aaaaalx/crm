<?php

namespace core\entities;

use core\helpers\LanguagesHelper;
use developeruz\db_rbac\interfaces\UserRbacInterface;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;
use yii\db\Expression;

/**
* Admin model
*
* @property integer $id
* @property string $email
* @property string $password_hash
* @property string $password_reset_token
* @property string $auth_key
* @property string $lang
* @property integer $time_zone
* @property integer $status
* @property integer $ctime
* @property integer $utime
* @property string $password write-only password
*/

class Admin extends ActiveRecord
{

    const STATUS_MODERATE = 10;
    const STATUS_ACTIVE = 20;
    const STATUS_DELETED = 30;

    public static function tableName()
    {
        return '{{%admins}}';
    }

    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                ],
            ],
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => ['utime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    private function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    private function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public static function create(string $email, string $password, int $timezone, string $lang): self
    {
        $admin = new self;
        $admin->email = $email;
        $admin->setPassword($password);
        $admin->generateAuthKey();
        $admin->time_zone = $timezone;
        $admin->status = self::STATUS_ACTIVE;
        $admin->setLang($lang);
        return $admin;
    }

    public function edit(string $email, string $password, int $timezone, string $lang): self
    {
        $this->email = $email;
        if ($password) {
            $this->setPassword($password);
        }
        $this->time_zone = $timezone;
        $this->setLang($lang);
        return $this;
    }

    public function setTimezone($timezone): void
    {
        $this->time_zone = $timezone;
    }

    public function setLang($lang): void
    {
        if (in_array($lang, LanguagesHelper::getLanguages())) {
            $this->lang = $lang;
        }
    }

    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function guardIsActivatable(): bool
    {
        if (!$this->isActive()) {
            return true;
        }
        throw new \DomainException(Yii::t('models.errors','Already activated'));
    }

    public function setActiveStatus()
    {
        if ($this->guardIsActivatable()) {
            $this->status = self::STATUS_ACTIVE;
        }
    }

    public function isModerate(): bool
    {
        return $this->status == self::STATUS_MODERATE;
    }

    public function guardIsModerate(): bool
    {
        if (!$this->isModerate()) {
            return true;
        }
        throw new \DomainException(Yii::t('models.errors','Already on moderation'));
    }

    public function setModerateStatus()
    {
        if ($this->guardIsModerate()) {
            $this->status = self::STATUS_MODERATE;
        }
    }
}