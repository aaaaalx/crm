<?php

namespace core\entities;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "task_files".
 *
 * @property int $id
 * @property string $file
 * @property int $task_id
 * @property string $ctime
 */
class TaskFiles extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_files';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    public static function create($task_id, $fileName): self
    {
        $taskFiles = new self();
        $taskFiles->file = $fileName;
        $taskFiles->task_id = $task_id;
        return $taskFiles;
    }

    public function edit($file, $task_id): void
    {
        $this->file = $file;
        $this->task_id = $task_id;
    }

    public function getTask(){
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

}
