<?php

namespace core\entities;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_history".
 *
 * @property int $id
 * @property int $task_id
 * @property string $changelog
 * @property string $ctime
 */
class TaskHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_history';
    }

    public static function create($task_id, $changelog, $ctime): self
    {
        $taskHistory = new self();
        $taskHistory->task_id = $task_id;
        $taskHistory->changelog = $changelog;
        $taskHistory->ctime = $ctime;
        return $taskHistory;
    }

    public function edit($task_id, $changelog, $ctime): void
    {
        $this->task_id = $task_id;
        $this->changelog = $changelog;
        $this->ctime = $ctime;
    }

    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

}
