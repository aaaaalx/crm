<?php

namespace core\entities;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "options".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 */
class Options extends \yii\db\ActiveRecord
{
    const TYPE_INT = 1;
    const TYPE_STRING = 2;
    const TYPE_LIST = 3;
    const TYPE_BOOL = 4;
    const TYPE_MULTIPLE_LIST = 5;
    const TYPE_DATE = 6;

    public $variants;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'options';
    }

    public static function crete($name, $type, $variants = [])
    {
        $option = new self();
        $option->name = $name;
        $option->type = $type;
        $option->variants = $variants;
        return $option;
    }

    public function edit($name, $type, $variants = [])
    {
        $this->name = $name;
        $this->type = $type;
        $this->variants = $variants;
        return $this;
    }

    public function afterFind(): void
    {
        if ($variants = $this->getAttribute('variants_json')) {
            $this->variants = array_filter(Json::decode($variants));
        }
        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        if (!empty($this->variants)) {
            $this->setAttribute('variants_json', Json::encode(array_filter($this->variants)));
        }
        return parent::beforeSave($insert);
    }

    public function isString(): bool
    {
        return $this->type === self::TYPE_STRING;
    }

    public function isInteger(): bool
    {
        return $this->type === self::TYPE_INT;
    }

    public function isBool(): bool
    {
        return $this->type === self::TYPE_BOOL;
    }

    public function isList(): bool
    {
        return $this->type === self::TYPE_LIST;
    }

    public function isMultipleList(): bool
    {
        return $this->type === self::TYPE_MULTIPLE_LIST;
    }

    public function isDate(): bool
    {
        return $this->type === self::TYPE_DATE;
    }
}
