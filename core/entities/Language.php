<?php

namespace core\entities;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

/**
 * @property integer $id
 * @property string $title
 * @property string $iso
 * @property integer $status
 * @property integer $ctime
 * @property integer $utime
 *
 */

class Language extends ActiveRecord
{

    const STATUS_ACTIVE = 10;
    const STATUS_DELETED = 20;

    public static function tableName()
    {
        return 'languages';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['ctime'],
                    self::EVENT_BEFORE_UPDATE => ['utime'],
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ]
        ];
    }

    public static function create($title, $iso): self
    {
        $lang = new self();
        $lang->title = $title;
        $lang->iso = $iso;
        $lang->status = self::STATUS_ACTIVE;
        return $lang;
    }

    public function edit($title, $iso): self
    {
        $this->title = $title;
        $this->iso = $iso;
        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->status == self::STATUS_DELETED;
    }

    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function guardIsCanBeDeleted(): bool
    {
        if ($this->isActive()) {
            return true;
        }
        throw new \DomainException(Yii::t('models.errors','Can not be deleted'));
    }

    public function guardIsCanBeRestored(): bool
    {
        if ($this->isDeleted()) {
            return true;
        }
        throw new \DomainException(Yii::t('models.errors','Can not be restored'));
    }

    public function remove(): void
    {
        $this->status = self::STATUS_DELETED;
    }

    public function restore()
    {
        $this->status = self::STATUS_ACTIVE;
    }
}