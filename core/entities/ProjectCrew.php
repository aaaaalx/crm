<?php

namespace core\entities;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "projects_crew".
 *
 * @property int $id
 * @property int $project_id
 * @property int $user_id
 * @property int $role_id
 */
class ProjectCrew extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects_crew';
    }

    public static function create($project_id, $user_id, $role_id): self
    {
        $projectCrew = new self();
        $projectCrew->project_id = $project_id;
        $projectCrew->user_id = $user_id;
        $projectCrew->role_id = $role_id;
        return $projectCrew;
    }

    public function edit($project_id, $user_id, $role_id): void
    {
        $this->project_id = $project_id;
        $this->user_id = $user_id;
        $this->role_id = $role_id;
    }

    public function getProject(){
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function getUser(){
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getRole(){
        return $this->hasOne(Role::class, ['id' => 'role_id']);
    }

}
