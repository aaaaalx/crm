<?php

namespace core\entities;


use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;

/**
 * Class UserRole
 * @package core\entities
 * @property $id integer
 * @property $user_id integer
 * @property $role_id integer
 */
class UserRole extends ActiveRecord
{
    public static function tableName()
    {
        return 'users_role';
    }

    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'user', 'role'
                ],
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function create($user_id, $role_id)
    {
        $userRole = new self();
        $userRole->user_id = $user_id;
        $userRole->role_id = $role_id;
        return $userRole;
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getRole()
    {
        return $this->hasOne(Role::class, ['id' => 'role_id']);
    }


}