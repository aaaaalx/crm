<?php

namespace core\interfaces;


interface FileInterface
{
    public function getFile();

    public function getMimeType();

    public function getFileName();

    public function getExtension();

    public function saveAs($path);
}