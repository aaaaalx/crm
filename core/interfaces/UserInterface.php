<?php


namespace core\interfaces;


interface UserInterface
{
    public function validatePassword($password);

}