<?php


namespace core\interfaces;


use core\entities\redis\User;

interface UserStorageRepository
{
    public function getById($id);

    public function getByEmail(string $email);

    public function getByToken(string $token);

    public function save(User $user): void;
}