<?php


namespace core\interfaces;


interface KeyValueStorageInterface
{
    public function get(string $key): string;

    public function set(string $key, string $value): void;
}