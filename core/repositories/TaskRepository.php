<?php

namespace core\repositories;

use core\entities\TaskCrew;
use core\entities\Tag;
use core\entities\TagsBind;
use core\entities\TaskFiles;
use core\entities\TaskRelations;
use Yii;
use core\entities\Task;
use core\exceptions\NotFoundException;

class TaskRepository
{
    public function save(Task $tag): void
    {
        if (!$tag->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving task'));
        }
    }

    public function saveCrew(TaskCrew $crew): void
    {
        if (!$crew->save()){
            $task = $this->getById($crew->task_id);
            if($task){
                $task->delete();
            }
            throw new \DomainException(Yii::t('app', 'Error while saving task crew'));
        }
    }

    public function saveRelations(TaskRelations $relation): void
    {
        if (!$relation->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving task options'));
        }
    }

    public function saveFiles(TaskFiles $files): void
    {
        if (!$files->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving task files'));
        }
    }

    public function deleteFiles(TaskFiles $files): void
    {
        if (!$files->delete()){
            throw new \DomainException(Yii::t('app', 'Error while deleting task files'));
        }
    }

    public function saveTag(Tag $tag): Tag
    {
        if ($tag->save()){
            return $tag;
        }
        else{
            throw new \DomainException(Yii::t('app', 'Error while saving task tag'));
        }
    }

    public function saveTagBind(TagsBind $tagBind): void
    {
        if (!$tagBind->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving task tag bind'));
        }
    }

    public function remove(Task $tag): void
    {
        if (!$tag->delete()){
            throw new \DomainException(Yii::t('app', 'Error while deleting task'));
        }
    }

    public function getById($id): Task
    {
        return $this->get(['id' => $id]);
    }

    private function get($condition): Task
    {
        if (!$task = Task::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException(Yii::t('app', 'Task not found.'));
        }
        return $task;
    }

    public function getTask(): ?array
    {
        return Task::find()->all();
    }

    public function getFileById($id): TaskFiles
    {
        if (!$taskFile = TaskFiles::find()->where(['task_id' => $id])->limit(1)->one()) {
            throw new NotFoundException(Yii::t('app', 'Task file not found.'));
        }
        return $taskFile;
    }

}