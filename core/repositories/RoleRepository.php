<?php

namespace core\repositories;

use core\entities\Role;
use core\exceptions\NotFoundException;

class RoleRepository
{
    public function save(Role $role): void
    {
        if (!$role->save()){
            throw new \DomainException('Error while saving role');
        }
    }

    public function remove(Role $role): void
    {
        if (!$role->delete()){
            throw new \DomainException('Error while deleting role');
        }
    }

    public function getById($id): Role
    {
        return $this->get(['id' => $id]);
    }

    private function get($condition): Role
    {
        if (!$role = Role::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Role not found.');
        }
        return $role;
    }

    public function getRoles(): ?array
    {
        return Role::find()->all();
    }
}