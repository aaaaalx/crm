<?php

namespace core\repositories;

use core\entities\Profile;
use core\exceptions\NotFoundException;

class ProfileRepository
{
    public function get($id): Profile
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(Profile $profile): void
    {
        if (!$profile->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Profile $profile): void
    {
        if (!$profile->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function updateAll($condition)
    {
        Profile::updateAll($condition);
    }

    private function getBy(array $condition): Profile
    {
        /**
         * @var Profile $profile
         */
        if (!$profile = Profile::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('User not found.');
        }
        return $profile;
    }

}