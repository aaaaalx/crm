<?php

namespace core\repositories;

use Yii;
use core\entities\TaskType;
use core\exceptions\NotFoundException;

class TaskTypeRepository
{
    public function save(TaskType $tagType): void
    {
        if (!$tagType->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving task type'));
        }
    }

    public function remove(TaskType $tagType): void
    {
        if (!$tagType->delete()){
            throw new \DomainException(Yii::t('app', 'Error while deleting task type'));
        }
    }

    public function getById($id): TaskType
    {
        return $this->get(['id' => $id]);
    }

    private function get($condition): TaskType
    {
        if (!$tagType = TaskType::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException(Yii::t('app', 'Task type not found.'));
        }
        return $tagType;
    }

    public function getTaskType(): ?array
    {
        return TaskType::find()->all();
    }
}