<?php


namespace core\repositories\redis;


use core\entities\redis\User;
use core\helpers\DumpHelper;
use core\interfaces\UserStorageRepository;

class UserRedisRepository implements UserStorageRepository
{
    public function getById($id): ?User
    {
        return User::find()->byId($id)->active()->one();
    }

    public function getByEmail(string $email): ?User
    {
        return User::find()->byEmail($email)->active()->one();
    }

    public function getByToken(string $token): ?User
    {
        return User::find()->byToken($token)->active()->one();
    }

    public function save(User $user): void
    {
        if (!$user->save()) {
            throw new \DomainException('Error while saving to Redis');
        }
    }
}