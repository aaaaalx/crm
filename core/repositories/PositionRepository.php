<?php

namespace core\repositories;

use core\entities\Position;
use core\exceptions\NotFoundException;

class PositionRepository
{
    public function get($id): Position
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(Position $position): void
    {
        if (!$position->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Position $position): void
    {
        if (!$position->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): Position
    {
        /**
         * @var Position $position
         */
        if (!$position = Position::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('User not found.');
        }
        return $position;
    }

}