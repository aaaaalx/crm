<?php

namespace core\repositories;

use core\entities\Options;
use core\exceptions\NotFoundException;

class OptionsRepository
{

    public function get($id): Options
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(Options $options): void
    {
        if (!$options->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Options $options): void
    {
        if (!$options->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): Options
    {
        /**
         * @var Options $options
         */
        if (!$options = Options::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('User not found.');
        }
        return $options;
    }

}