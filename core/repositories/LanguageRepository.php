<?php

namespace core\repositories;

use core\entities\Language;
use core\exceptions\NotFoundException;

class LanguageRepository
{

    public function save(Language $lang): void
    {
        if (!$lang->save()) {
            throw new \DomainException('Saving error');
        }
    }

    public function remove(Language $lang): void
    {
        if (!$lang->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function getById($id): Language
    {
        return $this->get(['id' => $id]);
    }

    public function getByIso($iso): Language
    {
        return $this->get(['iso' => $iso]);
    }

    private function get($condition = []): Language
    {
        /**
         * @var Language $lang
         */
        if (($lang = Language::find()->where($condition)->one()) !== null) {
            return $lang;
        }
        throw new NotFoundException('Language not found');
    }
}