<?php

namespace core\repositories;

use Yii;
use core\entities\Tag;
use core\exceptions\NotFoundException;

class TagRepository
{
    public function save(Tag $tag): void
    {
        if (!$tag->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving tag'));
        }
    }

    public function remove(Tag $tag): void
    {
        if (!$tag->delete()){
            throw new \DomainException(Yii::t('app', 'Error while deleting tag'));
        }
    }

    public function getById($id): Tag
    {
        return $this->get(['id' => $id]);
    }

    private function get($condition): Tag
    {
        if (!$tag = Tag::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException(Yii::t('app', 'Tag not found.'));
        }
        return $tag;
    }

    public function getTags(): ?array
    {
        return Tag::find()->all();
    }
}