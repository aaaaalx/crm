<?php
namespace core\repositories;

use yii\web\NotFoundHttpException;
use core\entities\Admin;

class AdminRepository
{

    public function get($id): Admin
    {
        return $this->getBy(['id' => $id]);
    }

    public function getByEmail($email): Admin
    {
        return $this->getBy(['email' => $email]);
    }

    public function getByPasswordResetToken($token): Admin
    {
        return $this->getBy(['password_reset_token' => $token]);
    }

    public function existsByPasswordResetToken(string $token): bool
    {
        return (bool) Admin::find()->where(['access_token' => $token])->exists();
    }

    public function save(Admin $user): void
    {
        if (!$user->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Admin $user): void
    {
        if (!$user->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): Admin
    {
        /**
         * @var Admin $admin
         */
        if (!$admin = Admin::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('Admin not found.');
        }
        return $admin;
    }
}