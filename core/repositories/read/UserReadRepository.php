<?php

namespace core\repositories\read;

use core\entities\User;

class UserReadRepository
{
    public function find($id): ?User
    {
        return User::findOne($id);
    }

    public function findActiveByEmail($email): ?User
    {
        return User::findOne(['email' => $email, 'status' => User::STATUS_ACTIVE]);
    }

    public function findActiveById($id): ?User
    {
        return User::findOne(['id' => $id, 'status' => User::STATUS_ACTIVE]);
    }

    public function findActiveByToken($token): ?User
    {
        return User::findOne(['access_token' => $token, 'status' => User::STATUS_ACTIVE]);
    }
}