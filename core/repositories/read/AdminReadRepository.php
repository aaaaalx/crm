<?php

namespace core\repositories\read;

use core\entities\Admin;

class AdminReadRepository
{
    public function find($id): ?Admin
    {
        return Admin::findOne($id);
    }

    public function findActiveByEmail($email): ?Admin
    {
        return Admin::findOne(['email' => $email, 'status' => Admin::STATUS_ACTIVE]);
    }

    public function findActiveById($id): ?Admin
    {
        return Admin::findOne(['id' => $id, 'status' => Admin::STATUS_ACTIVE]);
    }

    public function findActiveByToken($token): ?Admin
    {
        return Admin::findOne(['access_token' => $token, 'status' => Admin::STATUS_ACTIVE]);
    }
}