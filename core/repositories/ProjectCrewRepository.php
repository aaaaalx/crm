<?php

namespace core\repositories;

use Yii;
use core\entities\ProjectCrew;
use core\exceptions\NotFoundException;

class ProjectCrewRepository
{
    public function save(ProjectCrew $projectCrew): void
    {
        if (!$projectCrew->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving project crew'));
        }
    }

    public function remove(ProjectCrew $projectCrew): void
    {
        if (!$projectCrew->delete()){
            throw new \DomainException(Yii::t('app', 'Error while deleting project crew'));
        }
    }

    public function getById($id): ProjectCrew
    {
        return $this->get(['id' => $id]);
    }

    private function get($condition): ProjectCrew
    {
        if (!$projectCrew = ProjectCrew::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException(Yii::t('app', 'Project crew not found.'));
        }
        return $projectCrew;
    }

    public function getProjectsCrew(): ?array
    {
        return ProjectCrew::find()->all();
    }
}