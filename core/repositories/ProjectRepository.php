<?php

namespace core\repositories;

use core\entities\ProjectCrew;
use core\entities\ProjectOptions;
use core\entities\Tag;
use core\entities\TagsBind;
use Yii;
use core\entities\Project;
use core\exceptions\NotFoundException;
use yii\helpers\BaseVarDumper;

class ProjectRepository
{

    public function save(Project $project): void
    {
        if (!$project->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving project'));
        }
    }

    public function saveCrew(ProjectCrew $crew): void
    {
        if (!$crew->save()){
            $project = $this->getById($crew->project_id);
            if($project){
                $project->delete();
            }
            throw new \DomainException(Yii::t('app', 'Error while saving project crew'));
        }
    }

    public function saveOptions(ProjectOptions $option): void
    {
        if (!$option->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving project options'));
        }
    }

    public function saveTag(Tag $tag): Tag
    {
        if ($tag->save()){
            return $tag;
        }
        else{
            throw new \DomainException(Yii::t('app', 'Error while saving project tag'));
        }
    }

    public function saveTagBind(TagsBind $tagBind): void
    {
        if (!$tagBind->save()){
            throw new \DomainException(Yii::t('app', 'Error while saving project tag bind'));
        }
    }

    public function remove(Project $project): void
    {
        if (!$project->delete()){
            throw new \DomainException(Yii::t('app', 'Error while deleting project'));
        }
    }

    public function getById($id): Project
    {
        return $this->get(['id' => $id]);
    }

    private function get($condition): Project
    {
        if (!$project = Project::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException(Yii::t('app', 'Project not found.'));
        }
        return $project;
    }

    public function getProjects(): ?array
    {
        return Project::find()->all();
    }
}