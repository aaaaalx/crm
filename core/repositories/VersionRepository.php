<?php


namespace core\repositories;


use core\exceptions\NotFoundException;
use yii\web\NotFoundHttpException;
use core\entities\Version;

class VersionRepository
{
    public function getById($id)
    {
        return Version::findOne(['id' => $id]);
    }

    public function getLast(): ?Version
    {
        /**
         * @var Version $version
         */
        if (!$version = Version::find()->orderBy('id DESC')->one()) {
            throw new NotFoundException('There is no any records');
        }
        return $version;
    }

    public function save(Version $version): void
    {
        if (!$version->save()) {
            throw new \DomainException('Saving error.');
        }
    }

    public function remove(Version $version): void
    {
        if (!$version->delete()) {
            throw new \DomainException('Removing error.');
        }
    }
}