<?php

namespace core\services\console;

use core\entities\Admin;
use core\forms\console\AdminCreateForm;
use yii\db\Exception;
use yii\rbac\DbManager;
use yii\rbac\Permission;
use yii\rbac\Role;

class RbacService
{

    private $authManager;

    public function __construct(DbManager $manager)
    {
        $this->authManager = $manager;
    }

    public function createRole($name, $description): Role
    {
        $role = $this->authManager->createRole($name);
        $role->description = $description;
        $this->authManager->add($role);
        return $role;
    }

    public function createPermission($name, $description): Permission
    {
        $permission = $this->authManager->createPermission($name);
        $permission->description = $description;
        $this->authManager->add($permission);
        return $permission;
    }

    public function addChildren(Role $role, array $permissions)
    {
        foreach ($permissions as $permission) {
            $this->authManager->addChild($role, $permission);
        }
    }

    public function create(AdminCreateForm $form): Admin
    {
        $user = Admin::create($form->email, $form->password, 0);

        try {
            $user->save();
            $newRole = $this->authManager->getRole($form->role);
            $this->authManager->assign($newRole, $user->id);
        } catch (Exception $e) {
            throw new \DomainException('Can not create admin. ');
        }

        return $user;
    }

    public function roles()
    {
        return $this->authManager->getRoles();
    }
}