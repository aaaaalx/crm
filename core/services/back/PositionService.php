<?php
namespace core\services\back;

use core\entities\Position;
use \core\repositories\PositionRepository;

class PositionService
{
    public $positionRepository;

    public function __construct(PositionRepository $positionRepository)
    {
        $this->positionRepository = $positionRepository;

    }

    public function create($form)
    {
        $position = Position::create($form->title, $form->description, $form->status);
        $this->positionRepository->save($position);
        return $position;
    }

    public function edit($id, $form)
    {
        $position = $this->positionRepository->get($id);
        $position->edit($form->title, $form->description, $form->status);
        $this->positionRepository->save($position);
        return $position;
    }

    public function delete($id)
    {
        $position = $this->positionRepository->get($id);
        $position->checkStatusDeleted();
        $position->setStatusDeleted();
        $this->positionRepository->save($position);
    }

}