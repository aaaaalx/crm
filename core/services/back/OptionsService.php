<?php

namespace core\services\back;

use core\entities\Options;
use core\repositories\OptionsRepository;

class OptionsService
{
    public $repository;

    public function __construct(OptionsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create($form)
    {
        $option = Options::crete($form->name, $form->type, $form->variants);
        $this->repository->save($option);
        return $option;
    }

    public function edit($id, $form)
    {
        $option = $this->repository->get($id);
        $option->edit($form->name, $form->type, $form->variants);
        $this->repository->save($option);
        return $option;
    }

    public function remove($id)
    {
        $option = $this->repository->get($id);
        $this->repository->remove($option);
    }

}