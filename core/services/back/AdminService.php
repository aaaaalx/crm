<?php

namespace core\services\back;

use core\entities\Admin;
use core\forms\backend\AdminCreateForm;
use core\forms\backend\AdminUpdateForm;
use core\forms\backend\SetLangForm;
use core\helpers\TransactionManager;
use core\repositories\AdminRepository;
use yii\rbac\ManagerInterface;

class AdminService
{

    private $adminRepo;
    private $authManager;
    private $transactionManager;

    public function __construct(AdminRepository $adminRepo, ManagerInterface $authManager, TransactionManager $transactionManager)
    {
        $this->adminRepo = $adminRepo;
        $this->authManager = $authManager;
        $this->transactionManager = $transactionManager;
    }

    public function edit($id, AdminUpdateForm $form)
    {
        $admin = $this->adminRepo->get($id);
        $admin->edit($form->email, $form->password, $form->time_zone, $form->lang);
        $this->transactionManager->wrap(function() use ($form, $admin) {
            $this->adminRepo->save($admin);
            $this->setRoles($form->roles, $admin->id);
        });
    }

    public function create(AdminCreateForm $form)
    {
        $admin = Admin::create($form->email, $form->password, $form->time_zone, $form->lang);
        $this->transactionManager->wrap(function() use ($form, $admin) {
            $this->adminRepo->save($admin);
            $this->setRoles($form->roles, $admin->id);
        });
        return $admin;
    }

    public function timezone($id, $timezone)
    {
        $admin = $this->adminRepo->get($id);
        $admin->setTimeZone($timezone);
        $this->adminRepo->save($admin);
        return $admin;
    }

    public function activate($id): void
    {
        $admin = $this->adminRepo->get($id);
        $admin->setActiveStatus();
        $this->adminRepo->save($admin);
    }

    public function moderate($id): void
    {
        $admin = $this->adminRepo->get($id);
        $admin->setModerateStatus();
        $this->adminRepo->save($admin);
    }

    public function setLang(SetLangForm $form, $id): void
    {
        $admin = $this->adminRepo->get($id);
        $admin->setLang($form->lang);
        $this->adminRepo->save($admin);
    }

    private function setRoles(array $roles, $userId)
    {
        $this->authManager->revokeAll($userId);
        foreach ($roles as $newRole) {
            if (($role = $this->authManager->getRole($newRole)) == null) {
                continue;
            }
            $this->authManager->assign($role, $userId);
        }
    }
}