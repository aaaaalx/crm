<?php

namespace core\services\back;

use Yii;
use core\forms\backend\TagForm;
use core\helpers\TagsHelper;
use core\repositories\TagRepository;
use core\entities\Tag;

class TagService
{
    private $tags;

    public function __construct(TagRepository $tags)
    {
        $this->tags = $tags;
    }

    public function create(TagForm $form): Tag
    {
        $tag = Tag::create($form->name, $form->status);
        $this->tags->save($tag);
        return $tag;
    }

    public function edit(TagForm $form, $id): Tag
    {
        $tag = $this->tags->getById($id);
        $tag->edit($form->name, $form->status);
        $this->tags->save($tag);
        return $tag;
    }

    public function changeStatus($id, $status){
        $tag = $this->tags->getById($id);
        if(!$tag->isStatus($status) && TagsHelper::getStatus($status)){
            $tag->status = $status;
            $this->tags->save($tag);
        }
        else{
            throw new \DomainException(Yii::t('app', 'Status was not changed!'));
        }
    }

}