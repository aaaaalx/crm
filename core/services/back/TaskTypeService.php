<?php

namespace core\services\back;

use Yii;
use core\forms\backend\TaskTypeForm;
use core\helpers\TaskTypeHelper;
use core\repositories\TaskTypeRepository;
use core\entities\TaskType;

class TaskTypeService
{
    private $tagsTypes;

    public function __construct(TaskTypeRepository $tagsTypes)
    {
        $this->tagsTypes = $tagsTypes;
    }

    public function create(TaskTypeForm $form): TaskType
    {
        $tagType = TaskType::create($form->name, $form->status);
        $this->tagsTypes->save($tagType);
        return $tagType;
    }

    public function edit(TaskTypeForm $form, $id): TaskType
    {
        $tagType = $this->tagsTypes->getById($id);
        $tagType->edit($form->name, $form->status);
        $this->tagsTypes->save($tagType);
        return $tagType;
    }

    public function changeStatus($id, $status){
        $tagType = $this->tagsTypes->getById($id);
        if(!$tagType->isStatus($status) && TaskTypeHelper::getStatus($status)){
            $tagType->status = $status;
            $this->tagsTypes->save($tagType);
        }
        else{
            throw new \DomainException(Yii::t('app', 'Status was not changed!'));
        }
    }

    public function delete($id)
    {
        $project = $this->tagsTypes->getById($id);
        if($project->isStatus(TaskType::DELETED)){
            throw new \DomainException('The task type already deleted!');
        }
        $project->setStatusDeleted();
        $this->tagsTypes->save($project);
    }

}