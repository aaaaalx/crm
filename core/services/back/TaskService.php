<?php

namespace core\services\back;

use core\entities\Tag;
use core\entities\TagsBind;
use core\entities\TaskCrew;
use core\entities\TaskFiles;
use core\entities\TaskRelations;
use core\forms\backend\compositeForm\TaskCreateForm;
use core\forms\backend\compositeForm\TaskUpdateForm;
use core\helpers\CommonHelper;
use Yii;
use core\forms\backend\TaskForm;
use core\helpers\TaskHelper;
use core\repositories\TaskRepository;
use core\entities\Task;
use core\services\common\ManageFilesService;
use yii\helpers\BaseVarDumper;

class TaskService
{
    private $tasks;
    private $fileService;

    public function __construct(TaskRepository $tasks, ManageFilesService $fileService)
    {
        $this->tasks = $tasks;
        $this->fileService = $fileService;
    }

    public function create(TaskCreateForm $form, $path): Task
    {
        $task = Task::create($form->title, $form->desc, $form->user_id, $form->project_id, $form->priority, $form->type_id, $form->stime, $form->etime, $form->remember_time, $form->checklist, $form->status);

        if(is_array($form->list)){
            $list = [];
            foreach ($form->list as $item){
                $list[] = ['text' => $item->text, 'status' => $item->status];
            }
            $task->checklist = $list;
        }

        $this->tasks->save($task);

        if (!empty($form->uploadedFile)) {
            foreach ($form->uploadedFile as $file) {
                $fileNames = $this->fileService->upload($file, $path . '/web/uploads/tasks/' . $task->id . '/');
                if($fileNames){
                    $this->tasks->saveFiles(TaskFiles::create($task->id, $fileNames));
                }
            }
        }

        if($task->id){

            if(is_array($form->crew)){
                foreach ($form->crew as $crew){
                    if(!empty($crew->user_id) && !empty($crew->role)) {
                        $this->tasks->saveCrew(TaskCrew::create($task->id, $crew->user_id, $crew->role));
                    }
                }
            }

            if(is_array($form->relations)){
                foreach ($form->relations as $relation){
                    if(!empty($relation->related_task_id) && !empty($relation->relation_type)) {
                        $this->tasks->saveRelations(TaskRelations::create($task->id, $relation->related_task_id, $relation->relation_type));
                    }
                }
            }

            if(is_array($form->files)){
                foreach ($form->files as $file){
                    if(!empty($file->related_task_id)) {
                        $this->tasks->saveFiles(TaskFiles::create($file, $task->id));
                    }
                }
            }

            if(isset($form->tags['name']) && is_array($form->tags['name'])){
                $exists = array_column(Tag::find()->where(['in', 'id', array_filter($form->tags['name'], 'is_numeric')])->all(), null, 'id');
                foreach ($form->tags['name'] as $name){
                    if(!isset($exists[$name])){
                        $tag = $this->tasks->saveTag(Tag::create($name));
                        $this->tasks->saveTagBind(TagsBind::create($tag->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                    else{
                        $this->tasks->saveTagBind(TagsBind::create($exists[$name]->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                }
            }


        }
        return $task;
    }

    public function edit(TaskUpdateForm $form, $path, $task_id): Task
    {
        $task = $this->tasks->getById($task_id);
        $task->edit($form->title, $form->desc, $form->user_id, $form->project_id, $form->priority, $form->type_id, $form->stime, $form->etime, $form->remember_time, $form->checklist, $form->status);

        if(is_array($form->list)){
            $list = [];
            foreach ($form->list as $item){
                $list[] = ['text' => $item->text, 'status' => $item->status];
            }
            $task->checklist = $list;
        }

        $this->tasks->save($task);

        if (!empty($form->uploadedFile)) {
            $this->fileService->deleteDirectory($path . '/web/uploads/projects/' . $task->id . '/');
            foreach ($form->uploadedFile as $file) {
                $fileNames = $this->fileService->upload($file, $path . '/web/uploads/tasks/' . $task->id . '/');
                if($fileNames){
                    $this->tasks->saveFiles(TaskFiles::create($task->id, $fileNames));
                }
            }
        }

        if($task->id){

            if(is_array($form->crew)){
                foreach ($task->crew as $user){
                    $user->delete();
                }
                foreach ($form->crew as $crew){
                    if(!empty($crew->user_id) && !empty($crew->role)){
                        $this->tasks->saveCrew(TaskCrew::create($task->id, $crew->user_id, $crew->role));
                    }
                }
            }

            if(is_array($form->relations)){
                foreach ($task->relations as $relation){
                    $relation->delete();
                }
                foreach ($form->relations as $relation){
                    if(!empty($relation->related_task_id) && !empty($relation->relation_type)) {
                        $this->tasks->saveRelations(TaskRelations::create($task->id, $relation->related_task_id, $relation->relation_type));
                    }
                }
            }

            if(is_array($form->files)){
                foreach ($form->files as $file){
                    if(!empty($file->related_task_id)) {
                        $this->tasks->saveFiles(TaskFiles::create($file, $task->id));
                    }
                }
            }

            if(isset($form->tags['name']) && is_array($form->tags['name'])){
                $exists = array_column(Tag::find()
                    ->where(['in', 'id', array_filter($form->tags['name'], 'is_numeric')])
                    ->orWhere(['in', 'name', $form->tags['name']])
                    ->all(), null, 'id');
                $exists_names = array_column($exists, 'id', 'name');
                foreach ($task->tagsBind as $bind){
                    $bind->delete();
                }
                foreach ($form->tags['name'] as $name){
                    if(!empty($exists_names[$name])){
                        $name = $exists_names[$name];
                    }
                    if(empty($exists[$name]->name)){
                        $tag = $this->tasks->saveTag(Tag::create($name));
                        $this->tasks->saveTagBind(TagsBind::create($tag->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                    else{

                        $this->tasks->saveTagBind(TagsBind::create($exists[$name]->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                }
            }
        }
        return $task;
    }

    public function changeStatus($id, $status){
        $task = $this->tasks->getById($id);
        if(!$task->isStatus($status) && TaskHelper::getStatus($status)){
            $task->status = $status;
            $this->tasks->save($task);
        }
        else{
            throw new \DomainException('Status was not changed!');
        }
    }

    public function removeImage($id, $path)
    {
        $taskFile = $this->tasks->getFileById($id);
        if ($taskFile){
            $this->fileService->deleteDirectory($path.'/web/uploads/tasks/'.$taskFile->task->id);
            $taskFile->file = null;
        }
        $this->tasks->deleteFiles($taskFile);
    }

    public function delete($id)
    {
        $task = $this->tasks->getById($id);
        if($task->isStatus(Task::DELETED)){
            throw new \DomainException('The task already deleted!');
        }
        $task->setStatusDeleted();
        $this->tasks->save($task);
    }

}