<?php

namespace core\services\back;

use core\forms\backend\RoleForm;
use core\helpers\DumpHelper;
use core\repositories\RoleRepository;
use core\entities\Role;

class RoleService
{
    private $roles;

    public function __construct(RoleRepository $roles)
    {
        $this->roles = $roles;
    }

    public function create(RoleForm $form): Role
    {
        $role = Role::create($form->name, $form->tasksPermissions, $form->projectsPermissions, $form->projectMembersPermissions, $form->userPermission, $form->taskMembersPermissions);
        $this->roles->save($role);
        return $role;
    }

    public function edit(RoleForm $form, $id): Role
    {
        $role = $this->roles->getById($id);
        $role->edit($form->name, $form->tasksPermissions, $form->projectsPermissions, $form->projectMembersPermissions, $form->userPermission, $form->taskMembersPermissions);
        $this->roles->save($role);
        return $role;
    }
}