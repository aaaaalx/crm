<?php

namespace core\services\back;

use core\entities\Admin;
use core\forms\backend\LoginForm;
use core\forms\backend\SignupForm;
use core\repositories\AdminRepository;

class AuthService
{

    private $repo;

    public function __construct(AdminRepository $repo)
    {
        $this->repo = $repo;
    }

    public function auth(LoginForm $form): Admin
    {
        $admin = $this->repo->getByEmail($form->email);
        if (!$admin || !$admin->isActive() || !$admin->validatePassword($form->password)) {
            throw new \DomainException('Undefined user or password.');
        }
        return $admin;
    }


    public function signup(SignupForm $form): Admin
    {
        $admin = Admin::create($form->email, $form->password, $form->time_zone);
        $this->repo->save($admin);
        return $admin;
    }
}