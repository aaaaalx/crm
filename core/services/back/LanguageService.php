<?php

namespace core\services\back;

use core\entities\Language;
use core\forms\backend\LanguageForm;
use core\repositories\LanguageRepository;

class LanguageService
{
    private $languages;

    public function __construct(LanguageRepository $languages)
    {
        $this->languages = $languages;
    }

    public function create(LanguageForm $form)
    {
        $lang = Language::create($form->title, $form->iso);
        $this->languages->save($lang);
        return $lang;
    }

    public function edit(LanguageForm $form, $id)
    {
        $lang = $this->languages->getById($id);
        $lang->edit($form->title, $form->iso);
        $this->languages->save($lang);
        return $lang;
    }

    public function remove($id): Language
    {
        $lang = $this->languages->getById($id);
        $lang->guardIsCanBeDeleted();
        $lang->remove();
        $this->languages->save($lang);
        return $lang;
    }

    public function restore($id): Language
    {
        $lang = $this->languages->getById($id);
        $lang->guardIsCanBeRestored();
        $lang->restore();
        $this->languages->save($lang);
        return $lang;
    }
}