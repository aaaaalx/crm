<?php

namespace core\services\common;


use core\entities\Version;
use core\forms\backend\VersionCreateForm;
use core\repositories\VersionRepository;

class VersionService
{

    private $repo;

    public function __construct(VersionRepository $repository)
    {
        $this->repo = $repository;
    }

    public function create(VersionCreateForm $form): Version
    {
        $version = Version::create($form->version);
        $this->repo->save($version);
        return $version;
    }
}