<?php

namespace core\services\common;

use core\entities\Profile;
use core\forms\backend\ProfileCreateForm;
use core\forms\backend\ProfileUpdateForm;
use core\helpers\FileHelper;
use core\repositories\ProfileRepository;
use yii\base\Exception;
use core\helpers\TransactionManager;


class ProfileService
{

    private $profiles;
    private $fileService;
    private $transaction;

    public function __construct(ProfileRepository $profiles, ManageFilesService $fileService, TransactionManager $transaction)
    {
        $this->profiles = $profiles;
        $this->fileService = $fileService;
        $this->transaction = $transaction;
    }

    public function create(ProfileCreateForm $form, $path)
    {
        $profile = Profile::create($form->user_id, $form->phone, $form->position_id, $form->first_name, $form->patronymic,
            $form->surname, $form->additional_email, $form->gender, $form->country, $form->city, $form->lang_id, $form->time_zone
        );
        $this->transaction->wrap(function () use ($profile, $form, $path){
            $this->profiles->save($profile);
            if (!empty($form->avatar)) {
                $url = $this->fileService->upload($form->avatar, $path.'/web/uploads/profiles/'.$profile->id. '/');
                $profile->avatar = $url;
            }
            $this->profiles->save($profile);
        });

    }

    public function edit($id, ProfileUpdateForm $form, $path)
    {
        $profile = $this->profiles->get($id);
        $profile->edit($form->user_id, $form->phone, $form->position_id, $form->first_name, $form->patronymic,
            $form->surname, $form->additional_email, $form->gender, $form->country, $form->city,
            $form->lang_id, $form->time_zone
        );
        $this->transaction->wrap(function () use ($profile, $form, $path) {
            $this->profiles->save($profile);

            if (!empty($form->avatar)) {
                if (!empty($profile->avatar)) {
                    $this->fileService->deleteFile($path . '/web/uploads/profiles/' . $profile->id . '/' . $profile->avatar);
                }
                $url = $this->fileService->upload($form->avatar, $path . '/web/uploads/profiles/' . $profile->id . '/');
                $profile->avatar = $url;
            }
            $this->profiles->save($profile);
        });
    }

    public function delete($id)
    {
        $profile = $this->profiles->get($id);
        $profile->checkStatusDelete();
        $profile->setStatusDeleted();
        $this->profiles->save($profile);
    }

    public function removeImage($id, $path)
    {
        $profile = $this->profiles->get($id);
        if ($profile->avatar){
            $this->fileService->deleteDirectory($path.'/web/uploads/profiles/'.$profile->id);
            $profile->avatar = null;
        }
        $this->profiles->save($profile);
    }

}