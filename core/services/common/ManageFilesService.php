<?php

namespace core\services\common;

use core\helpers\DumpHelper;
use core\helpers\FileHelper;
use core\interfaces\FileInterface;
use core\helpers\Base64File;
use yii\helpers\BaseVarDumper;

class ManageFilesService
{

    public function uploadBase64($base64string, $path)
    {
       $fileImage = new Base64File($base64string);
       $fileImage->saveAs($path, $fileImage->getFileName());
    }

    public function deleteFile($path)
    {
        FileHelper::removeFile($path);
    }

    public function deleteDirectory($path)
    {
        FileHelper::removeDirectory($path);
    }

    public function upload(FileInterface $file, $path)
    {
        if ($file->saveAs($path)){
            return $file->getFileName();
        }
    }

}