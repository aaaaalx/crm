<?php

namespace core\services\common;

use core\entities\User;
use core\entities\UserRole;
use core\forms\backend\UserRoleForm;
use core\forms\common\UserForm;
use core\forms\common\UserUpdateForm;
use core\repositories\UserRepository;

class UserService
{

    private $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function create(UserForm $form): User
    {
        $user = User::create($form->email, $form->password, $form->timeZone);
        $this->users->save($user);
        return $user;
    }

    public function edit(UserUpdateForm $form, $id): User
    {
        $user = $this->users->get($id);
        $user->edit($form->email, $form->password, $form->timeZone);
        $this->users->save($user);
        return $user;
    }

    public function view($id): User
    {
        return $this->users->get($id);
    }

    public function remove($id): User
    {
        $user = $this->users->get($id);
        $user->guardIsCanBeDeleted();
        $user->remove();
        $this->users->save($user);
        return $user;
    }

    public function activate($id): User
    {
        $user = $this->users->get($id);
        $user->guardIsCanBeActivated();
        $user->activate();
        $this->users->save($user);
        return $user;
    }

    public function moderate($id): User
    {
        $user = $this->users->get($id);
        $user->guardIsCanBeModerate();
        $user->moderate();
        $this->users->save($user);
        return $user;
    }

    public function assignRole(UserRoleForm $form)
    {
        $user = $this->users->get($form->user_id);
        $user->userRole = UserRole::create($form->user_id, $form->role_id);
        $this->users->save($user);
    }

}