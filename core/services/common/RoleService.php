<?php

namespace core\services\common;

use core\repositories\RoleRepository;

class RoleService
{
    private $roles;

    public function __construct(RoleRepository $roles)
    {
        $this->roles = $roles;
    }

    public function rolesList(): array
    {
        return $this->roles->getRoles();
    }
}