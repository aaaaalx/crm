<?php

namespace core\services\api;

use core\entities\Role;
use core\entities\Tag;
use core\entities\TagsBind;
use core\entities\TaskCrew;
use core\entities\TaskFiles;
use core\entities\TaskRelations;
use core\forms\backend\compositeForm\TaskCreateForm;
use core\forms\backend\compositeForm\TaskUpdateForm;
use core\helpers\CommonHelper;
use core\repositories\UserRepository;
use core\repositories\TaskRepository;
use core\entities\Task;
use core\services\common\ManageFilesService;

class TaskService
{
    private $tasks;
    private $fileService;
    private $userRepository;
    private $path;

    public function __construct(TaskRepository $tasks, ManageFilesService $fileService, UserRepository $userRepository, $path)
    {
        $this->tasks = $tasks;
        $this->fileService = $fileService;
        $this->path = $path;
        $this->userRepository = $userRepository;
    }

    public function create(TaskCreateForm $form, $userId): Task
    {
        $this->checkUser($userId, Role::CREATE);
        $task = Task::create($form->title, $form->desc, $form->user_id, $form->project_id, $form->priority, $form->type_id, $form->stime, $form->etime, $form->remember_time, $form->checklist, Task::ACTIVE);

        if(is_array($form->list)){
            $list = [];
            foreach ($form->list as $item){
                $list[] = ['text' => $item->text, 'status' => $item->status];
            }
            $task->checklist = $list;
        }

        $this->tasks->save($task);

        if (!empty($form->uploadedFile)) {
            foreach ($form->uploadedFile as $file) {
                $fileNames = $this->fileService->upload($file, $this->path . '/web/uploads/tasks/' . $task->id . '/');
                if($fileNames){
                    $this->tasks->saveFiles(TaskFiles::create($task->id, $fileNames));
                }
            }
        }

        if($task->id){

            if(is_array($form->crew)){
                foreach ($form->crew as $crew){
                    if(!empty($crew->user_id) && !empty($crew->role)) {
                        $this->tasks->saveCrew(TaskCrew::create($task->id, $crew->user_id, $crew->role));
                    }
                }
            }

            if(is_array($form->relations)){
                foreach ($form->relations as $relation){
                    if(!empty($relation->related_task_id) && !empty($relation->relation_type)) {
                        $this->tasks->saveRelations(TaskRelations::create($task->id, $relation->related_task_id, $relation->relation_type));
                    }
                }
            }

            if(is_array($form->files)){
                foreach ($form->files as $file){
                    if(!empty($file->related_task_id)) {
                        $this->tasks->saveFiles(TaskFiles::create($file, $task->id));
                    }
                }
            }

            if(isset($form->tags['name']) && is_array($form->tags['name'])){
                $exists = array_column(Tag::find()->where(['in', 'id', array_filter($form->tags['name'], 'is_numeric')])->all(), null, 'id');
                foreach ($form->tags['name'] as $name){
                    if(!isset($exists[$name])){
                        $tag = $this->tasks->saveTag(Tag::create($name));
                        $this->tasks->saveTagBind(TagsBind::create($tag->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                    else{
                        $this->tasks->saveTagBind(TagsBind::create($exists[$name]->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                }
            }

        }
        return $task;
    }

    public function edit(TaskUpdateForm $form, $task_id, $userId): Task
    {
        $task = $this->tasks->getById($task_id);
        $this->checkUser($userId, Role::UPDATE, $task);
        $task->edit($form->title, $form->desc, $form->user_id, $form->project_id, $form->priority, $form->type_id, $form->stime, $form->etime, $form->remember_time, $form->checklist, $task->status);

        if(is_array($form->list)){
            $list = [];
            foreach ($form->list as $item){
                $list[] = ['text' => $item->text, 'status' => $item->status];
            }
            $task->checklist = $list;
        }

        $this->tasks->save($task);

        if (!empty($form->uploadedFile)) {
            $this->fileService->deleteDirectory($this->path . '/web/uploads/projects/' . $task->id . '/');
            foreach ($form->uploadedFile as $file) {
                $fileNames = $this->fileService->upload($file, $this->path . '/web/uploads/tasks/' . $task->id . '/');
                if($fileNames){
                    $this->tasks->saveFiles(TaskFiles::create($task->id, $fileNames));
                }
            }
        }

        if($task->id){

            if(is_array($form->crew)){
                foreach ($task->crew as $user){
                    $user->delete();
                }
                foreach ($form->crew as $crew){
                    if(!empty($crew->user_id) && !empty($crew->role)){
                        $this->tasks->saveCrew(TaskCrew::create($task->id, $crew->user_id, $crew->role));
                    }
                }
            }

            if(is_array($form->relations)){
                foreach ($task->relations as $relation){
                    $relation->delete();
                }
                foreach ($form->relations as $relation){
                    if(!empty($relation->related_task_id) && !empty($relation->relation_type)) {
                        $this->tasks->saveRelations(TaskRelations::create($task->id, $relation->related_task_id, $relation->relation_type));
                    }
                }
            }

            if(is_array($form->files)){
                foreach ($form->files as $file){
                    if(!empty($file->related_task_id)) {
                        $this->tasks->saveFiles(TaskFiles::create($file, $task->id));
                    }
                }
            }

            if(isset($form->tags['name']) && is_array($form->tags['name'])){
                $exists = array_column(Tag::find()
                    ->where(['in', 'id', array_filter($form->tags['name'], 'is_numeric')])
                    ->orWhere(['in', 'name', $form->tags['name']])
                    ->all(), null, 'id');
                $exists_names = array_column($exists, 'id', 'name');
                foreach ($task->tagsBind as $bind){
                    $bind->delete();
                }
                foreach ($form->tags['name'] as $name){
                    if(!empty($exists_names[$name])){
                        $name = $exists_names[$name];
                    }
                    if(empty($exists[$name]->name)){
                        $tag = $this->tasks->saveTag(Tag::create($name));
                        $this->tasks->saveTagBind(TagsBind::create($tag->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                    else{

                        $this->tasks->saveTagBind(TagsBind::create($exists[$name]->id, $task->id, CommonHelper::mb_basename(get_class($task))));
                    }
                }
            }
        }
        return $task;
    }

    public function changeStatus($id, $status, $userId): Void
    {
        $this->checkUser($userId, Role::UPDATE);
        $task = $this->tasks->getById($id);
        if(!$task->isStatus($status)){
            $task->status = $status;
            $this->tasks->save($task);
        }
        else{
            throw new \DomainException('Status was not changed!');
        }
    }

    public function view($id, $userId): Task
    {
        $task = $this->tasks->getById($id);
        $this->checkUser($userId, Role::READ, $task);
        return $task;
    }

    public function removeImage($id, $userId): Void
    {
        $this->checkUser($userId, Role::DELETE);
        $taskFile = $this->tasks->getFileById($id);
        if ($taskFile){
            $this->fileService->deleteDirectory($this->path.'/web/uploads/tasks/'.$taskFile->task->id);
            $taskFile->file = null;
        }
        $this->tasks->deleteFiles($taskFile);
    }

    public function delete($id, $userId): Void
    {
        $task = $this->tasks->getById($id);
        $this->checkUser($userId, Role::DELETE, $task);
        if($task->isStatus(Task::DELETED)){
            throw new \DomainException('The task already deleted!');
        }
        $task->setStatusDeleted();
        $this->tasks->save($task);
    }

    protected function checkUser($userId, $permission, Task $task = null): Void
    {
        $user = $this->userRepository->get($userId);
        if(empty($user->role)){
            throw new \DomainException('User role not exists!');
        }
        try{
            $user->role->checkPermission('task', $permission);
        }
        catch (\DomainException $e){
            $this->checkTask($task, $permission, $userId);
        }
    }

    protected function checkTask(Task $task, $permission, $userId): Void
    {
        if(!$task){
            throw new \DomainException('You have not access!');
        }
        $task_role = $task->getRole($userId);
        $task_role->checkPermission('task', $permission);
    }

}