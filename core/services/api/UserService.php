<?php

namespace core\services\api;

use core\entities\Role;
use core\entities\User;
use core\entities\UserRole;
use core\forms\common\UserForm;
use core\repositories\RoleRepository;
use core\repositories\UserRepository;

/**
 * Class UserService
 * @package core\services\api
 */
class UserService
{
    public $userRepository;
    public $role;

    public function __construct(UserRepository $userRepository, RoleRepository $role)
    {
        $this->userRepository = $userRepository;
        $this->role = $role;
    }

    public function create($authId, UserForm $form): User
    {
        $userAuth = $this->userRepository->get($authId);
        $role = $userAuth->role;
        $role->checkPermission('user', Role::CREATE);
        $user = User::create($form->email, $form->password, $form->timeZone);
        $this->userRepository->save($user);
        return $user;
    }

    public function edit($id, $authId, UserForm $form): User
    {
        $userAuth = $this->userRepository->get($authId);
        $role = $userAuth->role;
        $role->checkPermission('user', Role::UPDATE);
        $user = $this->userRepository->get($id);
        $user->edit($form->email, $form->password, $form->timeZone);
        $this->userRepository->save($user);
        return $user;
    }

    public function view($id, $authId): User
    {
        $userAuth = $this->userRepository->get($authId);
        $role = $userAuth->role;
        $role->checkPermission('user', Role::READ);
        $user = $this->userRepository->get($id);
        return $user;
    }

    public function delete($id, $authId)
    {
        $userAuth = $this->userRepository->get($authId);
        $role = $userAuth->role;
        $role->checkPermission('user', Role::DELETE);
        $user = $this->userRepository->get($id);
        $user->isDelete();
        $user->remove();
        $this->userRepository->save($user);
    }

    public function getRole($authId)
    {
        $userAuth = $this->userRepository->get($authId);
        $role = $userAuth->role;
        return $role;
    }

}