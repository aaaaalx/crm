<?php

namespace core\services\api;


use core\entities\redis\User as RUser;;
use core\forms\api\LoginForm;
use core\helpers\DumpHelper;
use core\interfaces\UserStorageRepository;
use core\repositories\UserRepository;

class AuthService
{
    public $userRepository;
    public $userRedisRepository;

    public function __construct(UserRepository $userRepository, UserStorageRepository $userRedisRepository)
    {
        $this->userRepository = $userRepository;
        $this->userRedisRepository = $userRedisRepository;
    }

    public function auth(LoginForm $form)
    {
        $user = $this->userRedisRepository->getByEmail($form->email);
        if(!$user){
            $user = $this->userRepository->getByEmail($form->email);
            $user = RUser::create($user->id, $user->access_token, $user->email, $user->password_hash, $user->time_zone, $user->status, $user->ctime);
            $this->userRedisRepository->save($user);
        }

        if(!$user->validatePassword($form->password)){
            throw new \DomainException('Undefined user or password.');
        }
        return $user;
    }


}