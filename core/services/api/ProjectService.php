<?php

namespace core\services\api;

use core\entities\ProjectCrew;
use core\entities\ProjectOptions;
use core\entities\Role;
use core\entities\Tag;
use core\entities\TagsBind;
use core\forms\backend\compositeForm\ProjectCreateForm;
use core\forms\backend\compositeForm\ProjectUpdateForm;
use core\helpers\CommonHelper;
use core\repositories\ProjectRepository;
use core\entities\Project;
use core\repositories\UserRepository;
use core\services\common\ManageFilesService;

class ProjectService
{
    private $projects;
    private $fileService;
    private $userRepository;
    private $path;

    public function __construct(ProjectRepository $projects, ManageFilesService $fileService, UserRepository $userRepository, $path)
    {
        $this->projects = $projects;
        $this->fileService = $fileService;
        $this->userRepository = $userRepository;
        $this->path = $path;
    }

    public function create(ProjectCreateForm $form, $userId): Project
    {
        $this->checkUser($userId, Role::CREATE);
        $project = Project::create($form->title, $form->description, $form->access_type, $form->stime, $form->etime, Project::ACTIVE);
        $this->projects->save($project);

        if ($form->image) {
            $fileName = $this->fileService->upload($form->image, $this->path . '/web/uploads/projects/' . $project->id . '/');
            $project->image = $fileName;
            $this->projects->save($project);
        }

        if($project->id){

            if(is_array($form->crew)){
                foreach ($form->crew as $crew){
                    $this->projects->saveCrew(ProjectCrew::create($project->id, $crew->user_id, $crew->role_id));
                }
            }
            if(is_array($form->options)){
                foreach ($form->options as $option){
                    if(!empty($option->option_id)) {
                        $this->projects->saveOptions(ProjectOptions::create($project->id, $option->option_id, $option->value));
                    }
                }
            }

            if(isset($form->tags['name']) && is_array($form->tags['name'])){
                $exists = array_column(Tag::find()->where(['in', 'id', array_filter($form->tags['name'], 'is_numeric')])->all(), null, 'id');
                foreach ($form->tags['name'] as $name){
                    if(!isset($exists[$name])){
                        $tag = $this->projects->saveTag(Tag::create($name));
                        $this->projects->saveTagBind(TagsBind::create($tag->id, $project->id, basename(get_class($project))));
                    }
                    else{
                        $this->projects->saveTagBind(TagsBind::create($exists[$name]->id, $project->id, basename(get_class($project))));
                    }
                }
            }
        }

        return $project;

    }

    public function edit(ProjectUpdateForm $form, $project_id, $userId): Project
    {
        $project = $this->projects->getById($project_id);
        $this->checkUser($userId, Role::UPDATE, $project);
        $project->edit($form->title, $form->description, $form->access_type, $form->stime, $form->etime, $project->status);
        $this->projects->save($project);

        if ($form->image) {
            if ($project->image) {
                $this->fileService->deleteFile($this->path . '/web/uploads/projects/' . $project->id . '/' . $project->image);
            }
            $fileName = $this->fileService->upload($form->image, $this->path . '/web/uploads/projects/' . $project->id . '/');
            $project->image = $fileName;
            $this->projects->save($project);
        }

        if($project->id){

            if(is_array($form->crew)){
                foreach ($project->crew as $user){
                    $user->delete();
                }
                foreach ($form->crew as $crew){
                    $this->projects->saveCrew(ProjectCrew::create($project->id, $crew->user_id, $crew->role_id));
                }
            }

            if(is_array($form->options)){
                foreach ($project->options as $option){
                    $option->delete();
                }
                foreach ($form->options as $option){
                    if(!empty($option->option_id)) {
                        $this->projects->saveOptions(ProjectOptions::create($project->id, $option->option_id, $option->value));
                    }
                }
            }

            if(isset($form->tags['name']) && is_array($form->tags['name'])){
                $exists = array_column(Tag::find()
                    ->where(['in', 'id', array_filter($form->tags['name'], 'is_numeric')])
                    ->orWhere(['in', 'name', $form->tags['name']])
                    ->all(), null, 'id');
                $exists_names = array_column($exists, 'id', 'name');
                foreach ($project->tagsBind as $bind){
                    $bind->delete();
                }
                foreach ($form->tags['name'] as $name){
                    if(!empty($exists_names[$name])){
                        $name = $exists_names[$name];
                    }
                    if(empty($exists[$name]->name)){
                        $tag = $this->projects->saveTag(Tag::create($name));
                        $this->projects->saveTagBind(TagsBind::create($tag->id, $project->id, CommonHelper::mb_basename(get_class($project))));
                    }
                    else{

                        $this->projects->saveTagBind(TagsBind::create($exists[$name]->id, $project->id, CommonHelper::mb_basename(get_class($project))));
                    }
                }
            }
        }
        return $project;

    }

    public function changeStatus($id, $status, $userId): Void
    {
        $project = $this->projects->getById($id);
        $this->checkUser($userId, Role::UPDATE, $project);
        if(!$project->isStatus($status)){
            $project->status = $status;
            $this->projects->save($project);
        }
        else{
            throw new \DomainException('Status was not changed!');
        }
    }

    public function view($id, $userId): Project
    {
        $project = $this->projects->getById($id);
        $this->checkUser($userId, Role::READ, $project);
        return $project;
    }

    public function removeImage($id, $userId): Void
    {
        $project = $this->projects->getById($id);
        $this->checkUser($userId, Role::DELETE, $project);
        if ($project->image){
            $this->fileService->deleteDirectory($this->path.'/web/uploads/projects/'.$project->id);
            $project->image = null;
        }
        $this->projects->save($project);
    }

    public function delete($id, $userId): Void
    {
        $project = $this->projects->getById($id);
        $this->checkUser($userId, Role::DELETE, $project);
        if($project->isStatus(Project::DELETED)){
            throw new \DomainException('The project already deleted!');
        }
        $project->setStatusDeleted();
        $this->projects->save($project);
    }

    protected function checkUser($userId, $permission, Project $project = null): Void
    {
        $user = $this->userRepository->get($userId);
        if(empty($user->role)){
            throw new \DomainException('User role not exists!');
        }
        try{
            $user->role->checkPermission('project', $permission);
        }
        catch (\DomainException $e){
            $this->checkProject($project, $permission, $userId);
        }
    }

    protected function checkProject(Project $project, $permission, $userId): Void
    {
        if(!$project){
            throw new \DomainException('You have not access!');
        }
        $project_role = $project->getRole($userId);
        $project_role->checkPermission('project', $permission);
    }

}