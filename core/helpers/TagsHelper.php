<?php

namespace core\helpers;

use Yii;
use core\entities\Tag;
use yii\helpers\ArrayHelper;

class TagsHelper
{
    public static function getStatuses(){
        return [
            Tag::ACTIVE => Yii::t('app', 'Active'),
            Tag::INACTIVE => Yii::t('app', 'Inactive'),
            Tag::DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    public static function getStatus($status){
        if(is_int($status+0)){
            return ArrayHelper::getValue(self::getStatuses(), $status);
        }
        return ArrayHelper::getValue(array_flip(self::getStatuses()), $status);
    }
}