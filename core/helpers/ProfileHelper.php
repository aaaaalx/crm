<?php

namespace core\helpers;


use core\entities\Language;
use core\entities\Position;
use core\entities\Profile;
use core\entities\User;
use yii\helpers\ArrayHelper;

class ProfileHelper
{
    public static function genderList()
    {
        return [
            Profile::GENDER_MALE => 'Male',
            Profile::GENDER_FEMALE => 'Female',
        ];
    }

    public static function getGender($gender)
    {
        $genders = self::genderList();
        return $genders[$gender] ?? null;
    }

    public static function positionList()
    {
        $position = Position::find()->where(['status' => Position::STATUS_ACTIVE])->all();
        $list = ArrayHelper::map($position, 'id', 'title');
        return $list;
    }

    public static function getPosition($position)
    {
        return ArrayHelper::getValue(self::positionList(), $position);
    }

    public static function lngList()
    {
        return ArrayHelper::map(Language::find()->all(), 'id', 'title');
    }

    public static function getLng($lngId)
    {
        return ArrayHelper::getValue(self::lngList(), $lngId);
    }

    public static function statusList()
    {
        return [
            Profile::STATUS_ACTIVE => 'new',
            Profile::STATUS_DELETED => 'deleted',
            Profile::STATUS_BLOCKED => 'blocked',
        ];
    }

    public static function getStatus($status)
    {
        $statuses = self::statusList();
        return $statuses[$status] ?? null;
    }

    public static function getEmail($userId)
    {
        $user = User::findOne($userId);
        return [$user->id => $user->email];
    }



}