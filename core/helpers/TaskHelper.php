<?php

namespace core\helpers;

use core\entities\TaskCrew;
use core\entities\TaskRelations;
use Yii;
use core\entities\Task;
use yii\helpers\ArrayHelper;

class TaskHelper
{
    public static function getStatuses(){
        return [
            Task::ACTIVE => Yii::t('app', 'Active'),
            Task::INACTIVE => Yii::t('app', 'Inactive'),
            Task::DELETED => Yii::t('app', 'Deleted')
        ];
    }

    public static function getStatus($status){
        if(is_int($status+0)){
            return ArrayHelper::getValue(self::getStatuses(), $status);
        }
        return ArrayHelper::getValue(array_flip(self::getStatuses()), $status);
    }

    public static function getRoles(){
        return [
            TaskCrew::RESPONSIBLE => Yii::t('app', 'Responsible'),
            TaskCrew::SUBCONTRACTORS => Yii::t('app', 'Subcontractors'),
            TaskCrew::OBSERVERS => Yii::t('app', 'Observers'),
            TaskCrew::DIRECTOR => Yii::t('app', 'Director'),
            TaskCrew::REVIEWER => Yii::t('app', 'Reviewer')
        ];
    }
    public static function getRole($role){
        if(is_int($role+0)){
            return ArrayHelper::getValue(self::getRoles(), $role);
        }
        return ArrayHelper::getValue(array_flip(self::getRoles()), $role);
    }

    public static function getRelationsTypes(){
        return [
            TaskRelations::START_FINISH => Yii::t('app', 'Start to Finish'),
            TaskRelations::FINISH_START => Yii::t('app', 'Finish to Start'),
            TaskRelations::START_START => Yii::t('app', 'Start to Start'),
            TaskRelations::FINISH_FINISH => Yii::t('app', 'Finish to Finish')
        ];
    }
}