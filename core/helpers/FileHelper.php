<?php

namespace core\helpers;

use core\interfaces\FileInterface;
use yii\helpers\FileHelper as File;

class FileHelper implements FileInterface
{
    private $fileName;
    private $file;
    private $extension;
    private $mimeType;

    public function __construct($file)
    {
        $this->file = $file;
        $this->mimeType = $file->type;
        $this->fileName = $file->name;
        $this->extension = $file->extension;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getExtension()
    {
        return $this->extension;
    }

    private function generateFileName()
    {
        return \Yii::$app->getSecurity()->generateRandomString(10) . '.' . $this->getExtension();
    }

    public function __toString()
    {
        return $this->getFileName();
    }

    public function saveAs($path)
    {
        File::createDirectory($path, $mode = 0775, $recursive = true);
        return file_put_contents($path.$this->getFileName(),  file_get_contents($this->file->tempName));
    }

    public static function removeFile($path)
    {
        File::unlink($path);
        //array_map('unlink', glob($path."*"));
    }

    public static function removeDirectory($path)
    {
        File::removeDirectory($path);
    }

}