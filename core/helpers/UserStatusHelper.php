<?php


namespace core\helpers;


use core\entities\User;
use Yii;

class UserStatusHelper
{
    public static function getStatuses()
    {
        return [
            User::STATUS_MODERATE => Yii::t('models.statuses','Moderate'),
            User::STATUS_ACTIVE => Yii::t('models.statuses','Active'),
            User::STATUS_DELETED => Yii::t('models.statuses','Deleted')
        ];
    }

    public static function getStatusLabel($status)
    {
        $statuses = self::getStatuses();
        return $statuses[$status] ?? null;
    }
}