<?php

namespace core\helpers;

use core\entities\Admin;
use Yii;

class AdminStatusHelper
{
    public static function getStatuses()
    {
        return [
            Admin::STATUS_MODERATE => Yii::t('models.statuses', 'Moderate'),
            Admin::STATUS_ACTIVE => Yii::t('models.statuses','Active'),
            Admin::STATUS_DELETED => Yii::t('models.statuses','Deleted')
        ];
    }

    public static function getStatusLabel($status)
    {
        $statuses = self::getStatuses();
        return $statuses[$status] ?? null;
    }
}