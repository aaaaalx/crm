<?php

namespace core\helpers;


use core\entities\Position;

class PositionHelper
{
    public static function statusList()
    {
        return [
            Position::STATUS_ACTIVE => 'active',
            Position::STATUS_NO_ACTIVE => 'no active',
            Position::STATUS_DELETED => 'deleted'
        ];
    }

    public static function getStatus($status)
    {
        $statuses = self::statusList();
        return $statuses[$status] ?? null;
    }

}