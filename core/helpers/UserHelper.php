<?php

namespace core\helpers;


use core\entities\Role;
use core\entities\UserRole;
use yii\helpers\ArrayHelper;

class UserHelper
{
    public static function roleList()
    {
        return ArrayHelper::map(Role::find()->all(), 'id', 'name');
    }

    public static function getRole($role)
    {
        return ArrayHelper::getValue(self::roleList(), $role);
    }

    public static function getRoleId($userId)
    {
        $role = UserRole::find()->andWhere(['user_id' => $userId])->one();
        return $role->role_id??null;

    }

}