<?php


namespace core\helpers;


class CommonHelper
{
    public static function getTimeZones()
    {
        return array_combine(range(-12, 14), range(-12, 14));
    }

    public static function mb_basename($file)
    {
        $res = explode('\\',$file);
        return end($res);
    }

}