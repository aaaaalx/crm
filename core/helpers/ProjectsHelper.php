<?php

namespace core\helpers;

use Yii;
use core\entities\Project;
use yii\helpers\ArrayHelper;

class ProjectsHelper
{
    public static function getStatuses(){
        return [
            Project::ACTIVE => Yii::t('app', 'Active'),
            Project::INACTIVE => Yii::t('app', 'Inactive'),
            Project::DELETED => Yii::t('app', 'Deleted')
        ];
    }

    public static function getStatus($status){
        if(is_int($status+0)){
            return ArrayHelper::getValue(self::getStatuses(), $status);
        }
        return ArrayHelper::getValue(array_flip(self::getStatuses()), $status);
    }

    public static function getAccesses(){
        return [
            Project::ACCESS_OPEN => Yii::t('app', 'Open'),
            Project::ACCESS_CLOSED => Yii::t('app', 'Close')
        ];
    }

    public static function getAccess($access){
        if(is_int($access+0)){
            return ArrayHelper::getValue(self::getAccesses(), $access);
        }
        return ArrayHelper::getValue(array_flip(self::getAccesses()), $access);
    }

}