<?php


namespace core\helpers;

use Yii;
use yii\helpers\ArrayHelper;

class RbacHelper
{

    public static function getRolesDropdown()
    {
        return ArrayHelper::map(self::getRoles(), 'name', 'description');
    }

    public static function getRoles()
    {
        return Yii::$app->authManager->getRoles();
    }

    public static function getRolesByUser($userId)
    {
        return Yii::$app->authManager->getRolesByUser($userId);
    }

    public static function getRolesDropdownByUser($userId)
    {
        return ArrayHelper::getColumn(self::getRolesByUser($userId), 'name');
    }
}