<?php

namespace core\helpers;

use yii\helpers\FileHelper;
use core\interfaces\FileInterface;

class Base64File implements FileInterface
{
    private $fileString;
    private $fileName;
    private $file;
    private $extension;
    private $mimeType;

    private $mimeToExtension = [
        'text/plain' => 'txt'
    ];

    public function __construct($base64string)
    {
        $this->fileString = $base64string;
        $this->setFile()->setMimeType()->setExtension();
        $this->fileName = $this->generateFileName();
    }

    public function setFile(): self
    {
        $data = explode(',', $this->fileString);
        $imageData = end($data);
        $imageData = base64_decode($imageData);
        $this->file = $imageData;
        return $this;
    }

    private function setExtension(): self
    {
        if (!$this->getMimeType()) {
            $this->setMimeType();
        }

        if ($ext = $this->checkExtensionByMime()) {
            $this->extension = $ext;
            return $this;
        }

        $mimeParts = explode('/', $this->mimeType);

        $this->extension = end($mimeParts);

        return $this;
    }

    private function setMimeType()
    {
        $f = finfo_open();

        $this->mimeType = finfo_buffer($f, $this->file, FILEINFO_MIME_TYPE);

        finfo_close($f);

        return $this;
    }

    private function generateFileName()
    {
        return \Yii::$app->getSecurity()->generateRandomString(10) . '.' . $this->getExtension();
    }

    private function checkExtensionByMime()
    {
        return $this->mimeToExtension[$this->mimeType] ?? null;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getExtension()
    {
        return $this->extension;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function saveAs($path, $name = ''){
        FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        $path .= $this->fileName??$name;
        return file_put_contents($path.$name, $this->file);
    }
}