<?php

namespace core\helpers;

use Yii;
use core\entities\TaskType;
use yii\helpers\ArrayHelper;

class TaskTypeHelper
{
    public static function getStatuses():array
    {
        return [
            TaskType::ACTIVE => Yii::t('app', 'Active'),
            TaskType::INACTIVE => Yii::t('app', 'Inactive'),
            TaskType::DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    public static function getStatus($status)
    {
        if(is_int($status+0)){
            return ArrayHelper::getValue(self::getStatuses(), $status);
        }
        return ArrayHelper::getValue(array_flip(self::getStatuses()), $status);
    }

    public static function getTypes(): array
    {
        $taskType = TaskType::find()->where(['status' => TaskType::ACTIVE])->asArray()->all();
        if(!empty($taskType)){
            return array_column($taskType, 'name', 'id');
        }
        return [];
    }
}