<?php

namespace core\helpers;

use core\entities\Language;
use Yii;

class LanguagesHelper
{
    public static function getList()
    {
        return \Yii::$app->i18n->languagesList;
    }

    public static function getLanguages()
    {
        return \Yii::$app->i18n->languages;
    }

    public static function getStatuses()
    {
        return [
            Language::STATUS_ACTIVE => Yii::t('models.statuses','Active'),
            Language::STATUS_DELETED => Yii::t('models.statuses','Deleted')
        ];
    }

    public static function getStatusLabel($status)
    {
        $statuses = self::getStatuses();
        return $statuses[$status] ?? null;
    }
}