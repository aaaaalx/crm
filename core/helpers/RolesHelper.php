<?php

namespace core\helpers;

use core\entities\Role;
use yii\helpers\ArrayHelper;

class RolesHelper
{
    public static function getRules(): array
    {
        return [
            Role::CREATE,
            Role::READ,
            Role::UPDATE,
            Role::DELETE
        ];
    }

    public static function getRulesNames(): array
    {
        return [
            Role::CREATE => 'Create',
            Role::READ => 'Read',
            Role::UPDATE => 'Update',
            Role::DELETE => 'Delete'
        ];
    }

    public static function getRulesConst(): array
    {
        return [
            'create' => Role::CREATE,
            'read' => Role::READ,
            'update' => Role::UPDATE,
            'delete' => Role::DELETE
        ];
    }

    public static function getRuleName($rule): string
    {
        return ArrayHelper::getValue(self::getRulesNames(), $rule);
    }

    public static function getTasksRulesHtml(Role $role): string
    {
        $html = '';
        foreach (self::getRules() as $rule) {
            $html .= ($role->checkTaskPermission($rule) ? '<i class="fa fa-check-square"></i> ' : '<i class="fa fa-square"></i> ') . self::getRuleName($rule) . '<br/>';
        }
        return $html;
    }

    public static function getProjectsRulesHtml(Role $role): string
    {
        $html = '';
        foreach (self::getRules() as $rule) {
            $html .= ($role->checkProjectPermission($rule) ? '<i class="fa fa-check-square"></i> ' : '<i class="fa fa-square"></i> ') . self::getRuleName($rule) . '<br/>';
        }
        return $html;
    }

    public static function getProjectMembersRulesHtml(Role $role): string
    {
        $html = '';
        foreach (self::getRules() as $rule) {
            $html .= ($role->checkProjectMemberPermission($rule) ? '<i class="fa fa-check-square"></i> ' : '<i class="fa fa-square"></i> ') . self::getRuleName($rule) . '<br/>';
        }
        return $html;
    }

    public static function getTaskMembersRulesHtml(Role $role): string
    {
        $html = '';
        foreach (self::getRules() as $rule) {
            $html .= ($role->checkTaskMemberPermission($rule) ? '<i class="fa fa-check-square"></i> ' : '<i class="fa fa-square"></i> ') . self::getRuleName($rule) . '<br/>';
        }
        return $html;
    }

    public static function getUsersRulesHtml(Role $role): string
    {
        $html = '';
        foreach (self::getRules() as $rule) {
            $html .= ($role->checkUserPermission($rule) ? '<i class="fa fa-check-square"></i> ' : '<i class="fa fa-square"></i> ') . self::getRuleName($rule) . '<br/>';
        }
        return $html;
    }

    public static function getTasksRulesArray(Role $role): array
    {
        $result = [];
        foreach (self::getRules() as $rule) {
            $role->checkTaskPermission($rule) ? $result[] = $rule : null;
        }
        return $result;
    }

    public static function getProjectsRulesArray(Role $role): array
    {
        $result = [];
        foreach (self::getRules() as $rule) {
            $role->checkProjectPermission($rule) ? $result[] = $rule : null;
        }
        return $result;
    }

    public static function getProjectMembersRulesArray(Role $role): array
    {
        $result = [];
        foreach (self::getRules() as $rule) {
            $role->checkProjectMemberPermission($rule) ? $result[] = $rule : null;
        }
        return $result;
    }

    public static function getTaskMembersRulesArray(Role $role): array
    {
        $result = [];
        foreach (self::getRules() as $rule) {
            $role->checkTaskMemberPermission($rule) ? $result[] = $rule : null;
        }
        return $result;
    }

    public static function getUsersRulesArray(Role $role): array
    {
        $result = [];
        foreach (self::getRules() as $rule) {
            $role->checkUserPermission($rule) ? $result[] = $rule : null;
        }
        return $result;
    }
}