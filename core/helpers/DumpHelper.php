<?php

namespace core\helpers;

use yii\helpers\BaseVarDumper;

class DumpHelper
{
    public static function dump($var, $end = true)
    {
        BaseVarDumper::dump($var, 10, true);
        if ($end) {
            die;
        }
    }
}