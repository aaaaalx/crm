<?php

namespace core\helpers;


use core\entities\Options;
use Yii;
use yii\helpers\Html;

class OptionsHelper
{
    public static function typeList()
    {
        return [
            Options::TYPE_INT => Yii::t('app', 'int'),
            Options::TYPE_STRING => Yii::t('app', 'string'),
            Options::TYPE_LIST => Yii::t('app', 'list'),
            Options::TYPE_BOOL => Yii::t('app', 'bool'),
            Options::TYPE_MULTIPLE_LIST => Yii::t('app', 'multiple list'),
            Options::TYPE_DATE => Yii::t('app', 'date'),
        ];
    }

    public static function getType($type)
    {
        $types = self::typeList();
        return $types[$type] ?? null;
    }

    public static function getOutputByType(Options $option, $field_name, $field_id = null, $value = null, $params = [])
    {
        $json = $option->variants ?? [];
        $field_id ? $params['id'] = $field_id : false;
        $params['class'] = isset($params['class'])?$params['class'].' form-control':$params['class'];
        switch ($option->type) {
            case Options::TYPE_INT:
                return Html::input('number', $field_name, $value, $params);
            case Options::TYPE_STRING:
                return Html::input('text', $field_name, $value, $params);
            case Options::TYPE_DATE:
                return Html::input('date', $field_name, $value, $params);
            case Options::TYPE_LIST:
                return Html::dropDownList($field_name, $value, $json, $params);
            case Options::TYPE_MULTIPLE_LIST:
                $options['multiple'] = true;
                return Html::dropDownList($field_name, $value, $json, $params);
            case Options::TYPE_BOOL:
                $params['class'] = '';
                return Html::checkbox($field_name, $value?true:false, $params);
        }
        return null;
    }


}