<?php

namespace core\helpers;

use yii\web\UploadedFile as File;

class UploadedFile
{
    public static function uploaded($model, $attribute)
    {
        $file = File::getInstance($model, $attribute);
        if($file){
           return new FileHelper($file);
        }
        return null;
    }

    public static function uploads($model, $attribute)
    {
        $files = File::getInstances($model, $attribute);
        $res = [];
        if(count($files)){
            foreach ($files as $file){
                $res[] = new FileHelper($file);
            }
            return $res;
        }
        return null;
    }

    public static function uploadBase64($base64string)
    {
        return new Base64File($base64string);
    }

    public static function uploadsBase64($base64strings)
    {
        $res = [];
        if(count($base64strings)){
            foreach ($base64strings as $base64string){
                $res[] = new Base64File($base64string);
            }
            return $res;
        }
        return null;
    }

}