<?php

namespace core\traits;

trait ConsoleValidationErrorLog
{
    public function consoleLogErrors(): string
    {
        $result = 'Errors: ' . PHP_EOL;
        if ($this->hasErrors()) {
            foreach ($this->getErrors() as $attribute => $errors) {
                $result .= '- ' . $attribute . ': ' . PHP_EOL;
                foreach ($errors as $error) {
                    $result .= '--- ' . $error . PHP_EOL;
                }
            }
        }
        return $result;
    }
}