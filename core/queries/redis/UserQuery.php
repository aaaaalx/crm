<?php


namespace core\queries\redis;


use core\entities\User;
use yii\redis\ActiveQuery;

class UserQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status' => User::STATUS_ACTIVE]);
    }

    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    public function byToken($token)
    {
        return $this->andWhere(['access_token' => $token]);
    }

    public function byEmail($email)
    {
        return $this->andWhere(['email' => $email]);
    }
}