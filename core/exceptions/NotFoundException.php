<?php

namespace core\exceptions;

use Throwable;

class NotFoundException extends \DomainException
{
    public function __construct($message = "Can not find object", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}