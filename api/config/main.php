<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => [
        'log',
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => 'json',
                'application/xml' => 'xml',
            ],
        ],
        'common\config\BootUp'
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if (!$response->isSuccessful) {
                    $message = isset($response->data['message']) ? [$response->data['message']] : null;
                    $data = $response->data;
                    if ($data['status'] === false) {
                        $message = $data['errors'];
                    }
                    $response->data = [
                        'error' => [
                            'code' => $response->getStatusCode(),
                            'message' => $message
                        ]
                    ];
                    $response->statusCode = 200;
                } else {
                    if (isset($response->data['index'])) {
                        $response->data = [
                            'result' => $response->data['index'],
                            '_links' => $response->data['_links'],
                            '_meta' => $response->data['_meta']
                        ];
                    } else {
                        $response->data = [
                            'result' => $response->data
                        ];
                    }
                }
            },
        ],
        'user' => [
            'identityClass' => 'common\auth\Identity',
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',

                'GET projects' => 'project/index',
                'GET project/<id:\d+>' => 'project/view',
                'POST project' => 'project/create',
                'PUT,PATCH project/<id:\d+>' => 'project/update',
                'DELETE project/<id:\d+>' => 'project/delete',
                'DELETE project/image/<id:\d+>' => 'project/remove-image',
                'PUT,PATCH project/active/<id:\d+>' => 'project/status-active',
                'PUT,PATCH project/inactive/<id:\d+>' => 'project/status-inactive',
                'PUT,PATCH project/delete/<id:\d+>' => 'project/status-delete',
                'GET projects/options' => 'project-options/options',
                'GET projects/list/<q:\w+>' => 'project-options/projects',

                'GET tasks' => 'task/index',
                'GET task/<id:\d+>' => 'task/view',
                'POST task' => 'task/create',
                'PUT,PATCH task/<id:\d+>' => 'task/update',
                'DELETE task/<id:\d+>' => 'task/delete',
                'DELETE task/image/<id:\d+>' => 'task/remove-image',
                'PUT,PATCH task/active/<id:\d+>' => 'task/status-active',
                'PUT,PATCH task/inactive/<id:\d+>' => 'task/status-inactive',
                'PUT,PATCH task/delete/<id:\d+>' => 'task/status-delete',
                'GET task/relations/<q:\w+>' => 'task-options/relations',

                'POST login' => 'auth/login',
                'POST signup' => 'auth/signup',
                'GET version' => 'version/version',
                'POST request-password-reset' => 'password-reset/request',
                'POST password-reset' => 'password-reset/reset',
                'POST user' => 'user/create',
                'PUT,PATCH user/<id:\d+>' => 'user/update',
                'GET user/<id:\d+>' => 'user/view',
                'DELETE user/<id:\d+>' => 'user/delete',
                'GET user/my-role' => 'user/my-role',
                'OPTIONS <controller>' => 'api/options',
                'OPTIONS <action>/<id:\d+>' => 'api/options',
                'OPTIONS <controller>/<action>/<id:\d+>' => 'api/options',
                'OPTIONS <controller>/<id:\d+>/<action>' => 'api/options'
            ],
        ]
    ],
    'as authenticator' => [
        'class' => 'yii\filters\auth\CompositeAuth',
        'authMethods' => [
            ['class' => 'yii\filters\auth\HttpBearerAuth'],
        ],
        'except' => [
            'api/options',
            'auth/login',
            'auth/signup',
            'version/version',
            'password-reset/request',
            'password-reset/reset'
        ]
    ],
    'params' => $params,
];
