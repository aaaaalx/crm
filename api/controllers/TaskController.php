<?php

namespace api\controllers;

use core\forms\backend\compositeForm\TaskCreateForm;
use core\forms\backend\compositeForm\TaskUpdateForm;
use core\forms\api\search\TaskSearch;
use core\helpers\UploadedFile;
use Yii;
use core\entities\Task;
use core\services\api\TaskService;
use yii\base\Module;
use yii\web\NotFoundHttpException;

class TaskController extends ApiController
{
    private $service;

    public function __construct($id, Module $module, array $config = [], TaskService $project)
    {
        parent::__construct($id, $module, $config);
        $this->service = $project;
    }

    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $dataProvider;
    }

    public function actionView($id)
    {
        try {
            $task = $this->service->view($id, Yii::$app->user->id);
            return $task;
        } catch (\DomainException $e) {
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionCreate()
    {
        $form = new TaskCreateForm(Yii::$app->request->getBodyParams());
        $form->load(Yii::$app->request->getBodyParams(), '');
        if ($form->validate()) {
            try {
                $form->uploadedFile = UploadedFile::uploadsBase64($form->files);
                return $this->service->create($form, Yii::$app->user->id);
            } catch (\DomainException $e) {
                return $this->setResponseErrorStatus($e->getMessage());
            }
        }
        return $this->setResponseErrorStatus($form->getErrors());
    }

    public function actionUpdate($id)
    {
        $task = $this->findModel($id);
        $form = new TaskUpdateForm(Yii::$app->request->getBodyParams(), $task);
        $form->load(Yii::$app->request->getBodyParams(), '');
        if ($form->validate()) {
            try {
                $form->uploadedFile = UploadedFile::uploadsBase64($form->files);
                return $this->service->edit($form, $task->id, Yii::$app->user->id);
            } catch (\DomainException $e) {
                return $this->setResponseErrorStatus($e->getMessage());
            }
        }
        return $this->setResponseErrorStatus($form->getErrors());
    }

    public function actionRemoveImage($id)
    {
        try{
            $this->service->removeImage($id, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionDelete($id)
    {
        try {
            $this->service->delete($id, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        } catch (\DomainException $e) {
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionStatusDelete($id){
        try{
            $this->service->changeStatus($id, Task::DELETED, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionStatusActive($id){
        try{
            $this->service->changeStatus($id, Task::ACTIVE, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionStatusInactive($id){
        try{
            $this->service->changeStatus($id, Task::INACTIVE, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}