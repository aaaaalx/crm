<?php

namespace api\controllers;

use core\forms\backend\compositeForm\ProjectCreateForm;
use core\forms\backend\compositeForm\ProjectUpdateForm;
use core\forms\api\search\ProjectSearch;
use core\helpers\UploadedFile;
use Yii;
use core\entities\Project;
use core\services\api\ProjectService;
use yii\base\Module;
use yii\web\NotFoundHttpException;

class ProjectController extends ApiController
{
    private $service;

    public function __construct($id, Module $module, array $config = [], ProjectService $project)
    {
        parent::__construct($id, $module, $config);
        $this->service = $project;
    }

    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $dataProvider;
    }

    public function actionView($id)
    {
        try {
            return $this->service->view($id, Yii::$app->user->id);
        } catch (\DomainException $e) {
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionCreate()
    {
        $form = new ProjectCreateForm(Yii::$app->request->getBodyParams());
        $form->load(Yii::$app->request->getBodyParams(), '');
        $image = $form->image;
        $form->image = null;
        if ($form->validate()) {
            try {
                $form->image = UploadedFile::uploadBase64($image);
                return $this->service->create($form, Yii::$app->user->id);
            } catch (\DomainException $e) {
                return $this->setResponseErrorStatus($e->getMessage());
            }
        }
        return $this->setResponseErrorStatus($form->getErrors());
    }

    public function actionUpdate($id)
    {
        $project = $this->findModel($id);
        $form = new ProjectUpdateForm(Yii::$app->request->getBodyParams(), $project);
        $form->load(Yii::$app->request->getBodyParams(), '');
        $image = $form->image;
        $form->image = null;
        if ($form->validate()) {
            try {
                $form->image = UploadedFile::uploadBase64($image);
                return $this->service->edit($form, $project->id, Yii::$app->user->id);
            } catch (\DomainException $e) {
                return $this->setResponseErrorStatus($e->getMessage());
            }
        }
        return $this->setResponseErrorStatus($form->getErrors());
    }

    public function actionStatusDelete($id){
        try{
            $this->service->changeStatus($id, Project::DELETED, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionStatusActive($id){
        try{
            $this->service->changeStatus($id, Project::ACTIVE, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionStatusInactive($id){
        try{
            $this->service->changeStatus($id, Project::INACTIVE, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionRemoveImage($id)
    {
        try{
            $this->service->removeImage($id, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }
        catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionDelete($id)
    {
        try {
            $this->service->delete($id, Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        } catch (\DomainException $e) {
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}