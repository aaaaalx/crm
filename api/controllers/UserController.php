<?php

namespace api\controllers;

use core\entities\User;
use core\exceptions\NotFoundException;
use core\forms\common\UserForm;
use core\services\api\UserService;
use yii\base\Module;

class UserController extends ApiController
{
    public $service;

    public function __construct(string $id, Module $module, array $config = [], UserService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionCreate()
    {
        $form = new UserForm();
        if ($form->load(\Yii::$app->request->getBodyParams(), '') && $form->validate()) {
            try {
                $user = $this->service->create(\Yii::$app->user->id, $form);
                return $user;
            } catch (\DomainException $e) {
                return $this->setResponseErrorStatus($e->getMessage());
            }
        }
    }

    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $form = new UserForm($user);
        if ($form->load(\Yii::$app->request->getBodyParams(), '') && $form->validate()) {
            try{
                $user = $this->service->edit($user->id, \Yii::$app->user->id, $form);
                return $user;
            }catch (\DomainException $e){
                return $this->setResponseErrorStatus($e->getMessage());
            }
        }
    }

    public function actionView($id)
    {
        $user = $this->findModel($id);
        try{
            return $this->service->view($user->id, \Yii::$app->user->id);
        }catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionDelete($id)
    {
        $user = $this->findModel($id);
        try{
            $this->service->delete($user->id, \Yii::$app->user->id);
            return $this->setResponseSuccessStatus();
        }catch (\DomainException $e){
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }

    public function actionMyRole()
    {
        return $this->service->getRole(\Yii::$app->user->id);
    }

    private function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundException('The requested page does not exist.');
    }

}