<?php

namespace api\controllers;

use core\entities\Tag;
use Yii;
use yii\db\Query;
use yii\web\Response;

class TagOptionsController extends ApiController
{
    public function actionGetTags($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, name AS text')->from('tags')->where(['status' => Tag::ACTIVE]);
            if (is_numeric($q)) {
                $query->andWhere(['id' => filter_var($q, FILTER_VALIDATE_INT)]);
                $query->orWhere(['like', 'name', $q]);
            } else {
                $query->andWhere(['like', 'name', $q]);
            }
            $query->orderBy(['id' => SORT_DESC])->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = $data;
        }
        return $out;
    }

}