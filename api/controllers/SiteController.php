<?php


namespace api\controllers;


use core\exceptions\NotFoundException;
use core\helpers\DumpHelper;
use core\repositories\VersionRepository;
use yii\base\Module;

class SiteController extends ApiController
{
    private $versionRepository;

    public function __construct($id, Module $module, array $config = [], VersionRepository $versionRepository)
    {
        parent::__construct($id, $module, $config);
        $this->versionRepository = $versionRepository;
    }

    public function actionIndex()
    {
        try {
            return $this->versionRepository->getLast();
        } catch (NotFoundException $e) {
            return $this->setResponseErrorStatus($e->getMessage());
        }
    }
}