<?php

namespace api\controllers;

use core\entities\Options;
use core\entities\Project;
use core\helpers\OptionsHelper;
use Yii;
use yii\db\Query;
use yii\web\Response;

class ProjectOptionsController extends ApiController
{
    public function actionOptions()
    {
        $post = Yii::$app->request->post();
        $project_id = !empty($post['project_id']) ? $post['project_id'] : null;
        $option_id = !empty($post['option_id']) ? $post['option_id'] : null;
        $options = !empty($post['options']) ? $post['options'] : null;
        $field_name = @$post['field_name'];
        $field_id = @$post['field_id'];
        if (Yii::$app->request->isAjax && $option_id) {
            $option = Options::find()->where(['id' => $option_id])->one();
            $projectOptions = Project::find()->where('project_id = :project_id AND option_id = :option_id', [':project_id' => $project_id, ':option_id' => $option_id])->one() ?: null;
            if ($option) {
                return OptionsHelper::getOutputByType($option, $field_name, $field_id, $projectOptions->value ?? null, $options);
            }
        }
    }

    public function actionProjects($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, title AS text')
                ->from('projects')
                ->where(['status' => Project::ACTIVE]);
            if (is_numeric($q)) {
                $query->andWhere(['id' => filter_var($q, FILTER_VALIDATE_INT)]);
                $query->orWhere(['like', 'title', $q]);
            } else {
                $query->andWhere(['like', 'title', $q]);
            }
            $query->orderBy(['id' => SORT_DESC])->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = $data;
        }
        return $out;
    }

}