<?php

namespace api\controllers;

use core\entities\Task;
use Yii;
use yii\db\Query;
use yii\web\Response;

class TaskOptionsController extends ApiController
{
    public function actionRelations($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, title AS text')
                ->from('tasks')
                ->where(['status' => Task::ACTIVE]);
            if (is_numeric($q)) {
                $query->andWhere(['id' => filter_var($q, FILTER_VALIDATE_INT)]);
                $query->orWhere(['like', 'title', $q]);
            } else {
                $query->andWhere(['like', 'title', $q]);
            }
            $query->orderBy(['id' => SORT_DESC])->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = $data;
        }
        return $out;
    }

}