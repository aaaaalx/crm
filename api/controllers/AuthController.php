<?php

namespace api\controllers;


use core\forms\api\LoginForm;
use core\services\api\AuthService;
use yii\base\Module;

class AuthController extends ApiController
{
    public $service;

    public function __construct(string $id, Module $module, array $config = [], AuthService $service)
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    public function actionLogin()
    {
       $form = new LoginForm();
       if ($form->load(\Yii::$app->request->getBodyParams(), '') && $form->validate()) {
            try{
                $user = $this->service->auth($form);
                return $user;
            }catch (\Exception $e){
                return $this->setResponseErrorStatus([$e->getMessage()]);
            }
       }
       return $this->setResponseErrorStatus($form->getErrors());
    }

}