<p align="center">
    <h1 align="center">CRM Admin panel and API</h1>
    <br>
</p>

Описание работы с API <a href="" target="_blank">здесь.</a>

Требования
-------------------
База данных: <code>PostgreSQL >=10</code>

PHP: <code>>=7.2.0</code>

Redis: <code>3.2</code>

Установка проекта
-------------------
```
git clone xxx

composer update

mkdir api/runtime

mkdir api/web/assets

init

```
В качестве примера файла конфигурации переменных окружения можно использовать файлы: <code>.env.dev</code> / <code>.env.prod</code>

Необходимо в корень проекта скопировать необходимый файл и переименовать его в <code>.env</code> с указанием всем необходимых значений переменных окружения
```

yii migrate

yii migrate --migrationPath=@yii/rbac/migrations/

yii migrate --migrationPath=@Zelenin/yii/modules/I18n/migrations
```
Консольные команды для RBAC
-------------------

После выполнения команды 

<code>yii rbac/init</code> 

в системе появится две роли: administrator и moderator

```
yii rbac/init - инициализация ролей и прав

yii rbac/add-administrator {email} {password} - создание админа c ролью администратор

yii rbac/add-moderator {email} {password} - создание админа c ролью модератор

yii rbac/add {email} {password} {role} - создание админа c ролью {role}

yii rbac/assign-role-to-user {email} {role} - назначение роли = {role} для администратора с email = {email}

yii rbac/revoke-role-from-user {email} {role} - удаление роли = {role} у администратора с email = {email}
```
Общие консольные команды
-------------------
```
yii common/init-languages - инициализация языков приложения

```

Точки входа
-------------------
Admin panel: <code>backend/web/index.php</code>

API: <code>api/web/index.php</code>
