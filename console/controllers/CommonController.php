<?php


namespace console\controllers;


use core\entities\Language;
use core\helpers\LanguagesHelper;
use core\repositories\LanguageRepository;
use yii\base\Module;
use yii\console\Controller;

class CommonController extends Controller
{

    private $languages;

    public function __construct($id, Module $module, array $config = [], LanguageRepository $languages)
    {
        parent::__construct($id, $module, $config);
        $this->languages = $languages;
    }

    public function actionInitLanguages()
    {
        $languages = LanguagesHelper::getList();

        foreach ($languages as $iso => $title) {
            try {
                $lang = $this->languages->getByIso($iso);
                $this->stderr('Language already exist: ' . $lang->iso . PHP_EOL);
                continue;
            } catch (\DomainException $e) {
                $lang = Language::create($title, $iso);
                try {
                    $this->languages->save($lang);
                    $this->stdout('Successfully created language: ' . $lang->iso . PHP_EOL);
                } catch (\DomainException $repoError) {
                    $this->stderr($e->getMessage());
                }
            }
        }
    }
}