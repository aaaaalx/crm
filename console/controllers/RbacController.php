<?php

namespace console\controllers;

use core\entities\Admin;
use core\forms\console\AdminCreateForm;
use core\services\console\RbacService;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class RbacController extends Controller
{

    private $service;

    public function __construct($id, Module $module, array $config = [], RbacService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionInit()
    {
        try {
            $permissions = [];
            $admin = $this->service->createRole('administrator', 'Site administrator');
            $this->service->createRole('moderator', 'Site moderator');

            $permissions[] = $this->service->createPermission('admin', 'Access to Admins');
            $permissions[] = $this->service->createPermission('permit', 'Access to Permission config');
            $permissions[] = $this->service->createPermission('user', 'Access to Users');
            $permissions[] = $this->service->createPermission('version', 'Access to Versions info');
            $permissions[] = $this->service->createPermission('access', 'Access to RBAC info');
            $permissions[] = $this->service->createPermission('search', 'Access to search');
            $permissions[] = $this->service->createPermission('language', 'Access to languages');
            $permissions[] = $this->service->createPermission('project', 'Access to projects');
            $permissions[] = $this->service->createPermission('tag', 'Access to tags');
            $permissions[] = $this->service->createPermission('options', 'Access to options');
            $permissions[] = $this->service->createPermission('role', 'Access to roles');
            $permissions[] = $this->service->createPermission('position', 'Access to position');
            $permissions[] = $this->service->createPermission('profile', 'Access to profile');

            $this->service->addChildren($admin, $permissions);

            $this->stdout('Success init. Create roles: "administrator", "moderator"');
        } catch (\Exception $e) {
            $this->stderr('Error. Roles already exists');
        }
    }

    public function actionAddAdministrator($email, $password)
    {
        $form = new AdminCreateForm('administrator');
        $form->load(compact(['email', 'password']), '');
        if ($form->validate()) {
            try {
                $admin = $this->service->create($form);
                $this->stdout( 'Successfully created admin with id: ' . $admin->id . ' and with role: administrator');
                return;
            } catch (\Exception $e) {
                $this->stderr($e->getMessage());
                return;
            }
        }
        $this->stderr($form->consoleLogErrors());
        return;
    }

    public function actionAddModerator($email, $password)
    {
        $form = new AdminCreateForm('moderator');
        $form->load(compact(['email', 'password']), '');
        if ($form->validate()) {
            try {
                $admin = $this->service->create($form);
                $this->stdout( 'Successfully created admin with id: ' . $admin->id . ' and with role: moderator');
                return;
            } catch (\Exception $e) {
                $this->stderr($e->getMessage());
                return;
            }
        }
        $this->stderr($form->consoleLogErrors());
        return;
    }

    public function actionAdd($email, $password, $role)
    {
        $form = new AdminCreateForm();
        $form->load(compact(['email', 'password', 'role']), '');
        if ($form->validate()) {
            try {
                $admin = $this->service->create($form);
                $this->stdout( 'Successfully created admin with id: ' . $admin->id . ' and with role: moderator');
                return;
            } catch (\Exception $e) {
                $this->stderr($e->getMessage());
                return;
            }
        }
        $this->stderr($form->consoleLogErrors());
        return;
    }

    public function actionAssignRoleToUser($email, $role)
    {
        $user = Admin::findOne(['email' => $email]);
        $authManager = \Yii::$app->authManager;
        if ($user !== null) {
            if ($role = $authManager->getRole($role)) {
                try {
                    $authManager->assign($role, $user->id);
                    $this->stdout('Successfully assigned role: ' . $role->name . ' to admin with id: ' . $user->id);
                    return;
                } catch (\Exception $e) {
                    $this->stderr('Failed to assign');
                    return;
                }
            }
            $this->stderr('Role not found');
            return;
        }
        $this->stderr('User not found');
        return;
    }

    public function actionRevokeRoleFromUser($email, $role)
    {
        $user = Admin::findOne(['email' => $email]);
        $authManager = \Yii::$app->authManager;
        if ($user !== null) {
            if ($role = $authManager->getRole($role)) {
                $authManager->revoke($role, $user->id);
                $this->stdout('Successfully revoked role: ' . $role->name . ' from admin with id: ' . $user->id);
                return;
            }
            $this->stderr('Role not found');
            return;
        }
        $this->stderr('User not found');
        return;
    }

    public function actionRoles()
    {
        try {
            $roles = $this->service->roles();
            $this->stdout( 'Roles list: ' . PHP_EOL . implode(PHP_EOL, ArrayHelper::getColumn($roles, 'name')));
            return;
        } catch (\Exception $e) {
            $this->stderr($e->getMessage());
            return;
        }
    }
}