<?php

namespace console\controllers;

use core\services\back\AdminService;
use yii\base\Module;
use yii\console\Controller;

class AdminController extends Controller
{

    private $service;

    public function __construct($id, Module $module, array $config = [], AdminService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionActivate($id)
    {
        try {
            $this->service->activate($id);
            $this->stdout('Admin with ID: ' . $id . ' was successfully activated');
        } catch (\DomainException $e) {
            $this->stdout($e->getMessage());
        }
    }

    public function actionModerate($id)
    {
        try {
            $this->service->moderate($id);
            $this->stdout('Admin with ID: ' . $id . ' was successfully send to moderation');
        } catch (\DomainException $e) {
            $this->stdout($e->getMessage());
        }
    }
}